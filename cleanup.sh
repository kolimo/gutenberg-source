#!/bin/bash
#############################################
# this is a cleanup script to prepare the 
# gutenberg.de corpus to get ingested in a
# xml database
#
# copy the folder dvd13 folder from the 
# Gutenberg DVD and start 
# within the folder that contains the ./dvd13
#############################################

START=$(find . -type f -name "*.xml" | wc -l)

echo "we start with $START xml files in this folder"

find . -name "*.jpg" -type f -delete
find . -name "*.gif" -type f -delete
find . -name "*.png" -type f -delete
find . -name "*.pdf" -type f -delete
find . -name "*.wav" -type f -delete
find . -name "*.mid" -type f -delete
find . -name "*.zip" -type f -delete # mimetypes (?), images (w busch)
find . -name "*.xsl" -type f -delete
find . -name "*.css" -type f -delete
find . -name "*.js"  -type f -delete
find . -name "*.DS_Store" -type f -delete # OS X hidden file
find . -name "*.doc" -type f -delete
find . -name "*.jbf" -type f -delete
find . -name "*.php" -type f -delete
find . -name "*.db"  -type f -delete # Thunbs.db = Windows thumbnails in folders containing images

find . -name "*xmo" -type f # seems to be a typo, lets correct
mv ./dvd13/morgenst/pfad/pfad.xmo ./dvd13/morgenst/pfad/pfad.xml
mv ./dvd13/hedenstj/allerlei/allerlei.xmo ./dvd13/hedenstj/allerlei/allerlei.xml
mv ./dvd13/ferry/waldlauf/waldlauf.xmo ./dvd13/ferry/waldlauf/waldlauf.xml

# just check:
find . -type f | awk -F. '!a[$NF]++{print $NF}'

# Taschengoedecke fliegt raus, beinhaltet keine Volltext-XML-Dateien und hat völlig andere Struktur
rm -r dvd13/hirschbe

find . -type f -name "*.pl" -delete

# info beinhaltet webseitentexte und "lesetipps"
rm -r dvd13/info
# autoreninformationen raus
rm -r dvd13/autoren

find . -type d -empty -delete

cd ./dvd13/alexis/bredow
find . ! -name 'bredow.xml' -type f -exec rm -f {} \;
cd ../../..

cd ./dvd13/brentans/marie
find . ! -name 'marie.xml' -type f -exec rm -f {} \;
cd ../../..

find . -type f -name "composed.xml" -delete
find . -type f -name "temp.xml" -delete
find . -type f -name "0html*" -delete

# find . -type d -name "gedichte" -delete 
# -delete can not remove folders containing files
find . -type d -name "gedichte" -exec rm -rf {} \;

find . -type d -empty -delete

END=$(find . -type f -name "*.xml" | wc -l)
echo "now we have $END xml files in this folder"
echo "we removed $(expr $END - $START) items"



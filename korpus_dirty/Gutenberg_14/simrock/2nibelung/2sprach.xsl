<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!--
	
Date
2003-06-13:	created


warning:	xxx

2do:		xxx

bugs:		xxx
-->

<xsl:output
	method="html"
	indent="yes"
	encoding="ISO-8859-1"/>


<xsl:variable name="file1" select="/merge/file1/@src"/>
<xsl:variable name="file2" select="/merge/file2/@src"/>

<xsl:template match="/">
<xsl:apply-templates/>
</xsl:template>

<!-- nicht alle infos aus prosa.css korrekt? -->

<xsl:template match="merge">
<html>
<head>
<link rel="stylesheet"	type="text/css"	href="../../css/prosa.css" />
<style>
body		{font-family: sans-serif}
p		{margin-top:0; margin-bottom:0}
td		{padding:0.5em;border:1px solid}
</style>
</head>
<body>
<table border="0" width="100%">
<tr><td width="50%">
<xsl:apply-templates select="document($file1)//body/div/h3"/>
</td>
<td>
<xsl:apply-templates select="document($file2)//body/div/h3"/>
</td></tr>
<xsl:for-each select="document($file1)//div[@class='vers']">
<tr><td>
 <xsl:apply-templates/>
</td>
<td>
 <xsl:call-template name="other">
  <xsl:with-param name="pos" select="position()"/>
 </xsl:call-template>
</td>
</tr>
</xsl:for-each>
</table>
</body>
</html>
</xsl:template>

<xsl:template name="other">
 <xsl:param name="pos" />
 <xsl:for-each select="document($file2)//div[@class='vers']">
  <xsl:if test="position()=$pos">
   <xsl:apply-templates />
  </xsl:if>
 </xsl:for-each>
</xsl:template>

<xsl:template match="*">
 <xsl:copy-of select="."/>
</xsl:template>

</xsl:stylesheet>

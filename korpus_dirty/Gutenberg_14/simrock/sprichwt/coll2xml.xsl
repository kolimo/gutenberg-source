<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0"
    xmlns:lxslt="http://xml.apache.org/xslt"
    xmlns:redirect="org.apache.xalan.lib.Redirect"
    extension-element-prefixes="redirect">

<xsl:output
	method="xml"
	encoding="ISO-8859-1"
	indent="yes" />

<!--	erzeuge XML-Dateien aus dirlist.xml

Aufruf:	\gutenb\bin\xalan\testxalan -in collection.xml -xsl coll2xml.xsl -out NUL:

20070222	erstellt


2do:			

-->

  <lxslt:component prefix="redirect" elements="write open close" functions="">
    <lxslt:script lang="javaclass" src="org.apache.xalan.lib.Redirect"/>
  </lxslt:component>  


<xsl:template match="/">
  <xsl:apply-templates select="/collection/doc" />
</xsl:template>

<xsl:template match="doc">
 <xsl:variable name="file" select="concat(@id,'.xml')"/>
 <xsl:message>create: <xsl:value-of select="$file" /></xsl:message>
 <redirect:write select="$file">
<xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="../../bin/xsl/xml2htm.xsl"</xsl:processing-instruction>
    <xsl:apply-templates />
 </redirect:write>
</xsl:template>

<xsl:template match="@*|node()">
 <xsl:copy>
  <xsl:apply-templates select="@*|node()" />
 </xsl:copy>
</xsl:template>

</xsl:stylesheet>

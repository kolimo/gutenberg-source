<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0"
    xmlns:lxslt="http://xml.apache.org/xslt"
    xmlns:redirect="org.apache.xalan.lib.Redirect"
    extension-element-prefixes="redirect">

<xsl:output
	method="xml"
	encoding="ISO-8859-1"
	indent="yes" />

<!--	extract recipes from document (only div.subchapter)
	version 1.00
	ID must be "Xnnnn" (one char+number)
	removes meta and comments
	meta.created should be today

20070422	modified for XML output, added processing instruction

	usage: testxalan -in temp.xml -xsl recipes.xsl -out errs.xml

-->

  <lxslt:component prefix="redirect" elements="write open close" functions="">
    <lxslt:script lang="javaclass" src="org.apache.xalan.lib.Redirect"/>
  </lxslt:component>  

<xsl:template match="/">
  <xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="../../bin/xsl/xml2htm.xsl"</xsl:processing-instruction>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="meta" />
<xsl:template match="processing-instruction()|comment()"/>

<xsl:template match="div[@class='subchapter']">
<xsl:variable name="number" select="substring(@id,2)"/>
<xsl:variable name="name" select="concat('nr',$number)"/>

<!--xsl:message><xsl:value-of select="$name" /></xsl:message-->

<redirect:write select="concat($name,'.xml')">
<xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="../../bin/xsl/xml2htm.xsl"</xsl:processing-instruction>
<html>
 <head>
<title><xsl:value-of select="./h4"/></title>
<link rel="stylesheet" type="text/css" href="../../css/prosa.css" />
<meta name="type"	content="reference" />
<meta name="author"	content="Henriette Davidis" />
<meta name="sender"	content="hille@abc.de" />
<meta name="generator"	content="recipes.xsl" />
<meta name="created"	content="20070422" />
 </head>
<body>
 <xsl:apply-templates />

<div class="navi">
<table width="20%" align="center">
<tr>
<td align="left"><a href="{concat('nr',concat($number -1,'.xml'))}"><img src="../../pic/bwd.gif" border="0" /></a></td>
<td align="center"><a href="0htmldir.xml"><img src="../../pic/up.gif" border="0"/></a></td>
<td align="right"><a href="{concat('nr',concat($number +1,'.xml'))}"><img src="../../pic/fwd.gif" border="0"/></a></td>
</tr>
</table>
</div>
</body>
</html>
</redirect:write>
</xsl:template>

<!-- a href="Nxxxx" assumed -->

<xsl:template match="a[@href]">
<xsl:variable name="refnr" select="substring(@href,2)"/>
<a href="{concat(concat('nr',$refnr),'.xml')}">
 <xsl:apply-templates/>
</a>
</xsl:template>

<xsl:template match="@*|node()">
<xsl:copy>
 <xsl:apply-templates select="@*|node()" />
</xsl:copy>
</xsl:template>

</xsl:stylesheet>

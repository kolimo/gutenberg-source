<?xml version="1.0" encoding="ISO-8859-1"?>

<!--
	generate temp file with incremental IDs for <div class="subchapter">
	created: 2003-03-08
	
	usage: testxalan -in kochbuch.xml -xsl gen-ids.xsl -out temp.xml

 -->

<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    version="1.0"
    xmlns:lxslt="http://xml.apache.org/xslt"
    xmlns:dibi="http://www.dibi.de/"
    extension-element-prefixes="dibi">


<xsl:output
	method="xml"
	encoding="ISO-8859-1"
	indent="yes" />

  <lxslt:component prefix="dibi" functions="nextid">
    <lxslt:script lang="javascript">
      var index=0;
      var s="N";
      function nextid()
      { index= index +1;
        return s+index;
      }
    </lxslt:script>
  </lxslt:component>

<xsl:template match="html">
<html>
 <xsl:apply-templates />
</html>
</xsl:template>

<xsl:template match="div[@class='subchapter']">
<xsl:variable name="number" select="dibi:nextid()"/>
<div class="{@class}" id="{$number}">
<!-- special code for Netscape, because ID is not recognized -->
<a name="{$number}"/>
 <xsl:apply-templates />
</div>
</xsl:template>

<xsl:template match="@*|node()">
<xsl:copy>
 <xsl:apply-templates select="@*|node()" />
</xsl:copy>
</xsl:template>

</xsl:stylesheet>

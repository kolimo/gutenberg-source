<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!--
	generate temp file with IDs for <div class="subchapter">
	created: 2003-01-28 (0:15 hours)
	
	usage: f:\gutenb\bin\testxslt -in kochbuch.xml -xsl gen-ids.xsl -out temp.xml

2do:	id muss inkrementell 1..n sein (xalan und javascript? oder per SR32?)
	als named template "next" ???
 -->

<xsl:output
	method="xml"
	encoding="ISO-8859-1"
	indent="yes" />


<xsl:template match="html">
<html>
 <xsl:apply-templates />
</html>
</xsl:template>

<xsl:template match="div[@class='subchapter']">
<div class="{@class}" id="{generate-id()}">
<!-- special code for Netscape, because ID is not recognized -->
<a name="{generate-id()}"/>
 <xsl:apply-templates />
</div>
</xsl:template>

<xsl:template match="@*|node()">
<xsl:copy>
 <xsl:apply-templates select="@*|node()" />
</xsl:copy>
</xsl:template>

</xsl:stylesheet>

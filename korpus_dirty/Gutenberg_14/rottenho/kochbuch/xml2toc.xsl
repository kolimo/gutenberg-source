<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!--	abgeleitet aus: xml2toc version 1.02

20070422	XML output
20020920	created:  (1:00 hours)
	
	usage: \gutenb\bin\testxslt -in kochbuch.xml -xsl xml2toc.xsl -out toc.xml

	alle div-Elemente brauchen ID, siehe genids.xsl

bugs:	nur anwendbar auf Kochbuch von Davidis
	indent=yes gibt gro�e Dateien (blanks)
 -->

<xsl:output
	method="xml"
	encoding="ISO-8859-1"
	indent="yes" />

<!-- get filename, set depth of indexing (both from external files -->

<xsl:variable name="filename">kochbuch.xml</xsl:variable>

<xsl:template match="/">
  <xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="../../bin/xsl/xml2htm.xsl"</xsl:processing-instruction>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="html">
<html>
<head>
 <link rel="stylesheet" type="text/css" href="../../css/prosa.css"/>
 <meta name="generator" content="xml2toc.xsl" />
 <meta name="sender"	content="hille@abc.de" />
 <meta name="created"	content="20070422" />
</head>
 <xsl:apply-templates select="body" />
</html>
</xsl:template>

<xsl:template match="body">
<body>
 <div class="toc" id="toc">
 <h3>Inhalt</h3>
  <ul>
   <xsl:apply-templates select="div" />
  </ul>
 </div>
</body>
</xsl:template>

<!--	exclude figure,titlepage and toc from building table of content
	check for other div classes not yet implemented
-->


<xsl:template match="div[@class='figure']" />
<xsl:template match="div[@class='titlepage']" />
<xsl:template match="div[@class='toc']" />
<xsl:template match="div[@class='impressum']" />


<xsl:template match="div[@class='section']">
   <li>
     <a href="{$filename}#{generate-id()}">
     <xsl:apply-templates select="h1[1] | h2[1] | h3[1] | h4[1] | h5[1]" />
     </a>
     <ul>
      <xsl:apply-templates select="div[@class='chapter']" />
     </ul>
   </li>
</xsl:template>

<xsl:template match="div[@class='chapter']">
   <li>
    <a href="{$filename}#{generate-id()}">
     <xsl:apply-templates select="h1[1] | h2[1] | h3[1] | h4[1] | h5[1]" />
    </a>
     <ul>
      <xsl:apply-templates select="div[@class='subchapter']" />
     </ul>
   </li>
</xsl:template>

<xsl:template match="div[@class='subchapter']">
   <li>
    <a href="{$filename}#{generate-id()}">
     <xsl:apply-templates select="h1[1] | h2[1] | h3[1] | h4[1] | h5[1]" />
    </a>
   </li>
</xsl:template>

<xsl:template match="h1 | h2 | h3 | h4 | h5">
 <xsl:apply-templates/>
</xsl:template>

</xsl:stylesheet>

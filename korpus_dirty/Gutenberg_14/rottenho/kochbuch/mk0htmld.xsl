<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output
	method="xml"
	encoding="ISO-8859-1"
	indent="yes" />

<!--	create 0htmldir from document
	version 1.00 (only valid for "Henriette Davidis Kochbuch")

20070422	modified for XML output
	usage: testxslt -in kochbuch.xml -xsl mk0htmld.xsl -out 0htmldir.htm

-->

<xsl:template match="/">
  <xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="../../bin/xsl/xml2htm.xsl"</xsl:processing-instruction>
  <xsl:apply-templates />
</xsl:template>

<xsl:template match="html">
<html>
 <head>
<title>Henriette Davidis - Praktisches Kochbuch: Inhaltsangabe</title>
<link rel="stylesheet" type="text/css" href="../../css/prosa.css" />
<meta name="type"	content="toc" />
<meta name="author"	content="Henriette Davidis" />
<meta name="sender"	content="hille@abc.de" />
<meta name="created"	content="20030308" />
<meta name="generator"	content="mk0htmld.xsl" />
 </head>
<body>
<h2>Henriette Davidis: Praktisches Kochbuch</h2>
<p>Zur <a href="0alfadir.xml">alfabetischen Rezeptliste</a></p>
 <xsl:apply-templates select="*//div"/>
</body>
</html>
</xsl:template>

<xsl:template match="div[@class='section']">
 <br/><xsl:apply-templates select="h2 | h3 | h4"/>
</xsl:template>

<xsl:template match="div[@class='part']">
 <br/><xsl:apply-templates select="h2 | h3 | h4"/>
</xsl:template>

<xsl:template match="div[@class='chapter']">
 <br/><xsl:apply-templates select="h2 | h3 | h4"/>
</xsl:template>

<!-- numbering is N1..N9999 -->

<xsl:template match="div[@class='subchapter']">
<xsl:variable name="number" select="substring(./@id,2)"/>
<xsl:variable name="name" select="concat('nr',$number)"/>
<br/><a href="{concat($name,'.xml')"><xsl:apply-templates select="./h4"/></a>
</xsl:template>

<xsl:template match="h2 | h3">
 <b><xsl:value-of select="./text()"/><xsl:apply-templates/></b>
</xsl:template>

<xsl:template match="h4">
 <xsl:value-of select="./text()"/><xsl:apply-templates/>
</xsl:template>

<xsl:template match="ins">
 <xsl:value-of select="./text()"/>
</xsl:template>

<xsl:template match="@*|node()"/>

</xsl:stylesheet>

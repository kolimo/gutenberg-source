#!/usr/bin/perl

# make list of *.GIF files of current dir (from command line args)
# cmd: perl allgifs.pl arg1 arg2
# out: mogrify.bat
#
# created: 12-04-2002 10:00 - 11:00 (gh)
#

open(OUTFILE,">process.bat") || die "Outfile test.xml nicht erzeugt\n";
print OUTFILE "REM created by mogrify.pl\n";
opendir(DIRNAME,".");
while($fname=readdir(DIRNAME))
{

@var=split(/\./,$fname,2);
$_=$var[1];
if (/gif/i){
print OUTFILE "ren $fname \n";
};
};
closedir(DIRNAME);
close(OUTFILE);

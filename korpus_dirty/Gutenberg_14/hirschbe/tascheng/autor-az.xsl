<?xml version="1.0" encoding="ISO-8859-1" ?> 
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:lxslt="http://xml.apache.org/xslt"
	xmlns:redirect="org.apache.xalan.lib.Redirect"
	extension-element-prefixes="redirect">
<!-- 

usage:	\gutenb\bin\xalan27\testxalan -in tascheng.xml -xsl autor-az.xsl -out temp.xml

2007-04-12	Umstellung auf XML, Spaltenzahl als Variable
2004-05-08	div f�r jeden Buchstaben
2004-04-27	created

--> 

<xsl:output
	method="xml"
	encoding="ISO-8859-1"
	indent="yes"/>

<xsl:variable name="columns">3</xsl:variable>

<xsl:template match="/">
 <xsl:apply-templates select="//letter[@id]" />
</xsl:template>

<xsl:template match="letter[@id]">
 <xsl:variable name="letter" select="translate(@id,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
 <xsl:variable name="entries" select="count(./author)"/>
 <redirect:write select="concat(concat('autor-',@id),'.xml')">
 <xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="../../bin/xsl/xml2htm.xsl"</xsl:processing-instruction>
<html>

<head>
 <title>Autornamen mit "<xsl:value-of select="$letter"/>"</title>
 <link rel="stylesheet"	type="text/css"	href="../../css/prosa.css"/>
 <meta name="generator"	content="autor-az.xsl XSLT-Transformation"/>
 <meta name="sender"	content="hille@abc.de"/>
 <meta name="created"	content="20070412"/>
</head>

<body>
 <h3>Gutenbergs Online Taschengoedeke</h3>
 <hr size="1"/>
 <h5>Autoren A-Z:
  <a href="autor-a.xml">A</a> |
  <a href="autor-b.xml">B</a> |
  <a href="autor-c.xml">C</a> |
  <a href="autor-d.xml">D</a> |
  <a href="autor-e.xml">E</a> |
  <a href="autor-f.xml">F</a> |
  <a href="autor-g.xml">G</a> |
  <a href="autor-h.xml">H</a> |
  <a href="autor-i.xml">I</a> |
  <a href="autor-j.xml">J</a> |
  <a href="autor-k.xml">K</a> |
  <a href="autor-l.xml">L</a> |
  <a href="autor-m.xml">M</a> |
  <a href="autor-n.xml">N</a> |
  <a href="autor-o.xml">O</a> |
  <a href="autor-p.xml">P</a> |
  <a href="autor-q.xml">Q</a> |
  <a href="autor-r.xml">R</a> |
  <a href="autor-s.xml">S</a> |
  <a href="autor-t.xml">T</a> |
  <a href="autor-u.xml">U</a> |
  <a href="autor-v.xml">V</a> |
  <a href="autor-w.xml">W</a> |
  <a href="autor-x.xml">X</a> |
  <a href="autor-y.xml">Y</a> |
  <a href="autor-z.xml">Z</a>
 </h5>
 <hr size="1"/>
 <h4>Autorenregister von "<xsl:value-of select="author/name"/>" bis "<xsl:value-of select="author[last()]/name"/>"</h4>
 <xsl:if test="$entries = 0">
 <p>Noch kein Eintrag</p>
 </xsl:if>
 <xsl:if test="$entries &gt; 0">
  <table cellspacing="2" style="border:1px solid black">
   <tr>
    <td valign="top"><xsl:apply-templates select="author"/></td>
   </tr>
  </table>
 </xsl:if>

 <hr size="1"/>
 <h5>Autoren A-Z:
  <a href="autor-a.xml">A</a> |
  <a href="autor-b.xml">B</a> |
  <a href="autor-c.xml">C</a> |
  <a href="autor-d.xml">D</a> |
  <a href="autor-e.xml">E</a> |
  <a href="autor-f.xml">F</a> |
  <a href="autor-g.xml">G</a> |
  <a href="autor-h.xml">H</a> |
  <a href="autor-i.xml">I</a> |
  <a href="autor-j.xml">J</a> |
  <a href="autor-k.xml">K</a> |
  <a href="autor-l.xml">L</a> |
  <a href="autor-m.xml">M</a> |
  <a href="autor-n.xml">N</a> |
  <a href="autor-o.xml">O</a> |
  <a href="autor-p.xml">P</a> |
  <a href="autor-q.xml">Q</a> |
  <a href="autor-r.xml">R</a> |
  <a href="autor-s.xml">S</a> |
  <a href="autor-t.xml">T</a> |
  <a href="autor-u.xml">U</a> |
  <a href="autor-v.xml">V</a> |
  <a href="autor-w.xml">W</a> |
  <a href="autor-x.xml">X</a> |
  <a href="autor-y.xml">Y</a> |
  <a href="autor-z.xml">Z</a>
 </h5>

</body>
</html>
</redirect:write>
</xsl:template>

<xsl:template match="author">
 <xsl:variable name="entries" select="count(parent::letter/author)"/>
 <xsl:variable name="percol" select="floor(($entries + $columns) div $columns)"/>

 <a href="letter-{parent::letter/@id}.xml#n{position()}"><xsl:apply-templates select="name" /></a><br/>
   <xsl:if test="(position() mod $percol) = 0">
     <xsl:text disable-output-escaping="yes">&lt;/td&gt;&lt;td valign="top"&gt;</xsl:text>
   </xsl:if>
</xsl:template>

<xsl:template match="name">
 <xsl:apply-templates />
</xsl:template>

<xsl:template match="*"/>
</xsl:stylesheet>

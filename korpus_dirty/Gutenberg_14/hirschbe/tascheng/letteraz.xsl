<?xml version="1.0" encoding="ISO-8859-1" ?> 
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:lxslt="http://xml.apache.org/xslt"
	xmlns:redirect="org.apache.xalan.lib.Redirect"
	extension-element-prefixes="redirect">
<!-- 

usage:	f:\gutenb>\gutenb\bin\xalan\testxalan -in tascheng.xml -xsl letteraz.xsl -out temp.xml

2004-05-08:	div f�r jeden Buchstaben
2004-04-27:	created

bugs:		Algorithmus f�r spaltenweises Setzen unklar (z.B: 5 Namen pro Tabellenspalte)
--> 

<xsl:output
	method="xml"
	encoding="ISO-8859-1"
	indent="yes"/>

<xsl:template match="/">
 <xsl:apply-templates select="//letter[@id]" />
</xsl:template>

<xsl:template match="letter[@id]">
 <xsl:variable name="letter" select="translate(@id,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
 <xsl:variable name="entries" select="count(./author)"/>
 <redirect:write select="concat(concat('letter-',@id),'.xml')">
 <xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="../../bin/xsl/xml2htm.xsl"</xsl:processing-instruction>

<html>

<head>
 <title>Autornamen mit "<xsl:value-of select="$letter"/>"</title>
 <link rel="stylesheet"	type="text/css"	href="../../css/prosa.css"/>
 <meta name="generator"	content="letteraz.xsl XSLT-Transformation"/>
 <meta name="sender"	content="hille@abc.de"/>
 <meta name="created"	content="20070413"/>
</head>

<body>
 <h3>Gutenbergs Online Taschengoedeke</h3>
 <hr size="1"/>
 <h5>Autoren A-Z:
  <a href="autor-a.xml">A</a> |
  <a href="autor-b.xml">B</a> |
  <a href="autor-c.xml">C</a> |
  <a href="autor-d.xml">D</a> |
  <a href="autor-e.xml">E</a> |
  <a href="autor-f.xml">F</a> |
  <a href="autor-g.xml">G</a> |
  <a href="autor-h.xml">H</a> |
  <a href="autor-i.xml">I</a> |
  <a href="autor-j.xml">J</a> |
  <a href="autor-k.xml">K</a> |
  <a href="autor-l.xml">L</a> |
  <a href="autor-m.xml">M</a> |
  <a href="autor-n.xml">N</a> |
  <a href="autor-o.xml">O</a> |
  <a href="autor-p.xml">P</a> |
  <a href="autor-q.xml">Q</a> |
  <a href="autor-r.xml">R</a> |
  <a href="autor-s.xml">S</a> |
  <a href="autor-t.xml">T</a> |
  <a href="autor-u.xml">U</a> |
  <a href="autor-v.xml">V</a> |
  <a href="autor-w.xml">W</a> |
  <a href="autor-x.xml">X</a> |
  <a href="autor-y.xml">Y</a> |
  <a href="autor-z.xml">Z</a>
 </h5>
 <hr size="1"/>
 <h4>Autorenregister von "<xsl:value-of select="author/name"/>" bis "<xsl:value-of select="author[last()]/name"/>"</h4>
 <xsl:if test="$entries = 0">
 <p>Noch kein Eintrag</p>
 </xsl:if>
 <xsl:if test="$entries &gt; 0">
  <table cellspacing="2">
   <xsl:apply-templates select="author"/>
  </table>
 </xsl:if>

 <hr size="1"/>
 <h5>Autoren A-Z:
  <a href="autor-a.xml">A</a> |
  <a href="autor-b.xml">B</a> |
  <a href="autor-c.xml">C</a> |
  <a href="autor-d.xml">D</a> |
  <a href="autor-e.xml">E</a> |
  <a href="autor-f.xml">F</a> |
  <a href="autor-g.xml">G</a> |
  <a href="autor-h.xml">H</a> |
  <a href="autor-i.xml">I</a> |
  <a href="autor-j.xml">J</a> |
  <a href="autor-k.xml">K</a> |
  <a href="autor-l.xml">L</a> |
  <a href="autor-m.xml">M</a> |
  <a href="autor-n.xml">N</a> |
  <a href="autor-o.xml">O</a> |
  <a href="autor-p.xml">P</a> |
  <a href="autor-q.xml">Q</a> |
  <a href="autor-r.xml">R</a> |
  <a href="autor-s.xml">S</a> |
  <a href="autor-t.xml">T</a> |
  <a href="autor-u.xml">U</a> |
  <a href="autor-v.xml">V</a> |
  <a href="autor-w.xml">W</a> |
  <a href="autor-x.xml">X</a> |
  <a href="autor-y.xml">Y</a> |
  <a href="autor-z.xml">Z</a>
 </h5>

</body>
</html>
</redirect:write>
</xsl:template>

<xsl:template match="author">
 <tr id="n{position()}">
 <th style="background:#c0c0c0">
  <xsl:apply-templates select="a"/>
 </th>
 <th style="background:#c0c0c0" colspan="2">
  <xsl:if test="@pg">
   <a href="../../autoren/namen/{@pg}.xml">
   <img src="../../pic/logo16.gif" border="0" align="left" alt="Autorbio im Projekt Gutenberg-DE"  title="Autorbio im Projekt Gutenberg-DE" /></a>
  </xsl:if>
 <xsl:apply-templates select="name" />
 <xsl:if test="book">
  <xsl:if test="pseudo">
   (Ps: <xsl:value-of select="pseudo/text()"/>)
  </xsl:if>
 </xsl:if>
 <xsl:if test="not(book)">
  <xsl:apply-templates select="pseudo" />
 </xsl:if>
 </th>
 <th style="background:#c0c0c0"><xsl:apply-templates select="lived"/></th>
 </tr>
 <xsl:apply-templates select="book"/>
</xsl:template>


<xsl:template match="pseudo">
 --&#x203a; <xsl:value-of select="text()"/>
</xsl:template>

<xsl:template match="name">
 <xsl:apply-templates />
</xsl:template>

<xsl:template match="book">
 <tr>
 <xsl:if test="position() mod 2"><xsl:attribute name="style">background:#e0e0e0</xsl:attribute></xsl:if>
  <td><xsl:apply-templates select="pubdate"/></td>
  <td><xsl:apply-templates select="title"/></td>
  <td><xsl:apply-templates select="publisher"/></td>
  <td><xsl:apply-templates select="pubplace"/></td>
 </tr>
</xsl:template>

<xsl:template match="pubdate">
 <xsl:apply-templates/>
</xsl:template>

<xsl:template match="title">
 <xsl:apply-templates/>
</xsl:template>

<xsl:template match="publisher">
 <xsl:apply-templates/>
</xsl:template>

<xsl:template match="pubplace">
 <xsl:apply-templates/>
</xsl:template>

<xsl:template match="lived">
 <xsl:apply-templates/>
</xsl:template>

<xsl:template match="a">
<xsl:variable name="zahl" select="substring-after(@name,'page') - 1"/>
 <a href="../../bin/xsl/showpic.htm?url=http://www.gaga.net/pgproj/6195d71c/&amp;nr={$zahl}" target="_blank"><img src="../../pic/seite.gif" alt="Scan anzeigen" title="Scan anzeigen" border="0"/></a>
</xsl:template>

<!-- alle nicht explizit angegebenen Tags werden ignoriert -->

<xsl:template match="*"/>
</xsl:stylesheet>

<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">


<xsl:output
	method="html"
	encoding="ISO-8859-1"
	indent="yes"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"/>

<xsl:variable name="isgaga" select="/html/head/meta[@name='projectid']/@content" />
<xsl:variable name="title"  select="/html/head/meta[@name='title']/@content" />
<xsl:variable name="author"  select="/html/head/meta[@name='author']/@content" />
<xsl:variable name="type"  select="/html/head/meta[@name='type']/@content" />
<xsl:variable name="pnd"  select="/html/head/meta[@name='pnd']/@content" />
<xsl:variable name="authname"  select="/html/body/*/h2[@class='name']/text()" />

<xsl:template match="/">
 <xsl:apply-templates />
</xsl:template>

<xsl:template match="html">
<html>
  <xsl:apply-templates select="head"/>
 <body>
 

<table align="center" class="center" width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td>

   <table align="center" class="center" width="100%" height="61" cellspacing="0" cellpadding="0" border="0">
      <tr>
        <td align="center" class="autalpha">
          <img src="../../info/pic/gb_800.jpg" border="0" alt="Projekt Gutenberg-DE, Edition 14" title="Projekt Gutenberg-DE, Edition 14"/>
        </td>
      </tr>
      <tr>
        <td>
          <table cellspacing="0" cellpadding="0" width="100%" height="20" align="center">
            <tr>
              <td class="mainnav">
                <a href="../../info/texte/index.xml">Startseite</a>
              </td>
			  <td class="mainnav">|</td>
              <td align="center" class="mainnav">
                <a href="../../info/texte/info.xml">Information</a>
              </td>
			  <td class="mainnav">|</td>
              <td align="center" class="mainnav">
                <a href="../../info/texte/allw-aut.xml">Alle Werke</a>
              </td>
			  <td class="mainnav">|</td>
			  <td align="center" class="mainnav">
                <a href="../../info/texte/belletri.xml">Belletristik</a>
              </td>	
			  <td class="mainnav">|</td>
			  <td align="center" class="mainnav">
                <a href="../../info/texte/sachbuch.xml">Sachbuch</a>
              </td>	
			  <td class="mainnav">|</td>		  		  
               <td align="center" class="mainnav">
                <a href="../../info/texte/neu-ed14.xml">Neu: Ed. 14</a>
              </td>
			  <td class="mainnav">|</td>
			  <td align="center" class="mainnav">
                <a href="../../info/lesetips/lesetips.xml">Lesetips</a>
              </td>
			  <td class="mainnav">|</td>
			  <td align="center" class="mainnav">
                <a href="../../info/texte/verlag.xml">Verlag</a>
              </td>
			  <td class="mainnav">|</td>
              <td align="center" class="mainnav">
                <a href="../../info/texte/impress.xml">Impressum</a>
              </td>
             </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td>
          <table width="100%" border="0" align="center" bgcolor="#eeeeee">
            <tr>
              <td align="center">
                Autoren:
              </td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-a.xml">A</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-b.xml">B</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-c.xml">C</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-d.xml">D</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-e.xml">E</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-f.xml">F</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-g.xml">G</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-h.xml">H</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-i.xml">I</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-j.xml">J</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-k.xml">K</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-l.xml">L</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-m.xml">M</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-n.xml">N</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-o.xml">O</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-p.xml">P</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-q.xml">Q</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-r.xml">R</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-s.xml">S</a>
              </td>

             <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-t.xml">T</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-u.xml">U</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-v.xml">V</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-w.xml">W</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-x.xml">X</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-y.xml">Y</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-z.xml">Z</a>
              </td>
              <td class="trenner">*</td>
              <td align="center" class="autalpha">
                <a href="../../autoren/info/autor-az.xml">Alle Autoren</a>
              </td>
              <td>&#160;</td>
			     <td class="right" align="right" height="24"><img src="../../pic/meta.gif" title="Quellenangabe" align="absmiddle" border="0" onclick="ShowMeta()" onmouseout="HideMeta()"/></td><!-- onmouseover geht nicht wg popupblocker -->

            </tr>
          </table>
        </td>
      </tr>
    </table>
</td>
</tr>

 </table>
<br clear="all"/>

  <xsl:apply-templates select="body"/>
</body>
</html>

</xsl:template>

<xsl:template match="head">
 <xsl:apply-templates />
 <script type="text/javascript" src="../../js/showmeta.js"/>
</xsl:template>

<xsl:template match="body">

<div id="foo">
<xsl:apply-templates />
</div>

  <xsl:if test="($type='authinfo' and not($pnd = ''))">
  <hr size="1"/>
  <!-- <p>Werke im Katalog der <a href="http://dispatch.opac.d-nb.de/DB=4.1/REL?PPN={$pnd}" target="_blank">Deutschen Nationalbibiothek</a></p> -->
  </xsl:if>
</xsl:template>

<xsl:template match="table">
<div align="center">
<xsl:copy>
 <xsl:apply-templates select="@*|node()" />
</xsl:copy>
</div>
</xsl:template>

<xsl:template match="*[@class='footnote']">
 <xsl:variable name="myid" select="generate-id()"/>
  <span class="footnote" id="{$myid}">
   <a href="#{$myid}">[<sup><b>*</b></sup>]
   <span class="fntext"><xsl:apply-templates/></span>
  </a>
  </span>
</xsl:template>





<xsl:template match="processing-instruction('xml-stylesheet')"/>

<!-- identity transform -->

<xsl:template match="@*|node()">
 <xsl:copy>
  <xsl:apply-templates select="@*|node()" />
 </xsl:copy>
</xsl:template>

</xsl:stylesheet>


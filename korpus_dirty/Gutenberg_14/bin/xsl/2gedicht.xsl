<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output
	method="html"
	indent="yes"
	encoding="ISO-8859-1"/>


<xsl:variable name="file1" select="/merge/file1/@src"/>
<xsl:variable name="file2" select="/merge/file2/@src"/>

<xsl:template match="/">
 <xsl:apply-templates/>
</xsl:template>


<xsl:template match="merge">
<xsl:variable name="count1"  select="count(document($file1)//p)" />
<xsl:variable name="count2"  select="count(document($file2)//p)" />
<html>
<head>
<link rel="stylesheet"	type="text/css"	href="../../css/prosa.css" />
<style>
body		{font-family:times; font-size:-1}
p		{margin-top:0; margin-bottom:0}
td		{padding:0.5em;vertical-align:top; font-family:arial; font-size:10pt}
</style>
</head>
<body>
<xsl:if test="not($count1 = $count2)">
 <p class="lektorat">Keine Korrespondenz!<br/>
  <xsl:value-of select="$file1" /> hat <xsl:value-of select="$count1" /> Abs�tze<br/>
  <xsl:value-of select="$file2" /> hat <xsl:value-of select="$count2" /> Abs�tze<br/>
 </p>
</xsl:if>

<table border="0" width="100%">
<tr><th colspan="2"><xsl:apply-templates select="document($file1)//*[@class='author']"/></th></tr>
<tr><td width="50%">
<xsl:apply-templates select="document($file1)//*[@class='title']"/>
<xsl:apply-templates select="document($file1)//*[@class='subtitle']"/>
</td>
<td>
<xsl:apply-templates select="document($file2)//*[@class='title']"/>
<xsl:apply-templates select="document($file2)//*[@class='subtitle']"/>
</td></tr>
<xsl:for-each select="document($file1)//p">
<tr><td>
 <xsl:apply-templates/>
</td>
<td>
 <xsl:call-template name="other">
  <xsl:with-param name="pos" select="position()"/>
 </xsl:call-template>
</td>
</tr>
</xsl:for-each>
</table>
</body>
</html>
</xsl:template>

<xsl:template name="other">
 <xsl:param name="pos" />
 <xsl:for-each select="document($file2)//p">
  <xsl:if test="position()=$pos">
   <xsl:apply-templates />
  </xsl:if>
 </xsl:for-each>
</xsl:template>


<xsl:template match="h3">
 <h3><xsl:apply-templates /></h3>
</xsl:template>

<xsl:template match="h2">
 <h2><xsl:apply-templates /></h2>
</xsl:template>

<xsl:template match="a[@href]">
 <xsl:apply-templates />
</xsl:template>

<xsl:template match="*">
 <xsl:copy-of select="."/>
</xsl:template>

</xsl:stylesheet>

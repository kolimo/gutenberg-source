<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!--
20071028	created
 -->

<xsl:output
	method="xml"
	encoding="ISO-8859-1"
	indent="yes"/>

<!-- Parameter werden von aufrufendem Programm gesetzt -->
<xsl:param name="file"><xsl:value-of select="/book/file/@name"/></xsl:param>
<xsl:param name="id"></xsl:param>

<xsl:template match="/">
<html>
 <xsl:apply-templates />
</html>
</xsl:template>

<xsl:template match="book/toc/part">
 <body>
  <xsl:apply-templates select="//*[@id=$id]"/>
 </body>
</xsl:template>

</xsl:stylesheet>


#!/usr/bin/perl

# make list of *.HTM files of current dir (from command line args)
# cmd: perl allhtml.pl arg1 arg2
# out: test.xml
#
# created: 12-04-2002 10:00 - 11:00 (gh)
# bug: test for other non-text XML files than 0htmldir.xml (meta.xml, log.xml,...)

$dir=$ARGV[0];
$ext=$ARGV[1];

open(OUTFILE,">dirlist.xml") || die "Outfile dirlist.xml nicht erzeugt\n";
print OUTFILE '<?xml version="1.0" encoding="ISO-8859-1"?>';
print OUTFILE "\n";
print OUTFILE "<docs>\n";
opendir(DIRNAME,".");
while($fname=readdir(DIRNAME))
	{
	@var=split(/\./,$fname,2);
	$_=$var[1];
	$tmp = $var[0];
	if (/xml/){
		if(($tmp ne "0htmldir") && ($tmp ne "dirlist") && ($tmp ne "build") && ($tmp ne "log") && ($tmp ne "meta") && ($tmp ne "errs")){
			print OUTFILE '  <doc href="';
			print OUTFILE "$var[0].xml";
			print OUTFILE '" />';
			print OUTFILE "\n";
		    };
		};
	};
print OUTFILE "</docs>\n";
closedir(DIRNAME);
close(OUTFILE);

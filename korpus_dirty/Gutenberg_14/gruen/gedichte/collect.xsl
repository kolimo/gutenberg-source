<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!--
	generate collection of files, keep full xhtml content
	created 2006-07-13

usage:	\bin\testxslt -in dirlist.xml -xsl collect.xsl -out collection.xml

20060713	created
		transforms type content to lc
		trasforms class attributes to lc
		xsl:strip-space f�r prettyprinting???
20060801	doc ID="filename"
20060817	fehlendes meta name=title aus title extrahieren

-->

<xsl:strip-space elements="*" />

<xsl:output
	method="xml"
	indent="yes"
	encoding="ISO-8859-1"/>

<xsl:template match="/">
 <xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="../../bin/xsl/showcoll.xsl"</xsl:processing-instruction>
<collection>
 <xsl:apply-templates select="docs/doc" />
</collection>
</xsl:template>

<xsl:template match="doc">
<xsl:variable name="docname" select="substring-before(@href,'.xml')" />
 <doc id="{$docname}">
  <xsl:apply-templates select="document(@href)/html" />
  <xsl:message>processing: <xsl:value-of select="@href"/></xsl:message>
 </doc>
</xsl:template>

<xsl:template match="html">
<html>
  <xsl:apply-templates select="head" />
  <xsl:apply-templates select="body" />
</html>
</xsl:template>

<xsl:template match="meta">
<xsl:variable name="name" select="translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ���','abcdefghijklmnopqrstuvwxyz���')" />
<xsl:variable name="lccontent" select="translate(@content,'ABCDEFGHIJKLMNOPQRSTUVWXYZ���','abcdefghijklmnopqrstuvwxyz���')" />
<xsl:if test="$name='type'"><meta name="{$name}" content="{$lccontent}"/></xsl:if>
<xsl:if test="not($name='type')"><meta name="{$name}" content="{@content}"/></xsl:if>
</xsl:template>

<xsl:template match="head">
<head>
 <xsl:variable name="title" select="title/text()" />
 <xsl:if test="not(meta[@name='title'])">
  <meta name="title" content="{$title}" />
  <xsl:message>Titel in Meta eingef�gt</xsl:message>
 </xsl:if>
 <xsl:apply-templates />
</head>
</xsl:template>

<xsl:template match="body">
<body>
  <xsl:apply-templates />
</body>
</xsl:template>

<xsl:template match="@class">
 <xsl:attribute name="class">
  <xsl:value-of select="translate(.,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')" />
 </xsl:attribute>
</xsl:template>

<xsl:template match="@rel">
 <xsl:attribute name="rel">
  <xsl:value-of select="translate(.,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')" />
 </xsl:attribute>
</xsl:template>

<xsl:template match="img[@src='../../pic/fn.gif']">
 <span class="footnote"><xsl:value-of select="@alt" /></span>
</xsl:template>

<xsl:template match="*|@*|processing-instruction()|comment()|text()">
  <xsl:copy>
    <xsl:apply-templates select="*|@*|text()|processing-instruction()|comment()" />
  </xsl:copy>
</xsl:template>

</xsl:stylesheet>


<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!--	
	usage: \gutdvd\bin\xslt\testxslt -param path '{path}' -in {file}.xml -xsl \gutdvd\bin\xsl\tocbyid.xsl -out toc.xml
	(quotes f�r infile sind wichtig!)

	Es w�re besser, in den Dokumenten die �berschriften von Kapiteln, Unterkapiteln
	zu markieren, z.B. <hn class="title">

20071029	path und infile aus meta.xml? Stylesheet muss dann aber in diesem Verzeichnis liegen
20071024	ben�tigt filename als param, um IDs zu generieren, link auf id im Dokument
20060709	Anpassung f�r XML
20050909	mehrfache subtitles erlauben
20050907	h5,h6 wird f�r Kapitel�berschriften ignoriert
20050416	arbeitet nur auf div.chapter

 -->

<xsl:output
	method="xml"
	encoding="ISO-8859-1"
	indent="yes" />

<xsl:param name="path"/>

<xsl:template match="/">
 <xsl:apply-templates/>
</xsl:template>

<xsl:template match="head">
<head>
 <title>Inhaltsverzeichnis</title>
 <meta name="generator" content="tocbyid" />
 <link rel="stylesheet"	type="text/css"	href="../../css/prosa.css"/>
 <xsl:apply-templates select="meta"/>
</head>
</xsl:template>

<xsl:template match="body">
<body>
 <div class="toc" id="toc">
 <h3 class="author"><xsl:value-of select="//*[@class='author']" /></h3>
 <h2 class="title"><xsl:value-of select="//*[@class='title']" /></h2>
 <xsl:apply-templates select="*[@class='subtitle']" />
 <h3 class="translator"><xsl:value-of select="//*[@class='translator']" /></h3>
 <h3>Inhalt</h3>
  <ul>
   <xsl:apply-templates select="div" />
  </ul>
 </div>
</body>
</xsl:template>

<!--	exclude some div classes from building table of content
	you may add or remove other div classes
-->


<xsl:template match="div[@class='figure']" />
<!--xsl:template match="div[@class='titlepage']" /-->
<xsl:template match="div[@class='toc']" />
<xsl:template match="div[@class='vers']" />

<!--	if footnote appears in headline(s) -->

<xsl:template match="span[@class='footnote']" />

<xsl:template match="div[@class='chapter']">
   <li><a href="http://gunti.dyndns.org/archiv/bin/php/xmlbyid.php?path={$path}&amp;id={@id}">
     <xsl:apply-templates select="h1[1] | h2[1] | h3[1] | h4[1]" />
<!--
     <xsl:if test="./div">
      <ul>
       <xsl:apply-templates select="div[@class='chapter']" />
      </ul>
     </xsl:if>
-->
     </a>
   </li>
</xsl:template>

<xsl:template match="meta[@name='type']">
 <meta name="type"	content="toc"/>
</xsl:template>

<xsl:template match="h1 | h2 | h3 | h4">
 &#160;<xsl:apply-templates/>
</xsl:template>

<xsl:template match="*[@class='subtitle']">
 <h3 class="subtitle"><xsl:apply-templates/></h3>
</xsl:template>

<!-- �ndere alte PI -->

<xsl:template match="processing-instruction('xml-stylesheet')">
<xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="../../bin/xsl/xml2htm.xsl"</xsl:processing-instruction>
</xsl:template>

<xsl:template match="@*|node()">
<xsl:copy>
 <xsl:apply-templates select="@*|node()" />
</xsl:copy>
</xsl:template>

</xsl:stylesheet>

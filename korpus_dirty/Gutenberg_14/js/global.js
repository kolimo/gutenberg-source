/*
20100917	var Meta = window; (f�r MSIE 8.0 Fehler)
		function hilite geht nicht in MSIE, da find() nicht implementiert
20100321	Referenzen auf gunti.dyndns.org entfernt
20070719	showmeta.js integriert
20070715	globale Funktionen f�r Korrekturinterface
		Variable newurl nur zum Testen, mu� sp�ter auf gutenberg.spiegel.de zeigen
Bugs:
20100321	feste URL in keyword() fehlt
20071120	#-Symbol und PHP-Script ergeben falschen Pfad

2do:
20100321	phptoc(url) l�schen?
*/

function getRange () {
  range="";
  if (window.getSelection) {
    range=window.getSelection();
  } else if (document.getSelection) {
    range=document.getSelection();
  } else if (document.selection) {
    range=document.selection.createRange().text;
  }
  return range;
}

isopen = false;
search = window.location.search;

/* 	bild, document.all.onerr und document.getElementById("onerr") sind in IE undefined
   	(wie ist richtige DOM-Programmierung? Abfrage umdrehen?)
	im IE erscheint jetzt keine Fehlermeldung, aber icon ist vorhanden
*/

/* Node was not found exception
if(!search){
  var bild = document.getElementById("onerr");
  if (bild){document.getElementById("errpic").removeChild(bild);}
};
*/

/* 	Gartenzaun macht Probleme bei decodeURI, hier schon l�schen?
	gleiches gilt f�r URL auf PHP-Script (Pfad zeigt dann auf /bin/php/xmlbyid.php)
 */
function korr()
{
url=document.URL.split("?");
pfad = url[0];
sel=getRange();
tmp=pfad.split("/archiv");
newurl=tmp[0]+"/archiv/info/korrektu/korrtext.htm?p="+tmp[1]+"&w="+sel;
newurl=encodeURI(newurl);
// alert(newurl);
if (sel==""){alert('Sie m�ssen erst den fehlerhaften Text selektieren');}
 else
   if (!isopen){
   MeinFenster = window.open(newurl, "Zweitfenster", "width=800,height=600,left=100,top=200,scrollbars=yes");
   MeinFenster.focus();
   isopen=true;}
   else MeinFenster.focus();
}

/*
	Vorsicht! Die URL mu� fest auf den Server zeigen, da PHP-Skript
*/
function keyword()
{
url=document.URL.split("?");
pfad = url[0];

//	php akzeptiert kein Hashmark "#" in der URL

chapid=window.location.hash;
if (chapid > "") {chapid="&id="+chapid.substr(1)};
tmp=pfad.split("/archiv");
p=tmp[1];
//  alert(p);
newurl=tmp[0]+"/archiv/info/wissen/showkey.php?p="+p+chapid;
newurl=encodeURI(newurl);
// alert(newurl);
   if (!isopen){
   MeinFenster = window.open(newurl, "Zweitfenster", "width=600,height=400,left=100,top=200,scrollbars=yes,resizable=yes");
   MeinFenster.focus();
   isopen=true;}
   else MeinFenster.focus();
}

function keyadd()
{
url=document.URL.split("?");
pfad = url[0];
/*		"file:///c:/archiv/" (17)
	oder	"http://gunti.dyndns.org/archiv/bin/php/xmlbyid.php?path="
	oder	"http://gunti.dyndns.org/archiv/" (30)

	php akzeptiert kein Hashmark "#" in der URL
*/
chapid=window.location.hash;
if (chapid > "") {chapid="&id="+chapid.substr(1)};
tmp=pfad.split("/archiv");
p=tmp[1];
// alert(url[1]);
newurl=tmp[0]+"/archiv/info/wissen/addfree.htm?p="+p+chapid;
newurl=encodeURI(newurl);
// alert(newurl);
   if (!isopen){
   MeinFenster = window.open(newurl, "Zweitfenster", "width=600,height=400,left=100,top=200,scrollbars=yes,resizable=yes");
   MeinFenster.focus();
   isopen=true;}
   else MeinFenster.focus();
}

// find nicht im MSIE implementiert

function hilite(){
   thestring = window.location.search.substring(1);
   what = unescape(thestring);
   
   if (what != "") {
    if (window.find) find(what,false,false);
   }
}

var Meta = window;

function ShowMeta(){
 Meta = window.open("", "Quellenangaben", "width=300,height=400,scrollbars=no");
 Meta.document.writeln('<html><head><title></title></head><body style="width:300px;font-size:8pt;font-family:sans-serif">');
 Meta.document.writeln('<div align="center">Quellenangabe</div>');
 Meta.document.writeln('<table cellpadding="0" cellspacing="2" style="font-size:8pt;font-family:sans-serif">');
 Meta.document.writeln('<tr><th>Name</th><th>Wert</th></tr>');
 anz = document.getElementsByTagName("meta").length;
 for (var i=0; i < anz;i++){
  n = document.getElementsByTagName("meta")[i].getAttribute("name",false);
  v = document.getElementsByTagName("meta")[i].getAttribute("content",false);
  if (v){Meta.document.writeln('<tr><td>',n,'</td><td>',v,'</td></tr>');}
  };
 Meta.document.writeln('</table></body></html>');
 Meta.document.close();
 }

function HideMeta(){Meta.close();}

/*
	Anzeige der Kinder des Body-Elements
	im XSLT-Stylesheet ist der Content in <div class="foo"> gekapselt,
	da sonst auch die per XSLT eingef�gten Elemente angezeigt w�rden.
	*** ersetzt durch eigenes Fenster ***

function ShowStruct()
{
msg='BODY-Kindelemente: \n\n';
 var Knoten = document.getElementById("foo");
 var Anzahl = Knoten.childNodes.length;
 Knoten = Knoten.firstChild;
 while (Knoten != null) {
  if (Knoten.nodeType == 1) {
	att = Knoten.getAttribute("class");
	msg = msg + Knoten.nodeName;
	if (att != null) {msg = msg +"."+att;};
	msg = msg + "\n";
  };
 Knoten = Knoten.nextSibling;
}
alert(msg+"\n");
}
*/

function DOMToc(){
 var insertpoint = document.getElementById("gentoc");
 document.getElementById("gentoc").replaceChild(FindToc(),document.getElementById("gentoc").firstChild);
}

function ShowTOC(){
 var newTOC = window.open("", "TOC", "width=400,height=600,scrollbars=yes");
 theToc = FindToc().cloneNode(true);
 printTOC(theToc,newTOC);
}

function phptoc(url){
alert("call ../../bin/php/phptoc.php?p="+url);
}

function ShowStruct(){
 var Struct = window.open("", "Strukturangaben", "width=400,height=600,scrollbars=yes");
 printDOMTree(document.getElementById('foo'),Struct);
}

function traverseDOMTree(targetDocument, currentElement, depth)
{
  if (currentElement)
  {
    var j;
    var att;
    var tagName=currentElement.nodeName;
    // Prints the node tagName, such as <A>, <IMG>, etc
    if (tagName == 'BODY' || tagName == 'DIV' || tagName == 'H1' || tagName == 'H2' || tagName == 'H3' || tagName == 'H4' || tagName == 'TABLE')
      {
    for (j=0; j<depth-1; j++){targetDocument.write("&nbsp;&nbsp;");};			
        targetDocument.write(currentElement.tagName);
        att = currentElement.getAttribute("class");
	if (att != null) {targetDocument.write(" class="+att);};
        att = currentElement.getAttribute("id");
	if (att != null) {targetDocument.write(" id="+att);};
        targetDocument.write("<BR>\n");
    // Traverse the tree
    var i=0;
    var currentElementChild=currentElement.childNodes[i];
    while (currentElementChild)
    {
      // Formatting code (indent the tree so it looks nice on the screen)
      if (currentElementChild.tagName){
      // Recursively traverse the tree structure of the child node
      traverseDOMTree(targetDocument, currentElementChild, depth+1);}
      i++;
      currentElementChild=currentElement.childNodes[i];
    }
  }
 }
}


function printDOMTree(domElement, destinationWindow)
{
  // Use destination window to print the tree.  If destinationWIndow is
  //   not specified, create a new window and print the tree into that window
  var outputWindow=destinationWindow;
  if (!outputWindow)
    outputWindow=window.open();

  // make a valid html page
  outputWindow.document.open("text/html", "replace");
  outputWindow.document.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"  "http://www.w3.org/TR/html4/loose.dtd">\n');
  outputWindow.document.write("<html><head><title>Dokumentstruktur</title></head><body>\n");
  outputWindow.document.write("<code>\n");
  traverseDOMTree(outputWindow.document, domElement, 1);
  outputWindow.document.write("</code>\n");
  outputWindow.document.write("</body></html>\n");
  
  // Here we must close the document object, otherwise Mozilla browsers 
  //   might keep showing "loading in progress" state.
  outputWindow.document.close();
}
/* 	Microsoft IE meldet Fehler "Bezeichner erwartet in Zeile xxx/32"
	MSIE kennt keine attributes.class
 */
function FindToc(){

var tables = document.getElementsByTagName("table");
hasTOC = 0;

if (tables.length > 0)
  {
  for (var i=0; i < tables.length;i++)
     {if (tables[i].attributes.length > 0) {
      if (tables[i].attributes["class"] && tables[i].attributes["class"].nodeValue == "toc") return tables[i];}
     }
  }

/* if not hasTOC */

divs = document.getElementsByTagName("div");
if (divs.length > 0)
  {
  for (var i=0; i < divs.length;i++)
     {if (divs[i].attributes.length > 0) {
      if (divs[i].attributes["class"] && (divs[i].attributes["class"].nodeValue == "toc")) return divs[i];}
     }
  }


var newTOC   = document.createElement("div");
/*
var header   = document.createElement("h3");
var headtext = document.createTextNode("Generiertes Inhaltsverzeichnis");
header.appendChild(headtext);
newTOC.appendChild(header);
*/

/* check meta for type=toc */
metas = document.getElementsByTagName("meta");
 if (metas.length > 0)
  {
   for (var i=0; i < metas.length;i++)
    {
     if (metas[i].attributes["content"].nodeValue == "toc")
      {
       var para = document.createElement("p");
       para.appendChild(document.createTextNode("Dieses Dokument ist bereits ein Inhaltsverzeichnis"));
       newTOC.appendChild(para);
       var body = document.getElementById("foo");
       newTOC.appendChild(body);
       return newTOC;
      }
    }
 }

/*	create DOM element with TOC from content
	2do:	if no H-Element found, then "ohne Kapitel�berschrift" oder "chapid"
*/

//alert("FindToc failed");
divs  = document.getElementsByTagName("div");
count = 0;

if (divs.length > 0)
  {
   for (var i=0; i < divs.length;i++)
     {if (divs[i].attributes.length > 0)
       {if (divs[i].attributes["class"] && (divs[i].attributes["class"].nodeValue == "chapter"))
          {
	   var chapid = divs[i].attributes["id"].nodeValue;
	   var para = document.createElement("div");

	   /* find headline of chapter */

	   var childs = divs[i].childNodes.length;
	   for (var j=0; j < childs; j++){
	     var tag = divs[i].childNodes[j];

/*	2do: 	handling of h2, exit if first h3 found
	done:	do not create, if h3 element has class
		fails, if h3 has more than one #text child node
 */

	     if ((tag.nodeName == "H2" || tag.nodeName == "H3") && !tag.attributes["class"]) {
	       var anchor = document.createElement("a");
	       anchor.setAttribute("href","#"+chapid);
//               anchor.appendChild(document.createTextNode(tag.firstChild.data));
	 	anchor.appendChild(document.createTextNode(""));
		for (var k=0; k < tag.childNodes.length;k++){
		 anchor.firstChild.appendData(tag.childNodes[k].nodeValue);
//		 alert(tag.childNodes[k].nodeValue);
		}
	       para.appendChild(anchor);
	       newTOC.appendChild(para);
	       }
             };
	   count=count+1;
          }
      }
     }
  }

  if (count == 0){
    var para = document.createElement("p");
    para.appendChild(document.createTextNode("Dieses Dokument hat keine Kapitel"));
    newTOC.appendChild(para);
  }

return newTOC;
}

function printTOC(domElement, destinationWindow)
{
  // Use destination window to print the tree.  If destinationWIndow is
  //   not specified, create a new window and print the tree into that window
  var outputWindow=destinationWindow;
  if (!outputWindow)
    outputWindow=window.open();

  // make a valid html page
  outputWindow.document.open("text/html", "replace");
  outputWindow.document.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"  "http://www.w3.org/TR/html4/loose.dtd">\n');
  outputWindow.document.write("<html>\n<head>\n");
  outputWindow.document.write("<base href='",window.location.href,"'>\n");
  outputWindow.document.write('<link rel="stylesheet" type="text/css" href="../../css/prosa.css">\n');
  outputWindow.document.write("<title>Inhalt</title>\n</head>\n<body>\n");
  printChilds(outputWindow.document, domElement);
  outputWindow.document.write("</body></html>\n");
  
  // Here we must close the document object, otherwise Mozilla browsers 
  //   might keep showing "loading in progress" state.
  outputWindow.document.close();
}

function printChilds(targetDocument, currentElement)
{
  if (currentElement)
  {
    var j;
    var att;
    var tagName=currentElement.nodeName;
        targetDocument.write("<"+currentElement.tagName);
// besser: alle Attribute gleich behandeln
        att = currentElement.getAttribute("class");
	if (att != null) {targetDocument.write(" class="+att);};
        att = currentElement.getAttribute("id");
	if (att != null) {targetDocument.write(" id="+att);};
        att = currentElement.getAttribute("href");
	if (att != null) {targetDocument.write(" href="+att+" target=parent");};
        targetDocument.write(">");
    // Traverse the tree
    var i=0;
    var currentElementChild=currentElement.childNodes[i];
    while (currentElementChild)
    {
      // Formatting code (indent the tree so it looks nice on the screen)
      if (currentElementChild.tagName){
      // Recursively traverse the tree structure of the child node
      printChilds(targetDocument, currentElementChild);}
       else if(currentElement.firstChild.data) targetDocument.write(currentElement.firstChild.data);
      i++;
      currentElementChild=currentElement.childNodes[i];
    }
    targetDocument.write("</"+currentElement.tagName+">\n");
 } 
}

function insertpic(id,projid,page){
 var pageurl,url="";
 if (page < 10) pageurl="000"+page
  else if (page < 100) pageurl="00"+page
   else pageurl ="0"+page;
 url="http://www.gaga.net/pgproj/"+projid+"/"+pageurl+".png";
 var oldimage=document.getElementById(id);
 oldimage.setAttribute("src",url);
 oldimage.setAttribute("width","500");
 var align = document.createAttribute("align");
 align.nodeValue = "top";
 oldimage.setAttributeNode(align);
}

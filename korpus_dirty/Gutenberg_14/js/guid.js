/* 	Aufruf von guid erzeugt aus Zufallszahlen einen String,
	der wie eine GUID formatiert ist. Reicht f�r die meisten
	Anwendungen aus, ist aber im strengen Sinne keine GUID,
	da nicht an den Rechner gebunden
*/

function S4() {
   return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
}
function guid() {
   return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}


/*    path2move Javascript
   f�r Android

besser: komplett Parsen und Array liefern?
   "M 173 29L176 29C..." kommt auch vor

2do:	fehlende Befehle aus SVG wie?
	"H,h" : Horizontale Linie x
	"T,t" : smooth quad Bezier to x y
	"V,v" : vertikale Linie y
	"A,a" :	elliptic arc	(7 Argumente)
sind nicht in Android Path einfach implementierbar

	Tabs und Space als Delimiter abfangen
*/

s="m173 29 l176 29 c176 32.93 184.7 30.92 180 33 Q176.9 33.65 175.9 30.07 C173.9 29.92 168.9 29.15 173 29 Z";

function path2move(){
  document.writeln("Path: ",s+"<br>");
  parseString();
}

function parseString(){
 while (s.length > 0) {
  getCommand();
 }
}

function getCommand(){
ch=s.charAt(0);
while (ch == " "){s = s.substring(1); ch = s.charAt(0)};
s = s.substring(1);

 switch (ch) {
   case "M":
    x = getNumber();
    y = getNumber();
    document.writeln("moveTo("+x+","+y+");<br>");
    break;

   case "m":
    x = getNumber();
    y = getNumber();
    document.writeln("rMoveTo("+x+","+y+");<br>");
    break;

   case "L":
    x = getNumber();
    y = getNumber();
    document.writeln("lineTo("+x+","+y+");<br>");
    break;

   case "l":
    x = getNumber();
    y = getNumber();
    document.writeln("rLineTo("+x+","+y+");<br>");
    break;

   case "Q":
    x1 = getNumber();
    y1 = getNumber();
    x2 = getNumber();
    y2 = getNumber();
    document.writeln("quadTo("+x1+","+y1+","+x2+","+y2+");<br>");
    break;

   case "q":
    x1 = getNumber();
    y1 = getNumber();
    x2 = getNumber();
    y2 = getNumber();
    document.writeln("rQuadTo("+x1+","+y1+","+x2+","+y2+");<br>");
    break;

   case "C":
    x1 = getNumber();
    y1 = getNumber();
    x2 = getNumber();
    y2 = getNumber();
    x3 = getNumber();
    y3 = getNumber();
    document.writeln("cubicTo("+x1+","+y1+","+x2+","+y2+","+x3+","+y3+");<br>");
    break;

   case "c":
    x1 = getNumber();
    y1 = getNumber();
    x2 = getNumber();
    y2 = getNumber();
    x3 = getNumber();
    y3 = getNumber();
    document.writeln("rCubicTo("+x1+","+y1+","+x2+","+y2+","+x3+","+y3+");<br>");
    break;

   case "z","Z":
     document.writeln("close();");
    break;

   default:
    document.writeln(ch+": not implemented<br>"); /* skip all until "A".."Z" (command) */
    break;
 }
}

function getNumber(){
sn="";
ch=s.charAt(0);
/* skip blanks */
 while (ch == " "){s = s.substring(1); ch = s.charAt(0)};
 while (numdigits(ch)) {
  sn = sn+ch;
  s = s.substring(1);
  ch = s.charAt(0);
 };
// document.writeln("number: "+sn+"<br>");
return parseFloat(sn);
}

function numdigits(ch){
  if ((ch >= "0" && ch <= "9") || ch == ".") {return true}
   else return false
}


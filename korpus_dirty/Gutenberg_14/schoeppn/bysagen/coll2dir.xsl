<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0"
    xmlns:lxslt="http://xml.apache.org/xslt"
    xmlns:redirect="org.apache.xalan.lib.Redirect"
    extension-element-prefixes="redirect">

<xsl:output
	method="xml"
	encoding="ISO-8859-1"
	indent="yes" />

<!--	erzeuge 0htmldir.xml aus collection.xml

Aufruf:	\gutenb\bin\xalan\testxalan -in collection.xml -xsl coll2dir.xsl -out 0htmldir.xml

20070223	erstellt


2do:			

-->

<xsl:template match="/">
<xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="../../bin/xsl/xml2htm.xsl"</xsl:processing-instruction>
<html>
  <head>
   <link rel="stylesheet" type="text/css" href="../../css/prosa.css"/>
   <meta name="type" content="toc"/>
   <meta name="generator" content="coll2dir.xsl"/>
   <meta name="author" content="hille@abc.de"/>
   <meta name="created" content="20070223"/>
  </head>
  <body>
   <h4><xsl:value-of select="count(collection/doc)"/> vorhandene Texte, sortiert nach Titel</h4>
    <ul>
      <xsl:apply-templates select="/collection/doc" >
      <xsl:sort select="html/head/title" />
      </xsl:apply-templates>
    </ul>
  </body>
</html>
</xsl:template>

<xsl:template match="doc">
 <xsl:variable name="file" select="concat(@id,'.xml')"/>
 <li>
  <a href="{$file}"><xsl:value-of select="html/head/title" /></a>
 </li>
</xsl:template>

<xsl:template match="img"/>

<xsl:template match="@*|node()">
 <xsl:copy>
  <xsl:apply-templates select="@*|node()" />
 </xsl:copy>
</xsl:template>

</xsl:stylesheet>

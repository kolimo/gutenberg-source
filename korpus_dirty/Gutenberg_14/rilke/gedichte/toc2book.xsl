<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!--
	create book from file with "a href="

usage:	\gutenb\bin\testxslt -in {tocfile}.xml -xsl \gutenb\bin\xsl\toc2book.xsl -out book.xml

-->

<xsl:output
	method="xml"
	indent="yes"
	encoding="ISO-8859-1"/>

<xsl:template match="/">

 <xsl:apply-templates />

</xsl:template>

<xsl:template match="html">
<html>
 <xsl:apply-templates select="head"/>
 <body>
 <xsl:apply-templates select="body/*[@class='author']"/>
 <xsl:apply-templates select="body/*[@class='title']"/>
 <xsl:apply-templates select="body/*[@class='subtitle']"/>
 <xsl:apply-templates select="body"/>
 </body>
</html>
</xsl:template>

<xsl:template match="body">
 <xsl:for-each select="//a[@href]">
  <xsl:call-template name="process" />
 </xsl:for-each>
</xsl:template>

<xsl:template name="process">
<xsl:variable name="file" select="concat(substring-before(@href,'.xml'),'.xml')"/>
<div class="chapter" id="{generate-id()}">
<xsl:apply-templates select="document($file)/html/body/*"/>
</div>
</xsl:template>

<xsl:template match="*|@*|processing-instruction()|comment()|text()">
  <xsl:copy>
    <xsl:apply-templates select="*|@*|text()|processing-instruction()|comment()" />
  </xsl:copy>
</xsl:template>

</xsl:stylesheet>


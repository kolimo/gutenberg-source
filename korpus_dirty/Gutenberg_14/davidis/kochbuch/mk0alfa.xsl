<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output
	method="xml"
	encoding="ISO-8859-1"
	indent="yes" />

<!--	create 0alfadir from document
	version 1.00 (only valid for "Henriette Davidis Kochbuch")

20070422	modified to XML output

	usage: testxslt -in kochbuch.xml -xsl mk0htmld.xsl -out 0alfadir.xml

-->

<xsl:template match="/">
  <xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="../../bin/xsl/xml2htm.xsl"</xsl:processing-instruction>
  <xsl:apply-templates />
</xsl:template>

<xsl:template match="html">
<html>
 <head>
<title>Henriette Davidis - Praktisches Kochbuch: Inhaltsangabe</title>
<link rel="stylesheet" type="text/css" href="../../css/prosa.css" />
<meta name="type"	content="toc" />
<meta name="author"	content="Henriette Davidis" />
<meta name="sender"	content="hille@abc.de" />
<meta name="created"	content="20070422" />
<meta name="generator"	content="mk0alfa.xsl" />
 </head>
<body>
<h2>Henriette Davidis: Praktisches Kochbuch</h2>
<p>Zur <a href="0htmldir.xml">thematischen Rezeptliste</a></p>
 <xsl:for-each select="*//div[@class='subchapter']">
  <xsl:sort select="h4/text()"/>
  <xsl:apply-templates select="."/>
 </xsl:for-each>
</body>
</html>
</xsl:template>

<xsl:template match="div[@class='subchapter']">
<xsl:variable name="number" select="substring(./@id,2)"/>
<xsl:variable name="name" select="concat('nr',$number)"/>
<br/><a href="{concat($name,'.xml')"><xsl:apply-templates select="./h4"/></a>
</xsl:template>

<xsl:template match="h2 | h3 | h4">
 <xsl:value-of select="./text()"/>
</xsl:template>

<xsl:template match="@*|node()"/>

</xsl:stylesheet>

<?xml version="1.0" encoding="ISO-8859-1"?>
<?xml-stylesheet type="text/xsl" href="../../bin/xsl/xml2htm.xsl"?>
<html>
<head>
  <title>An meinen Sohn Johannes</title>
  <link href="../../css/prosa.css" type="text/css" rel="stylesheet"/>
  <meta name="type"    content="letter"/>
  <meta name="author"  content="Matthias Claudius"/>
  <meta name="title"   content="An meinen Sohn Johannes"/>
  <meta name="sender"  content="utehorn@web.de"/>
  <meta name="created" content="20020924"/>
</head>
<body>
  <div id="meinsohn" class="chapter">
    <h3>Brief von Matthias Claudius an seinen Sohn Johannes</h3>
    <p>An meinen Sohn Johannes, 1799</p>
    <p class="right">Gold und Silber habe ich nicht;<br/>
      was ich aber habe, gebe ich dir.</p>
    <p>Lieber Johannes!</p>
    <p>Die Zeit k�mmt allgemach heran, dass ich den Weg gehen muss, den man nicht wieder k�mmt. Ich kann dich nicht mitnehmen und lasse dich in einer Welt zur�ck, wo guter Rat nicht �berfl�ssig ist.</p>
    <p>Niemand ist weise von Mutterleibe an; Zeit und Erfahrung lehren hier und fegen die Tenne.</p>
    <p>Ich habe die Welt l�nger gesehen als du.</p>
    <p>Es ist nicht alles Gold, lieber Sohn, was gl�nzet, und ich habe manchen Stern vorn Himmel fallen und manchen Stab, auf den man sich verlie�, brechen sehen.</p>
    <p>Darum will ich dir einigen Rat geben und dir sagen, was ich funden habe und was die Zeit mich gelehret hat.</p>
    <p>Es ist nichts gro�, was nicht gut ist; und nichts wahr, was nicht bestehet.</p>
    <p>Der Mensch ist hier nicht zu Hause, und er geht hier nicht von ungef�hr in dem schlechten Rock umher. Denn siehe nur, alle andre Dinge hier mit und neben ihm sind und gehen dahin, ohne es zu wissen; der Mensch ist sich bewusst und wie eine hohe bleibende Wand, an der die Schatten vor�ber gehen. Alle Dinge mit und neben ihm gehen dahin, einer fremden Willk�r und Macht unterworfen, er ist sich selbst anvertraut und tr�gt sein Leben in seiner Hand.</p>
    <p>Und es ist nicht f�r ihn gleichg�ltig, ob er rechts oder links gehe.</p>
    <p>Lass dir nicht weismachen, dass er sich raten k�nne und selbst seinen Weg wisse.</p>
    <p>Diese Welt ist f�r ihn zu wenig, und die unsichtbare siehet er nicht und kennet sie nicht.</p>
    <p>Spare dir denn vergebliche M�he, und dir kein Leid, und besinne dich dein.</p>
    <p>Halte dich zu gut, B�ses zu tun.</p>
    <p>H�nge dein Herz an kein verg�nglich Ding.</p>
    <p>Die Wahrheit richtet sich nicht nach uns, lieber Sohn, sondern wir m�ssen uns nach ihr richten.</p>
    <p>Was du sehen kannst, das siehe, und brauche deine Augen, und �ber das Unsichtbare und Ewige halte dich an Gottes Wort.</p>
    <p>Bleibe der Religion deiner V�ter getreu und hasse die theologischen Kannengie�er.</p>
    <p>Scheue niemand so viel als dich selbst. Inwendig in uns wohnet der Richter, der nicht tr�gt, und an dessen Stimme uns mehr gelegen ist als an dem Beifall der. ganzen Welt und der Weisheit der Griechen und �gypter. Nimm es dir vor, Sohn, nicht wider seine Stimme zu tun; und was du sinnest und vorhast, schlage zuvor an deine Stirne und frage ihn um Rat. Er spricht anfangs nur leise und stammelt wie ein unschuldiges Kind doch wenn du seine Unschuld ehrst, l�set er gemach seine Zunge und wird dir vernehmlicher sprechen.</p>
    <p>Lerne gerne von andern, und wo von Weisheit, Menschengl�ck, Licht, Freiheit, Tugend etc. geredet wird, da h�re flei�ig zu. Doch traue nicht flugs und allerdings, denn die Wolken haben nicht alle Wasser, und es gibt mancherlei Weise. Sie meinen auch, dass sie die Sache h�tten, wenn sie davon reden k�nnen und davon reden. Das ist aber nicht, Sohn. Man hat darum die Sache nicht, dass man davon reden kann und davon redet. Worte sind nur Worte, und wo sie so gar leicht und beh�nde dahin fahren, da sei auf deiner Hut, denn die Pferde, die den Wagen mit G�tern hinter sich haben, gehen langsameren Schritts.</p>
    <p>Erwarte nichts vom Treiben und den Treibern; und wo Ger�usch auf der Gassen ist, da gehe f�rbass.</p>
    <p>Wenn dich jemand will Weisheit lehren, da siehe in sein Angesicht. D�nket er sich noch, und sei er noch so gelehrt und noch so ber�hmt, lass ihn und gehe seiner Kundschaft m��ig. Was einer nicht hat, das kann er auch nicht geben. Und der ist nicht frei, der da will tun k�nnen, was er will, sondern der ist frei, der da wollen kann, was er tun soll. Und der ist nicht weise, der sich d�nket, dass er wisse; sondern der ist weise, der seiner Unwissenheit inne geworden und durch die Sache des D�nkels genesen ist.</p>
    <p>Was im Hirn ist, das ist im Hirn; und Existenz ist die erste aller Eigenschaften.</p>
    <p>Wenn es dir um Weisheit zu tun ist, so suche <i>sie</i> und nicht das deine, und brich deinen Willen und erwarte geduldig die Folgen.</p>
    <p>Denke oft an heilige Dinge und sei gewiss, dass es nicht ohne Vorteil f�r dich abgehe und der Sauerteig den ganzen Teig durchs�uere.</p>
    <p>Verachte keine Religion, denn sie ist dem Geist gemeint, und du wei�t nicht, was unter unansehnlichen Bildern verborgen sein k�nne.</p>
    <p>Es ist leicht zu verachten, Sohn; und verstehen ist viel besser.</p>
    <p>Lehre nicht andre, bis du selbst gelehrt bist.</p>
    <p>Nimm dich der Wahrheit an, wenn du kannst und lass dich gerne ihretwegen hassen; doch wisse, dass deine Sache nicht die Sache der Wahrheit ist, und h�te, dass sie nicht ineinander flie�en, sonst hast du deinen Lohn dahin. Tue das Gute vor dich hin, und bek�mmre dich nicht, was daraus werden wird.</p>
    <p>Wolle nur einerlei, und das wolle von Herzen.</p>
    <p>Sorge f�r Deinen. Leib, doch nicht so, als wenn er deine Seele w�re.</p>
    <p>Gehorche der Obrigkeit, und lass die andern �ber sie streiten.</p>
    <p>Sei rechtschaffen gegen jedermann, doch vertraue dich schwerlich.</p>
    <p>Mische dich nicht in fremde Dinge, aber die deinigen tue mit Flei�.</p>
    <p>Schmeichle niemand, und lass dir nicht schmeicheln. Ehre einen jeden nach seinem Stande, und lass ihn sich sch�men, wenn er's nicht verdient.</p>
    <p>Werde niemand nichts schuldig; doch sei zuvorkommend, als ob sie alle deine Gl�ubiger w�ren.</p>
    <p>Wolle nicht immer gro�m�tig sein, aber gerecht sei immer.</p>
    <p>Mache niemand graue Haare, doch wenn du Recht tust, hast du um die Haare nicht zu sorgen.</p>
    <p>Misstraue der Gestikulation, und geb�rde dich schlecht und recht.</p>
    <p>Hilf und gib gerne, wenn du hast, und d�nke dir darum nicht mehr; und wenn du nicht hast, so habe den Trunk kalten Wassers zur Hand, und d�nke dir darum nicht weniger.</p>
    <p>Tue keinem M�dchen Leides und denke, dass deine Mutter auch ein M�dchen gewesen ist.</p>
    <p>Sage nicht alles, was du wei�t, aber wisse immer, was du sagest.</p>
    <p>H�nge dich an keinen Gro�en.</p>
    <p>Sitze nicht, wo die Sp�tter sitzen, denn sie sind die elendesten unter allen Kreaturen.</p>
    <p>Nicht die fr�mmelnden, aber die frommen Menschen achte und gehe ihnen nach. Ein Mensch, der wahre Gottesfurcht im Herzen hat, ist wie die Sonne, die da scheinet und w�rmt, wenn sie auch nicht redet.</p>
    <p>Tue was des Lohnes wert ist, und begehre keinen. Wenn du Not hast, so klage sie dir und keinem andern.</p>
    <p>Habe immer etwas Gutes im Sinn.</p>
    <p>Wenn ich gestorben bin, so dr�cke mir die Augen zu und beweine mich nicht.</p>
    <p>Stehe deiner Mutter bei und ehre sie so lange sie lebt und begrabe sie neben mir.</p>
    <p>Und sinne t�glich nach �ber Tod und Leben, ob du es finden m�chtest, und habe einen freudigen Mut; und gehe nicht aus der Welt, ohne deine Liebe und Ehrfurcht f�r den Stifter des Christentums durch irgendetwas �ffentlich bezeuget zu haben.</p>
    <p class="right">Dein treuer Vater.</p>
  </div>
</body>
</html>

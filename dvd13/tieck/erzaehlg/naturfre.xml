<?xml version="1.0" encoding="ISO-8859-1"?>
<?xml-stylesheet type="text/xsl" href="../../bin/xsl/xml2htm.xsl"?>
<html>
<head>
  <title>Der Naturfreund</title>
  <meta name="type"      content="narrative"/>
  <meta name="author"    content="Ludwig Tieck"/>
  <meta name="title"     content="Der Naturfreund"/>
  <meta name="booktitle" content="Das gro�e deutsche Erz�hlbuch"/>
  <meta name="isbn"      content="3-7610-8047-6"/>
  <meta name="pages"     content="185-190"/>
  <meta name="sender"    content="hille@abc.de"/>
  <meta name="created"   content="20010612"/>
  <meta name="firstpub"  content="1797"/>
  <link href="../../css/prosa.css" type="text/css" rel="stylesheet"/>
</head>
<body>
  <h3 class="author">Ludwig Tieck</h3>
  <h2 class="title">Der Naturfreund</h2>
  <p>Um die Zeit im Sommer, in der ein Teil der sch�nen Welt gew�hnlich seine Zuflucht nach einem Bade nimmt, setzte sich auch ein Kriegsrat <i>Kielmann</i> in einen Wagen, um die Stadt zu verlassen. Er war nicht krank, und wollte auch kein Bad besuchen, sondern eine Zeitlang in der N�he eines Gesundbrunnens wohnen, um die sch�ne Natur zu genie�en.</p>
  <p>Der Kriegsrat Kielmann war ohngef�hr drei�ig Jahre alt uns ein sehr brauchbarer Gesch�ftsmann, er hatte eine Erholung n�tig, weil er eine lange Zeit strenge gearbeitet hatte, und er jetzt selbst f�r seine Gesundheit f�rchtete. Er wollte daher mehrere Wochen auf dem Lande zubringen, um sich und einer sch�nen Muse zu leben: denn der Kriegsrat war zugleich ein Mann von Empfindung, der in seinen j�ngern Jahren die sch�nen Wissenschaften studiert hatte. Daraus wollten ihm manche Leute in der Stadt einen Vorwurf machen; ja manche gingen gar so weit, ihn einen Narren zu schelten: diese aber waren meist mit dem Kriegsrate Weller verwandt, dessen Tochter Herr Kielmann nicht geheiratet hatte, ohngeachtet es ihm angeboten und sie das reichste M�dchen in der Stadt war. Kielmann achtete wenig auf dieses Gerede, denn er war zu sehr Philosoph, um sich um Stadtgeschw�tz zu k�mmern; er fuhr jetzt mit frohem Sinne durch das Tor, und steckte seinen Kopf l�chelnd weit aus der Chaise heraus, um sogleich das freie, sonnige Feld in Augenschein zu nehmen.</p>
  <p>Jetzt will ich dich nun auch recht genie�en o Natur, dachte der Kriegsrat bei sich selber; alle meine Arbeiten und Gesch�fte will ich nun vergessen und nur f�r dich Augen und Ged�chtnis haben. Ich will zu den Empfindungen meiner poetischen Kinderjahre zur�ckkehren, ich will mein Dasein verj�ngen und wie ein Kind an den H�nden der Sch�nheit und der Natur einhergehen.</p>
  <p>Der Wagen fuhr indessen weiter, und der Kriegsrat gab sich gro�e M�he, ja keinen Berg oder kein Dorf mit seinen Augen zu vers�umen, damit er nichts vom Genu� der l�ndlichen Natur verliere. &#8211; Wie gl�cklich bin ich, fuhr er dann mit seinem Selbstgespr�che fort, da� ich noch so frei und ledig bin, ganz meinen eigenen Einf�llen folgen kann und nicht von den Launen einer Frau abh�nge; die Mademoiselle Weller ist ein ganz h�bsches M�dchen, sie hat viel Geld, aber wenig Verstand und noch weniger Empfindung, keine Lekt�re und keine Liebe f�r die Poesie; aus der Natur macht sie sich gar nichts, sie lacht zu viel, sie scherzt �ber alles. &#8211; Es ist �berhaupt besser, da� ich mich nicht mit dem Heiraten �bereile; denn wie selten ist es, da� wir eine Seele finden, die mit uns sympathisiert und ohne die reinste Sympathie der Seelen f�hlt man in der Ehe nur die Fesseln, und den Verlust der Freiheit.</p>
  <p>Kielmann hatte w�hrend diesen Betrachtungen einen See, der links an der Stra�e lag, zu bewundern vergessen; er lie� daher den Kutscher still halten, und stieg aus, um das Vers�umte nachzuholen. Dann ging er einen Fu�steig �ber eine Wiese und lie� den Wagen langsam weiter fahren; er betrachtete nun jede Gruppe von B�umen sehr genau, und suchte sie seiner Phantasie einzupr�gen; er empfand ungemein viel, und stieg nur erst wieder in den Wagen, als ihn das Gehen erm�det hatte.</p>
  <p>Als er wieder im Wagen sa�, freute er sich auf den Anblick einiger Ruinen, die in einer halben Stunde erscheinen w�rden, und bei denen er schon in der blo�en Vorstellung einen kleinen Schauder empfand. &#8211; B�ume und H�user gingen nun rasch seinen Augen vor�ber, der Gesang der V�gel, das Rasseln der R�der, das Rauschen der B�ume und die wiegende Bewegung des Wagens versetzen ihn bald in eine gewisse Trunkenheit, er rieb die Augen zu widerholtenmalen, g�hnte dann, und nach einiger Zeit akkompagnierte er das Konzert der Natur mit einem lauten Schnarchen.</p>
  <p>Der Fuhrmann rief: Brrr!! &#8211; Die Pferde standen, der Wagen hielt; der Kriegsrat dehnte sich, g�hnte und rieb die Augen mit seinen ausgespreiteten H�nden. &#8211; Wo sind wir denn? rief er jetzt dem Fuhrmann zu.</p>
  <p>Beim Wirtshause, Herr Kriegsrat, hier wollen wir f�ttern. &#8211; die letzte halbe Meile hieher. &#8211; Aber wo sind denn die Ruinen? O Gottlob, da sind wir schon seit einer Stunde vorbei. Schon seit einer Stunde? fragte der Kriegsrat und stieg noch halb schlaftrunken aus dem Wagen.</p>
  <p>Ei! ei! sagte er zu sich selber, das ist nicht fein! Pfui! in der sch�nen offenen Natur einzuschlafen! Auf einer Reise, auf die du dich schon seit so lange gefreut hast! &#8211; Wenn das so fortginge, so w�rden wir mit dem Genu� der Natur nicht weit kommen.</p>
  <p>Man bereitete das Mittagessen, das unsern Reisenden wieder st�rkte; der Wirt unterhielt ihn dabei mit den Namen der Brunneng�ste, die schon durchgereist w�ren; Kielmann a� und trank und wiederholte sich die sch�nen Szenen in seiner Phantasie, die ihm auf dem Lande bevorst�nden; die rauschenden W�lder, der Gesang der Nachtigallen und Lerchen, die sch�ne Unschuld von Dorfbewohnern, die Simplizit�t ihrer Lebensart usw. &#8211; Es mi�fiel ihm die Geschw�tzigkeit des Wirts und er trieb den Fuhrmann und seinen Bedienten an, um so geschwind wie m�glich, wieder anzuspannen.</p>
  <p>Die Reise ging weiter. Der Kriegsrat labte sich wieder an den sch�nen Aussichten und schlief dann zur Abwechselung wieder ein; auf jeder Meile nahm er sich fest vor, munter zu bleiben, aber seine Natur �berwand jedesmal seinen Vorsatz; dann ward er auf sich selbst b�se und war am Ende doch gen�tigt, sich wieder mit sich auszus�hnen. &#8211; Sp�t in der Nacht hielt der Wagen in dem Dorfe, in welchem der Kriegsrat seinen Wohnsitz aufschlagen wollte. Er a� nur wenig und legte sich bald schlafen.</p>
  <p>Der Gesundbrunnen war nur eine halbe Meile von diesem Dorfe entfernt, und hier wohnte neben andern f�r uns uninteressanten G�sten die Geheimr�tin Langhoff mit ihrer Tochter Caroline; der Mann war schon seit einigen Jahren tot, und sie lebten von einer Pension und den unbedeutenden Renten eines kleinen Verm�gens. Die Tochter ward in jedem Sommer krank, und die Mutter wandte einen gro�en Teil ihres j�hrlichen Einkommens darauf, um mit Carolinen eine Zeitlang auf dem Brunnen zu wohnen, um sie dort mit allen G�sten bekannt zu machen; der Zweck davon war: Mademoiselle Langhoff war schon f�nfundzwanzig Jahr alt und doch noch nicht verheiratet. Man war in der Gesellschaft, man tanzte und lachte und die Mutter glaubte, da� sich die Tochter doch wohl irgendeinmal einen reichen, angesehenen Mann antanzen w�rde, den ihre sch�nen Augen oder ihr noch sch�nerer Wuchs auf ewig zu ihrem Sklaven, oder was noch schlimmer und bedeutender war, zu ihrem rechtm��igen Manne machen w�rde.</p>
  <p>Der Leser, der so g�tig ist, diese kleine und unbedeutende Erz�hlung Wort f�r Wort zu lesen, wird uns nun erlauben, mit Briefen fortzufahren, die wir neben einander stellen wollen, damit die Verschiedenheit des Stils und der Charaktere desto mehr in die Augen falle.</p>
  <p>Beim Sonnenaufgang sa� der Kriegsrat schon an einem Tisch und schrieb einen Brief an einen Freund in der Stadt, den er aber nicht sogleich abschickte, sondern in der Form eines kleinen Tagebuches fortsetzen wollte; die sch�ne Caroline schrieb fast um dieselbe Stunde an eine Freundin, und der Leser erh�lt nun hier die Parallelbriefe:</p>
  <div class="letter">
    <p class="sender">Brief des Kriegsrats Kielmann.</p>
    <p class="date">am 3ten Juli.</p>
    <p class="address">Liebster Freund,</p>
    <p>O wie gl�cklich, wie au�erordentlich gl�cklich bin ich! &#8211; Ich schreibe Ihnen aus meinem Dorfe, indem die Sonne eben aufgeht und rote feurige Strahlen �ber mein Papier wirft. &#8211; Ein sch�ner H�gel mit B�umen bekr�nzt steht vor meinen Augen, und mir ist so frisch und leicht, da� ich es Ihnen gar nicht beschreiben kann.</p>
    <p>Weiche reine gesunde Luft atme ich hier ein! &#8211; Wie froh werde ich nach einigen Wochen zur Stadt und zu meinen Gesch�ften zur�ckkehren! &#8211; Hier brauche ich nun nach keinem Rathause zu gehen. Hier �ngstigen mich nicht die vollgestopften Repositorien mit ihren best�ubten Akten. Ich will oft an diese Qualen zur�ckdenken, um die kurze Zeit, die ich hier zubringe, desto mehr zu genie�en.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief der M. Caroline.</p>
    <p class="date">am 3ten Juli.</p>
    <p class="address">Liebe Louise,</p>
    <p>Ich bin heut fr�her als gew�hnlich aufgestanden, und es scheint heute recht sch�nes Wetter zu werden. &#8211; Was das hier angenehm ist, da� man sich nicht so wie in der Stadt zu genieren braucht. &#8211; Ich habe nun endlich meine elegante Morgenhaube fertig, und ich trage sie heute im Neglig� zum erstenmale. &#8211; Das �ftere Umkleiden, die Pl�siers, das Brunnentrinken macht, da� die Zeit vergeht, man wei� selbst nicht wie. Alles ist hier so lustig und munter, besonders ein gewisser Herr Brand die Seele der ganzen Gesellschaft. Er ist lauter Leben. Bald springt er herum, bald gibt er R�tsel auf, bald neckt er einige aus der Gesellschaft; er hat ein erstaunliches Ged�chtnis. &#8211; Manche wollen es ihm nachmachen, aber es gelingt doch keinem so recht.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief des Kriegsrats Kielmann</p>
    <p class="date">am 4ten Juli nachmittags.</p>
    <p>Ich habe gestern und heut die sch�nen Gegenden umher besucht. Da ist ein kleiner Wasserfall hier ganz in der N�he, der mich heut morgen entz�ckt hat.</p>
    <p>Das Mittagessen, das ganz einfach war, hat mir heute k�stlicher geschmeckt, als je in der Stadt, und die Menschen, bei denen ich wohne, sind so simpel und so gut, da� mich ihre Gespr�che mehr unterhalten, als bei mit jenen verfeinerten Stadtmenschen, die nie wissen, was sie glauben oder sagen sollen.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief der M. Caroline.</p>
    <p class="date">nachmittags am 4ten Juli.</p>
    <p>Ich kann immer noch vor Lachen nicht zu mir selber kommen. Herr Brand hatte heute mittag einen Bauern zum besten, der Erdbeeren zum Verkauf brachte. Die ganze Tischgesellschaft wollte sich vor Lachen aussch�tten. Es ist ein allerliebster Mensch, der Brand! die Frauenzimmer hier rei�en sich auch um ihn; wie wenige M�nner gibt es doch, die ihm �hnlich sind. Wie stechen die alten, steifen Offiziere, die hier sind, gegen ihn ab!</p>
  </div>
  <div class="letter">
    <p class="sender">Brief des Kriegsrats Kielmann</p>
    <p class="date">am 5ten Juli.</p>
    <p>In dieser Nacht ist pl�tzlich Regenwetter eingefallen, und es scheint anhalten zu wollen. &#8211; Das macht mir freilich einen gro�en Strich durch meine sch�ne Rechnung; ich mu� mich aber tr�sten und meine Zuflucht zur Lekt�re nehmen. Es ist denn doch gut, da� ich einige von meinen Lieblingsdichtern mitgenommen habe. Ich habe Thomsons Jahrszeiten schon angefangen und lese dies sch�ne Gedicht immer wieder mit gro�em Interesse von neuem.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief der M. Caroline</p>
    <p class="date">am 5ten Juli.</p>
    <p>Es ist um zu verzweifeln! Es war so eine sch�ne Landpartie arrangiert, und nun f�llt es dem Himmel ein, zu regnen. &#8211; Da ist nun die liebe Frau von Lemstein und Herr Mannert gebeten, und nun werden wir uns heute an den langweiligen l'Hombretisch setzen m�ssen. Ich werde Langeweile haben und vielleicht noch mein Geld verlieren, denn ich gebe gewi� auf das Spiel nicht Achtung. &#8211; Ist es nicht um zu verzweifeln, liebe Louise.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief des Kriegsrats Kielmann</p>
    <p class="date">am 7ten Juli.</p>
    <p>Immer noch Regen und schwarz bezogener Himmel! &#8211; Das Wetter macht mich ganz unbegreiflich tr�ge und schl�frig. Ich lese fast unaufh�rlich; aber das Lesen spannt mich zu sehr an.</p>
    <p>Statt selbst in der goldenen Zeitenwelt zu leben, lese ich jetzt Gesners g�ttliche Schilderungen davon. Es will mir nur alles nicht recht behagen, weil ich mich auf die Natur selbst zu sehr gefreuet habe.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief der M. Caroline</p>
    <p class="date">am 7ten Juli.</p>
    <p>Jetzt ist mir bei dem schlechten Wetter doch besser, wenigstens etwas. Herr Brand hat uns schon einigemal recht lustige Anekdoten vorgelesen, wir kommen dabei im Saale zusammen; heute abend wollen wir ein Pf�nderspiel versuchen.</p>
    <p>Das schlechte Wetter ist doch immer hier noch eher zu ertragen, als in der Stadt, man ist doch ungeniert und dabei in Gesellschaft.</p>
    <p>Wie ich es sagte! ich habe gestern einen Taler und drei Groschen verloren.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief des Kriegsrats Kielmann</p>
    <p class="date">am l0ten Juli.</p>
    <p>Es ist doch zu arg! Denken Sie nur lieber Freund, das Regenwetter will immer noch nicht aufh�ren. Die Zeit meines Urlaubs verstreicht indes, und ich sitze hier in einem schmutzigen elenden Dorfe gefangen, ohne Besch�ftigung, ohne Gesellschaft. &#8211; Soll man dabei nicht unzufrieden werden? Wenn ich w��te, da� das Wetter so bliebe, lie�e ich gleich anspannen und f�hre wieder nach der Stadt zur�ck. &#8211; Alles macht mir hier Langeweile; da ich nicht mehr spazieren gehen kann. Die Leute hier sind zwar auf den ersten Anblick recht gut; aber zum Umgang sind sie doch ganz unbrauchbar. Das Essen hier ist auch meistensteils sehr schlecht, und was das Schlimmste ist, die Menschen wissen es nicht zuzurichten. &#8211; Ich bin ordentlich auf Neuigkeiten aus der Stadt begierig; aber man erf�hrt hier nichts, ich lebe hier, wie in der Arabischen W�ste.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief der M. Caroline</p>
    <p class="date">am l0ten Juli.</p>
    <p>Wir sind jetzt immer alle recht vergn�gt. Es ist nur �rgerlich, da� mir Mama jetzt immer Streiche spielt. Sie mag den Herrn Brand nicht gerne leiden, und darum soll ich auch nicht viel mit ihm umgehen. Die Pf�nderspiele haben uns alle recht am�siert, und der kleine Brand wu�te es so einzurichten, da� ich ihm durchaus ein paar K�sse geben mu�te. Es ist recht schade, da� der h�bsche Mensch nicht mehr Verm�gen hat; denn so sagt man von ihm, da� er viel schuldig sein soll. Ein paar allerliebste Spr�chw�rter hat er auch erfunden und aufgef�hrt; in dem einen mu�te ich seine Frau vorstellen; das gab denn zu allerhand Neckereien Gelegenheit, die Mama viel zu ernsthaft genommen hat. Ich wette, wenn der junge Mensch reicher w�re, Mama w�rde ihn selber gern sehn. &#8211; Aber so, &#8211; ach, ich wei� nicht, was ich alles schwatze! &#8211;</p>
  </div>
  <div class="letter">
    <p class="sender">Brief des Kriegsrats Kielmann</p>
    <p class="date">am 11ten Juli.</p>
    <p>Ich bin mit dem Prediger des Dorfes, einem alten wunderlichen Manne, bekannt geworden. Er hat eine au�erordentliche Leidenschaft f�rs Kartenspiel, versteht aber kein anderes als das gemeine alte fr�nkische Mariage. Er lenkte bald darauf ein, und ihm zu Gefallen habe ich heute fast den ganzen Tag an dem Spieltisch versessen. &#8211; Was sagen Sie dazu, mein Freund? Aber was soll man auch bei dem abscheulichen Wetter anfangen?</p>
  </div>
  <div class="letter">
    <p class="sender">Brief der M. Caroline</p>
    <p class="date">am 11ten Juli.</p>
    <p>Ich kann doch den Herrn Brand nicht vermeiden, ohne die ganze Brunnengesellschaft aufmerksam zu machen, nicht wahr, liebe Louise? Und doch will es Mama durchaus so haben. Und ich wei� es, da� es den armen Menschen betr�bt, wenn ich mich jetzt mehr zur�ckziehe. Er geht mir immer nach und sucht recht geflissentlich meine Gesellschaft, &#8211; ja Mama mag es ihm selber verbieten! was geht es mich an?</p>
  </div>
  <div class="letter">
    <p class="sender">Brief des Kriegsrats Kielmann</p>
    <p class="date">am 14ten Juli.</p>
    <p>Ich finde doch, da� man bei jedem Spiele mehr Feinheit anbringen kann, als man im Anfange glaubt. Der Prediger hatte bisher immer von mir gewonnen; aber jetzt ist oft der Sieg zweifelhaft. Das Spiel interessiert mich ordentlich lebhaft; der sonderbare Mann hat mich mit seiner Leidenschaft angesteckt.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief der M. Caroline</p>
    <p class="date">am 14ten Juli.</p>
    <p>Denken Sie nur, man sagt sich ins Ohr: Brand w�rde die dicke Frau von Lemstein heiraten. Er spricht zwar viel mit ihr, aber das kann ich denn doch unm�glich von ihm glauben. Sind Sie nicht auch meiner Meinung, liebe Louise? Sie kennen ja auch das h��liche Weib.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief des Kriegsrats Kielmann</p>
    <p class="date">am 20ten Juli.</p>
    <p>Ich bin recht b�se auf mich, und ich denke, ich habe Ursache dazu. Schon seit vier Tagen ist das sch�nste Wetter von der Welt, und ich habe sie am Spieltische zugebracht, mit dem abgeschmackten Prediger und seinem kl�glichen Spiele habe ich sie verschwendet. Erst heute bin ich wieder ausgegangen. Wie kann der Mensch so schwach sein? &#8211; Ich begreife mich selbst nicht.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief der M. Caroline</p>
    <p class="date">am 20ten Juli.</p>
    <p>Es ist gewi� mit der Frau von Lemstein. O der Windbeutel! &#8211; Aber die ganze Gesellschaft hier verachtet ihn auch, und das mit Recht; der Harlekin k�mmt einem gar nicht wie ein ordentlicher Mann vor. Blo� des Verm�gens wegen ein altes, h��liches Weib zu heiraten?</p>
    <p>Wie kann ein Mensch so elend sein? &#8211; Ich kann es nicht begreifen.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief des Kriegsrats Kielmann</p>
    <p class="date">am 21ten Juli.</p>
    <p>Ich habe den benachbarten Brunnen heute besucht, und ich finde, da� uns selbst auf dem Lande Gesellschaft unentbehrlich ist. Es sind viele Bekannte hier, als die Geheimr�tin Langhof mit ihrer Tochter, die Frau von Lemstein und andre. Pharao wird hier hoch gespielt. Ich werde �fter herkommen.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief der M. Caroline</p>
    <p class="date">am 21ten Juli.</p>
    <p>Ich w�nsche, wir m�chten wieder bald nach der Stadt zur�ckreisen. Alles wird hier so langweilig; man am�siert sich jetzt mit Hasardspielen. &#8211; Da war heute der unausstehliche Kriegsrat <i>Kielmann</i> hier, der empfindsame Narr. Sie m�ssen ihn auch kennen, der einmal eine Liebschaft mit der Mamsell Weller hatte.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief des Kriegsrats Kielmann</p>
    <p class="date">am 22ten Juli.</p>
    <p>Bin ich nicht ein rechter Narr, da� ich meine Zeit verderbe und mein Geld verspiele? &#8211; Ich habe heute im Pharao sehr ansehnlich verloren; ich will es auch k�nftig unterlassen.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief der M. Caroline</p>
    <p class="date">am 22ten Juli.</p>
    <p>Mama hat auch Lust, nach der Stadt zur�ckzukehren, und ich w�nsche, es w�rde nur erst angespannt, dann k�nnt' ich mit Ihnen, liebe Louise, �ber dies und jenes weitl�ufig sprechen.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief des Kriegsrats Kielmann</p>
    <p class="date">am 23ten Juli.</p>
    <p>Die Gegend um den Brunnen und die Gesellschaft dort gef�llt mir au�erordentlich. Ich habe heute nicht gespielt und mich doch sehr unterhalten. Sie werden die Tochter der R�tin Langhof kennen, es ist ein sehr liebensw�rdiges M�dchen; ich habe mit ihr und der Mutter viel gesprochen, wir gingen ziemlich lange miteinander spazieren. Man hat mich eingeladen.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief der M. Caroline</p>
    <p class="date">am 23ten Juli.</p>
    <p>Das fehlt uns noch, da� uns die langweiligen Narren auf den Hals kommen! Da hat sich der pinselnde Kielmann den ganzen Tag mit uns herum getrieben und mir vollends alle Laune verdorben. Mama ist von dem vern�nftigen Manne ganz charmiert und hat ihn auf morgen gebeten. &#8211; Alles ist mir entgegen! &#8211; Ich m�chte manchmal toll werden!</p>
  </div>
  <div class="letter">
    <p class="sender">Brief des Kriegsrats Kielmann</p>
    <p class="date">am 24ten Juli.</p>
    <p>Die Mademoiselle Langhof ist nicht nur ein sch�nes, sondern auch ein �beraus verst�ndiges M�dchen, sie spricht auch mit vielem Gef�hl. Ein affektiertes Windspiel strich heut viel bei ihr herum; sie begegnete ihm aber, zu meiner gro�en Freude, mit der geh�rigen Verachtung. Etwas, das man selbst bei den kl�gsten Frauenzimmern nur sehr selten findet, denn fast alle lieben bei den Mannspersonen die Affenmanieren.</p>
    <p>Die R�tin selbst ist eine hochachtungsw�rdige Frau; sie scheint von mir eine sehr gute Meinung zu haben. Sie �u�erte heut, da� sie w�nschte, ich m�chte sie �fter besuchen, damit sie sich etwas mehr von der uninteressanten Brunnengesellschaft entfernen k�nne. &#8211; Wenn ich der Tochter nur nicht zur Last falle! Mir schien es heut, als wenn sie mich nicht besonders gerne s�he. &#8211; Es tut mir fast leid, da� ich nicht selbst auf dem Brunnen wohne: der Weg nach dem Dorfe ist doch etwas beschwerlich.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief der M. Caroline</p>
    <p class="date">am 24ten Juli.</p>
    <p>Einen so verdrie�lichen Tag habe ich seit langem nicht erlebt. Der Kriegsrat ist fast bis um Mitternacht geblieben, und auch der elende Brand war impertinent genug, uns auf eine Stunde zu besuchen. Ich denke aber, ich bin ihm so begegnet, da� er nicht wieder kommen soll. Recht das Gegenteil von ihm ist der Kriegsrat, mit dem Mama au�erordentlich h�flich und freundschaftlich ist, weil er Verm�gen hat; er findet sich dadurch sehr geschmeichelt.</p>
    <p>Es war gestern ein Gewitter, und ich glaubte gewi�, da� uns der Kriegsrat verschonen w�rde; aber er kam dennoch. &#8211; Mama meint, er w�re in mich verliebt; je nu, als Mann w�re er wohl noch zu ertragen. Wir wollen sehen, wie es sich f�gt; ich will wenigstens von nun an freundlicher gegen ihn sein; sollte es auch nur deswegen geschehen, um den j�mmerlichen Brand recht empfindlich zu kr�nken. &#8211; Wenn der Kriegsrat nur nicht so ganz au�erordentlich langweilig w�re.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief des Kriegsrats Kielmann</p>
    <p class="date">am 27ten Juli.</p>
    <p>Ich bin jetzt den ganzen Tag auf dem Brunnen. Morgen wird hier ein Zimmer leer, und ich will nun noch auf einige Tage hier wohnen.</p>
    <p>Die R�tin hat mir erz�hlt, da� ihre Tochter mich sehr gerne s�he, da� sie oft nach mir frage, und da� sie nur zu bl�de und bescheiden sei, um etwas von ihrer Zuneigung in meiner Gegenwart zu �u�ern. Ich habe es nie recht glauben k�nnen, aber jetzt bin ich davon �berzeugt. Sie ist seit zwei Tagen sehr freundlich gegen mich, und als ich ihr heut aus dem Klopstock etwas vorlas, bemerkte ich pl�tzlich, da� Tr�nen aus ihren Augen brachen. &#8211; Wenn ich aufrichtig sein soll, lieber Freund, so mu� ich Ihnen sagen, da� das mein Herz gebrochen hat; ich f�hle es jetzt, da� ich sie liebe, die Natur umher hat neue Reize f�r mich, ich bin gl�cklich. &#8211; Wenn sie mich nur wieder liebte, so wie ich sie liebe!</p>
  </div>
  <div class="letter">
    <p class="sender">Brief der M. Caroline</p>
    <p class="date">am 27ten Juli.</p>
    <p>Der Kriegsrat wohnt jetzt auf dem Brunnen, so sehr hat er sich an uns attachiert.</p>
    <p>Ich m�chte jetzt mehr darauf wetten, da� er wirklich in mich verliebt ist. Unaufh�rlich betrachtet er mich mit sehr z�rtlichen Augen; er seufzt und ist oft in Gedanken. Ich begegne ihm freundlicher, und er ist dadurch sehr gl�cklich. Er las uns heute aus dem Klopstock etwas vor; liest sehr schlecht, und dann machte mir auch der unaufh�rliche Kram von Engeln und b�sen Geistern, die unverst�ndlichen Verse, und da� das Gedicht durchaus nicht spa�haft war, so viel Langeweile, da� mir die Kinnbacken vom verbi�nen G�hnen weh taten; meine Augen gingen endlich davon �ber, und er hielt es f�r R�hrung.</p>
    <p>Seit diesem Augenblicke wurde er noch weit z�rtlicher gegen mich; meine Mutter ist sehr zufrieden, und ich bin es beinahe auch.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief des Kriegsrats Kielmann</p>
    <p class="date">am 28ten Juli.</p>
    <p>Ich habe mich erkl�rt, ich habe die Einwilligung. &#8211; Beschuldigen Sie mich keiner �bereilung, teurer Freund; wie selten findet man jetzt ein f�hlendes Herz? man achte es k�stlich, wenn man es gefunden hat.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief der M. Caroline</p>
    <p class="date">am 28ten Juli.</p>
    <p>Er hat sich erkl�rt, er hat die Einwilligung. &#8211; Nennen Sie mich nicht rasch, liebe Louise, denn meine Mutter hat recht. Die reichen M�nner sind jetzt selten, und man schlage schnell zu, wenn sich einer anbietet.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief des Kriegsrats Kielmann</p>
    <p class="date">am 2ten August</p>
    <p>Morgen reise ich von hier ab, und zwar in Gesellschaft meiner Braut und meiner Schwiegermutter; ich glaube, es wird nun gerade ein Monat sein, da� ich die Stadt verlassen habe. &#8211; Wie freue ich mich darauf, Sie wieder zu sehn und Ihnen meine k�nftige Gattin vorzustellen.</p>
  </div>
  <div class="letter">
    <p class="sender">Brief der M. Caroline</p>
    <p class="date">am 2ten August.</p>
    <p>Ich komme zur�ck, und zwar mit einem Br�utigam. &#8211; Endlich werde ich Sie nun wiedersehn, liebe Louise, und Sie m�ssen gleich in den ersten Tagen den Kriegsrat, meinen zuk�nftigen Mann, kennen lernen. &#8211; Leben Sie bis dahin recht wohl.</p>
  </div>
  <p>Und weiter? &#8211; Alle kamen gl�cklich zur Stadt zur�ck, es ward eine gew�hnliche Heirat geschlossen. Der Kriegsrat ward ein Ehemann; die ganze Stadt lachte, selbst die Braut lachte ein Duett mit ihrer Mutter. Und der Kriegsrat Kielmann? &#8211; je nun, der sah ein, da� er sich geirrt habe. &#8211; Aber ist nicht all unser Wissen in dieser Welt nur ein Irrtum? &#8211; Er tr�stete sich mit diesem Gedanken.</p>
</body>
</html>

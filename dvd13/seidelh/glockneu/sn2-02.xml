<?xml version="1.0" encoding="ISO-8859-1"?>
<?xml-stylesheet type="text/xsl" href="../../bin/xsl/xml2htm.xsl"?>
<html>
<head>
  <title>Das Eisfest</title>
  <meta name="booktitle" content=" Neues Glockenspiel &#8211; Gesammelte Gedichte &#8211; II. Sammlung, Band XI der Gesammelten Schriften"/>
  <meta name="author"    content="Heinrich Seidel"/>
  <meta name="title"     content="Das Eisfest"/>
  <meta name="publisher" content="J.G. Cotta'sche Buchhandlung Nachfolger G.m.b.H."/>
  <meta name="address"   content="Stuttgart"/>
  <meta name="type"      content="poem"/>
  <meta name="sender"    content="magnus.mueller@phys.chemie.uni-giessen.de"/>
  <meta name="firstpub"  content="1893"/>
  <link href="../../css/prosa.css" rel="stylesheet" type="text/css"/>
</head>
<body>
  <div class="poem">
    <h3 class="author">Heinrich Seidel</h3>
    <h2 class="title">Das Eisfest</h2>
    <p class="vers">Ein Wintersonntagnachmittag am Meer.<br/>
      Des Nordens schneidend strenger Hauch, der weit<br/>
      Die See zur Spiegelfl�che umgewandelt,<br/>
      Zeigt milder sich in seinem Regiment,<br/>
      Bezwungen scheinbar von der Sonne Licht,<br/>
      Die von des nebelblauen Himmels Rund<br/>
      Die schr�gen winterlichen Strahlen sendet.<br/>
      So einsam liegt die kleine Nordseestadt;<br/>
      Die Strassen leer, der Marktplatz �d' und still &#8211;<br/>
      Denn Alle, Jung und Alt, so Mann als Weib<br/>
      Zur Eislust zogen an die See hinaus.<br/>
      Wettlaufen gab es heut, ein Schlittschuhfest,<br/>
      Ein st�hlern frisches nordisches Vergn�gen; &#8211;<br/>
      Nur wer zu alt und krank, blieb heut zu Haus.</p>
    <p class="vers">Dort auf dem D�nenh�gel, nah am Strand<br/>
      Im Fischerhaus, das hoch dort und allein<br/>
      Hinschaut auf's Meer, blieb einsam auch zur�ck<br/>
      Ein altes M�tterlein gel�hmt und schwach.<br/>
      Doch steht ihr Bett an's Fenster hinger�ckt,'<br/>
      Dass sich ihr Auge an dem Jubel weide.<br/>
      Sie blickt hinaus, wo ferne, weit vom Strand<br/>
      Es freudig w�hlt auf spiegelglattem Platt,<br/>
      Wie's dort sich dr�ngt, wo gastlich heller Rauch<br/>
      Den immer neu gef�llten Punschnapf k�ndet.<br/>
      Und dort, wie dicht, um's tragbare Theater,<br/>
      Wo Kasperle die alten, ewig neuen<br/>
      Nie ausbelachten Sp�sse treibt, und dort,<br/>
      Wo w�rd'ge B�rger, wohlgewiegte Kenner,<br/>
      Der Jugend Schlittschuhk�nste kritisiren.<br/>
      Und rings umher um dichteres Gedr�nge,<br/>
      Unruhig wirrt und schwirrt es ab und zu<br/>
      Und dehnt sich aus und breitet sich in's Weite,<br/>
      Bis dann am Horizont in blauem D�mmer<br/>
      Wie dunkle Punkte noch die fernsten irren.</p>
    <p class="vers">Sie schaut und sinnt und denkt an alte Zeit,<br/>
      An l�ngst versunkne Winterlust zur�ck. &#8211;<br/>
      Es war einmal ein Wintertag wie heut,<br/>
      Und sie war jung, und all die bunte Lust<br/>
      War noch ihr eigen &#8211; das war sch�ne Zeit.<br/>
      Der junge Schiffer, der von langer Fahrt<br/>
      Zur�ckgekehrt &#8211; drei Jahre war er fort<br/>
      In Indien und China und so braun<br/>
      Von fremder Sonne gl�hendheissem Brand &#8211;<br/>
      Im Schlitten fuhr er sie &#8211; er bat so sehr.<br/>
      Der sprach so Mancherlei &#8211; das klang so fremd<br/>
      Und doch gefiel es ihr. Sie wusste nichts,<br/>
      Was ihr bis jetzt so sehr gefallen h�tte.<br/>
      Dann kam er �fter in des Winters Lauf,<br/>
      Am Abend, wenn man von der Arbeit ruht.<br/>
      Sie sassen dann um's Feuer mit den Alten<br/>
      Und sprachen hin und her so allerlei, &#8211;<br/>
      Von fremden L�ndern, wie es seltsam dort<br/>
      Und anders ist. Der wusste zu erz�hlen:<br/>
      Von der Chinesen schlitzge�ugtem Volk,<br/>
      Vom braunen Indier, der am Ganges wohnt,<br/>
      Von Sturm und Schiffbruch, &#8211; wie des Seemanns Loos<br/>
      Ihn hin und herwirft wie der Wind die Welle.<br/>
      Und jedesmal bracht' er ein seltsam St�ck,<br/>
      Ein bunt chinesisch Tuch, ein G�tzenbild,<br/>
      Ein farbig Schneckenhaus &#8211; und schenkt es ihr.<br/>
      Und eines Tags, da es zum Fr�hling ging<br/>
      Und seine bunten Sch�tze all' geschwunden,<br/>
      Bracht' er ein einfach golden Ringlein mit,<br/>
      Und sie ward seine Braut &#8211; und als er dann<br/>
      Im Herbste endlich wieder heimgekehrt,<br/>
      Da zogen sie selband in's D�nenhaus.<br/>
      Ja, ja &#8211; so geht in Lust und Leid die Zeit.<br/>
      Jetzt ruht er dort im St�dtchen bei der Kirche,<br/>
      Die Kinder sind schon gross und fortgezogen,<br/>
      Und nur ein fr�hverwaistes Enkelkind<br/>
      Blieb ihr zur Pflege in dem Haus zur�ck.</p>
    <p class="vers">Sie sinnt und tr�umt und schauet in die Ferne,<br/>
      Zum Horizont, wo sich im Winterduft<br/>
      Die ungeheure Fl�che still verliert. &#8211;<br/>
      Da pl�tzlich zuckt sie &#8211; blicket starr hinaus!<br/>
      Sie zittert &#8211; krampft sich an des Bettes Rand:<br/>
      Das W�lkchen dort, das sich am Horizont<br/>
      In weisslich grauem Schein wie Nebelduft<br/>
      Unheimlich still und schnell zusammendichtet<br/>
      Und w�chst und aufr�ckt an des Himmels Rund,<br/>
      Die Wetterwolke ist's, sie deutet Sturm!<br/>
      Es t�uscht sich nicht des Seemanns kund'ges Weib,<br/>
      Sie weiss des Himmels Wolkenschrift zu lesen<br/>
      Und diese deutet Sturm! Nur kurze Zeit,<br/>
      Und des Orkanes riesenhafte Macht<br/>
      W�hlt auf das Meer, wo es das Eis nicht b�ndigt<br/>
      Und bricht und w�thet fort und fort,<br/>
      Und eh' sie nur, die arglos hier vertraun,<br/>
      Das Unheil ahnen, st�rmt es schon herein<br/>
      Und Alle, Alle m�ssen untergehn.<br/>
      Sie ruft, sie schreit, sie will das Fenster �ffnen!<br/>
      Umsonst! der Frost h�lt es versperrt! Sie schl�gt<br/>
      Die Scheiben ein und schreit hinaus! Umsonst!<br/>
      Ihr d�nner Ruf versinkt im Meer der Luft<br/>
      Wie eine Flocke! Gott, was soll ich thun?!<br/>
      Die Wolke! Keine Stunde mehr! O Gott,<br/>
      Hilf du doch, dass nicht Alle untergehn! &#8211;<br/>
      Sie schaut auf Rettung hastend wild umher,<br/>
      Sie findet nichts! &#8211; Da f�llt ihr Blick<br/>
      Zum flammenden Kamin. Ein Flammenschein<br/>
      Des Jubels zuckt durch ihr Gesicht. �Herr Gott,<br/>
      Ich danke dir! Du gabst mir den Gedanken!�<br/>
      Sie richtet auf den vielgequ�lten Leib &#8211;<br/>
      Und, ob's wie scharfe Schwerter ihr Gebein<br/>
      Durchzuckt, sie schleppt sich aus dem Bett hervor &#8211;<br/>
      Das Bettstroh reisst sie aus und streut's umher.<br/>
      Dort in den alten Schrank, ein angeerbtes St�ck,<br/>
      Ein sch�n geschnitztes, nussbaumbraun Ger�th,<br/>
      Daran Erinnrung um Erinnrung ist gekn�pft,<br/>
      Daran ihr Herz seit ihrer Jugend h�ngt,<br/>
      Stopft hastig sie das Stroh, und zum Kamin<br/>
      Mit tausend Qualen kriecht sie m�hsam hin<br/>
      Und reisst den besten Feuerbrand hervor<br/>
      Und z�ndet hier und z�ndet dort und bl�st:<br/>
      Hei, wie es flackert! Und nicht eher kriecht<br/>
      Sie hin zur Th�r, bis Alles flammt und gl�ht..<br/>
      Sie bricht zusammen nun &#8211; sie rafft sich auf &#8211;<br/>
      Der Rauch erstickt sie fast &#8211; doch jetzt die Th�r! &#8211;<br/>
      Und nun mit Brausen saust der Zug der Luft<br/>
      Durch Th�r und offnes Fenster in die Flamme.</p>
    <p class="vers">Und m�hsam schleppt die Kranke sich vor's Haus, &#8211;<br/>
      Zusammensinkend bei dem alten Stein,<br/>
      Wo sie so oft in sch�ner Sommerzeit<br/>
      In stiller Abendd�mmrung sonst gesessen.<br/>
      Hei, wie's von Ferne fr�hlich summt und braust:<br/>
      Sie ruft und winkt. &#8211; Man h�rt und sieht es nicht,<br/>
      Sie schaut zur�ck und bebt. Nur d�nner Rauch<br/>
      Geht sparsam durch die Fensterscheiben aus<br/>
      Und scheint allm�lig g�nzlich zu verschwinden!<br/>
      Da, endlich bricht es schwer und dicht hervor!<br/>
      Aus Th�r und Fenster w�lzt sich schwarzer Rauch,<br/>
      Die Flamme leckt mit rother Zunge nach,<br/>
      Und nun, wie eine S�ule in die Luft<br/>
      Gewaltig steigt das Warnungszeichen auf<br/>
      �Herr Gott, ich danke dir &#8211; es brennt, es brennt!�<br/>
      So ruft sie aus &#8211; ohnm�chtig sinkt sie hin.</p>
    <p class="vers">Wie wenn ein Bienenvolk zur Sommerszeit<br/>
      Zum Schw�rmen dicht gedr�ngt sich summend f�gt,<br/>
      So l�uft das Volk, das ,noch in bunter Lust<br/>
      Verloren eben harmlos sich vergn�gte,<br/>
      Zusammen im Gewirr und starrt und staunt.<br/>
      Und dann, wie aufgeschreckter Staare Flug<br/>
      In dunklem Schwarm mit brausendem Get�n<br/>
      Strebt Alles unter Rufen, Fragen, Schreien,<br/>
      In wild bewegter Hast dem Lande zu,<br/>
      Und hastig naht der letzte dunkle Punkt:<br/>
      Sie kommen Alle &#8211; keiner bleibt zur�ck.</p>
    <p class="vers">Hinan zum H�gel st�rmen schon die Einen:<br/>
      Sie stehn und sehn dort oben. Keine Rettung!<br/>
      Schon sank das Dach und Flammen �berall.<br/>
      �Hier liegt die Frau!� ruft Einer, �sie ist todt!�<br/>
      �O nein, sie athmet noch, O seht sie regt sich!�<br/>
      Die schl�gt die Augen auf und sieht umher<br/>
      Und sieht, wie's auf dem H�gel dr�ngt und w�chst,<br/>
      Wie Alle, Alle kommen, und ein Schein<br/>
      Gl�cksel'gen L�chelns kl�rt ihr Angesicht.<br/>
      Ein wildes Fragen bricht auf sie herein:<br/>
      �Wie ist's' geschehn? Was ist des Ungl�cks Grund?<br/>
      O schweigt! Sie redet!� Stille wird es nun<br/>
      Bis auf der Flamme Brausen, und die Glocke,<br/>
      Die fern vom Thurm den Feuerangstruf sendet.<br/>
      Den schwachen Arm erhebt das M�tterlein<br/>
      Und deutet zitternd nur hinaus auf's Meer.<br/>
      Und wie, als wenn des Ungewitters Wuth<br/>
      Auf dieses Zeichen nur geharrt &#8211; ert�nt<br/>
      Aus jener Wolkenwand, die schwer und dr�uend<br/>
      Mit grausenhafter Schnelle steigt empor,<br/>
      Ein klirrend Brausen fern und f�rchterlich.<br/>
      Dann st�rmt's herbei in fesselloser Wuth!<br/>
      Voran, hinirrend durch des Eises Plan,<br/>
      Herolde, die des Herrschers Ankunft k�nden,<br/>
      Springt mit des Donners Hallen Spalt auf Spalt!<br/>
      Der Eisstaub fegt hinsausend durch die Luft,<br/>
      Und an den H�gel st�rzt des Windes Wuth,<br/>
      Dass Br�nde, wie Raketen in die Luft<br/>
      Hinsausen von der Brandstatt. Hei, und nun<br/>
      Das Wasser, wie es durch des Eises Spalten<br/>
      Vorleckt und dunkel z�ngelt! Dann empor<br/>
      Steigt hier die Scholle, th�rmt mit Schollen sich<br/>
      Und st�rzt mit Krachen. In die L�cke fasst<br/>
      Des Sturmes Faust und bricht und w�thet weiter.<br/>
      Die Wogen, von der Fessel nun befreit,<br/>
      Sie jauchzen auf und st�rzen auf die Dr�nger<br/>
      Und nun in wildem Kampfe auf und ab &#8211;<br/>
      Ein st�rmisch krachend Wogen rings umher.<br/>
      Die auf dem H�gel liegen auf den Knieen,<br/>
      Bis milder wird der Sturm, der, wie er kam,<br/>
      Verbraust und weitersaust. Und nun hervor<br/>
      Mit weissem Haar, das hell im Winde weht,<br/>
      Tritt an des Ufers ragend steilen Rand<br/>
      Der alte Pfarrherr zu dem M�tterlein.<br/>
      Er h�lt die Hand ihr segnend auf das Haupt,<br/>
      Und deutet hin auf's Meer und spricht bewegt:<br/>
      �Gelobt sei Gott! Er weiss es wohl zu machen!<br/>
      Der Dank sei ihm, der m�chtig ist im Schwachen !�</p>
  </div>
</body>
</html>

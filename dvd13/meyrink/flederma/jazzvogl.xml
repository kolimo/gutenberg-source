<?xml version="1.0" encoding="ISO-8859-1"?>
<?xml-stylesheet type="text/xsl" href="../../bin/xsl/xml2htm.xsl"?>
<html>
<head>
  <title>Der Jazz-Vogel</title>
  <meta name="booktitle" content="Tiergeschichten"/>
  <meta name="type"      content="narrative"/>
  <meta name="title"     content="Der Jazz-Vogel"/>
  <meta name="author"    content="Gustav Meyrink"/>
  <meta name="year"      content="1984"/>
  <meta name="publisher" content="Moewig"/>
  <meta name="isbn"      content="3-8118-1809-0"/>
  <meta name="pages"     content="96-100"/>
  <meta name="sender"    content="hille@abc.de"/>
  <meta name="created"   content="20080715"/>
  <link href="../../css/prosa.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<h3 class="author">Gustav Meyrink</h3>
<h2 class="title">Der Jazz-Vogel</h2>
<p>
Leichtfertig, wie der Mensch nun einmal ist, liest er zwar seit
Generationen in der Bibel, da� Gott die Welt erschuf,
indem er ein Wort aussprach, aber leider denkt sich niemand
etwas dabei. Eingefleischte Lateiner behaupten, dieses
Wort h�tte �Fiat� gelautet. Ich will das nicht glauben;
was hat der liebe Gott mit einem italienischen Automobil zu
tun?! Die wegen ihrer Fr�mmigkeit ber�chtigten tibetochinesischen
Lamapriester glauben es auch nicht und beharren
entgegen den christlichen Missionaren fest auf der
Ansicht, die sch�pferische Ursache aller Formen seien
Kl�nge, unh�rbare allerdings, da unser Ohr nicht auf sie
eingestellt sei. Musik also. Streuen wir auf eine vermittels des
Violinbogens in t�nende Schwingung versetzte Glasplatte
feinen Sand, so formt er sich zu wundersch�nen geometrischen
Figuren. Da� Schneeflocken, durch eine Lupe gesehen,
ebenfalls solche Bilder aufzeigen, wissen wir alle, aber
wer hat sich bis heute Gedanken dar�ber gemacht: k�nnte
man nicht Schl�sse ziehen aus ihrer Gestaltung auf die
geheimnisvolle Sph�renmusik, der sie ihr Aussehen verdanken?
Wem es gel�nge, diese Kl�nge mit der Kehle oder
durch Instrumente wieder hervorzubringen, der bes��e den
Schl�ssel zu gr��ter Macht. Musik als Zauberstaat! &#8211;
</p><p>
Es gab vor Jahren einen seltsamen Menschen &#8211; einen
Neger &#8211; der eine Ahnung gehabt zu haben schien, welch
unheimliche magische Gewalt dem Rhythmus der Musik
innewohnen kann. Er war dem Urklang auf der Spur. Dem
zerst�renden freilich und nicht dem sch�pferischen. Wer
den Mann den Erfinder des Jazz nennt, hat wahrlich recht.
Auf seiner Visitenkarte stand die phantastische Inschrift:
</p>
<div class="box">
Prof. Dr. Mval Djumboh Cassekanari. Chevalier de
l'Ordre du Voudou Saint des Egbas. Professor der
schwarzen Magie an der T'changa Wanga Universit�t.
Geheimrat und Quimboisseur Seiner Excellenz des
Expr�sidenten von Haiti, Mitglied der haupts�chlichsten
westafrikanischen und westindischen wissenschaftlichen
Gesellschaften etc. etc. etc.
</div>
<p>
�Professor� Cassekanari galt unter den haitischen Negerst�mmen
der Koromantyn, Eboe und Boppo als eingeweihter
Obeanpriester eines uralten bis auf Ham, den Sohn
Noahs, zur�ckreichenden schauderhaften Kultes, dessen
Gottheit auf Erden repr�sentiert wird durch die Tarantelspinne,
die einen gro�en wei�en Sack mit sich herumschleppt
 &#8211; den Sack, in den die wei�e Rasse eingefangen
werden soll, wenn die Zeit ihrem Ende zugeht. Professor
Cassekanari hielt sich einen Zwergraben, Jazz genannt, aus
seiner Heimat stammend: dem Kongogebiet; man sagt,
manche Exemplare w�rden 150 und noch mehr Jahre alt.
Diesem Vogel sang, grunzte und schnatterte der unheimliche
Neger bis kurz vor seinem Tode Tag f�r Tag eine
phantastische Melodie vor, die er teils mit H�ndeklappen
begleitete, teils mit Pfiffen oder Kl�ngen durchfurchte, wie
er sie einem alten verwitterten Bockshorn, rostigen Pfannen
und anderen dem Tode verfallenen Dingen zu entlocken
verstand. &#8211; �Nur ein gefl�geltes Tier wird meine Musik
and�chtig im Herzen bewahren�, pflegte er zu sagen, wenn
man ihn nach Z�cken eines Trinkgeldes fragte, wozu das
alles gut sei: �Die Menschen sind zu treulos und verge�lich,
denn ihre Gedanken wandern best�ndig.� In solchen F�llen
tat er gew�hnlich ein �briges und gab dem Vogel ein
Zeichen, worauf dieser zuerst vor den ungl�cklichen Zuh�rern
eine tiefe, nicht mi�zuverstehend h�hnische Verbeugung
machte, sich sodann in seinem schwarzen Frack stolz
aufrichtete wie ein Konzerts�nger und durch gewissenhaft
genaue Wiedergabe der eingelernten Hymne das Auditorium
in wilde Flucht jagte.&#8211; &#8211; &#8211;
</p><p>
Als eines Tages Professor Cassekanari unter gr��lichem
Augenverdrehen das Zeitliche gesegnet hatte, bestanden
die katholischen Missionare darauf, da� ihm ein christliches
Begr�bnis zuteil werde, aber ihre Andacht wurde leider
durch das ruchlose Dazwischentreten des h�mischen Vogels
betr�chtlich gest�rt. Das Biest hatte es sich n�mlich nicht
versagen k�nnen, auf eine in der N�he der Kapelle stehende
Palme zu fliegen und von hoch oben herab die Grabpredigt
mit der Wiedergabe aller Kompositionen seines toten Herrn
zu begleiten. Den Negern schien das einen Mordspa� zu
machen; sie zwinkerten einander zu, und schlie�lich gerieten
sie in eine wilde Tanzekstase, wobei sie ihre Plattf��e
auf grauenhafte Weise verdrehten. Erst als ein Chorknabe
mit einem rostigen Schie�gewehr angeeilt kam und die
Donnerb�chse auf den Raben anlegte, trat Ruhe ein. Der
Vogel jedoch lie� sich nicht beirren; er sang gelassen bis zu
Ende, und eine Sekunde, bevor der Schu� losging, schrie er
selber: Krach, Bumm, und flog davon. Da sage noch
jemand, Tieren wohne kein Pflichtgef�hl inne!
</p><p>
Alles das hatte sich vor etwa vierzig Jahren begeben, und
niemand w��te mehr darum, w�re mir nicht vor kurzem ein
alter Schulfreund, der Zeichner Meixner, begegnet, der sich
fr�her als Orchideensammler lange, lange Zeit auf Jamaika
herumgetrieben hatte. Was er mir erz�hlte, klang so wundersam,
da� ich mir alle M�he geben mu�te, es zu glauben,
aber mein Freund ist ein so wahrheitsliebender Mensch, da�
ich nicht zweifeln darf. Auf seinen Wanderungen im Urwald
und im Gebirge sei er des �ftern tier�hnlichen Wesen
begegnet, die nicht die geringste �hnlichkeit mit den Gesch�pfen
gehabt h�tten, wie man sie zum Beispiel bei
Hagenbeck und �hnlichen zoologischen Unternehmungen
zu sehen bek�me. Ein Steinbock mit Elsterfedern auf dem
Ges��, einer Entenpatte als Hinterbein und elefantiasisartig
degeneriertem Vorderfu� war noch das bescheidenste, was
er mir vorsetzte. Grimmig die Z�hne zusammenbei�en
mu�te ich doch, um ihn in seiner Schilderung einer alten
Dame mit Schinken�rmeln nicht zu unterbrechen, die ihm
angeblich eines Abends auf einer menschenleeren Landstra�e
als Marabu verkleidet begegnet sei, ich konnte mir kaum
mehr helfen, so st�rmisch fielen die Zweifel �ber mich her.
Ich h�tte auch sicherlich den Verkehr mit Meixner abgebrochen
in dem unabweisbaren Gef�hl, das Opfer eines Phantasten
geworden zu sein, g�ben mir nicht immer wieder die
Tatsachen zu denken, da� Kl�nge und T�ne die Ursachen
aller Formen auf Erden und im Weltraum sind und sein
m�ssen. Als ich vor l�ngerer Zeit einmal eine Jazzband
spielen h�rte, wunderte ich mich, da� solche Musik nicht auf
der Stelle heftige Ver�nderungen im menschlichen K�rper
verursache. Nun: Wahrscheinlich stehen wir noch in den
Anf�ngen; offenbar ist die Melodie des gottseligen Professors
Cassekanari noch nicht gewissenhaft aus dem Allheilland
Amerika bis zu uns gedrungen. Vorl�ufig scheint der
Jazzvogel sich an den Tieren des Urwalds f�r sein erstes
Deb�t in New York einzu�ben. Hoffen wir also das Beste.
</p>
</body>
</html>

<?xml version="1.0" encoding="ISO-8859-1"?>
<?xml-stylesheet type="text/xsl" href="../../bin/xsl/xml2htm.xsl"?>
<html>
<head>
  <title>M. G. Conrad: Die bayrische K�nigstrag�die im B�rgerhause</title>
  <meta name="type"      content="novelette"/>
  <meta name="booktitle" content="Lenzesfrische, Sturm und Drang"/>
  <meta name="author"    content="Michael Georg Conrad"/>
  <meta name="year"      content="1996"/>
  <meta name="publisher" content="Buchendorfer Verlag"/>
  <meta name="address"   content="M�nchen"/>
  <meta name="isbn"      content="3-927984-55-8"/>
  <meta name="title"     content="Die bayrische K�nigstrag�die im B�rgerhause"/>
  <meta name="pages"     content="101-106"/>
  <meta name="sender"    content="gerd.bouillon@t-online.de"/>
  <link href="../../css/prosa.css" type="text/css" rel="stylesheet"/>
  <meta name="firstpub"  content="1886"/>
</head>
<body>
  <div id="ludwig2" class="chapter">
    <h3 class="author">Michael Georg Conrad</h3>
    <h2 class="title">Die bayrische K�nigstrag�die im B�rgerhause</h2>
    <h3>(1886)</h3>
    <p>Die Abendmahlzeit war still beendet worden; der gro�e eichene Tisch wurde abger�umt, das Fenster mit den Butzenscheiben im Erker der altdeutschen Speisestube ge�ffnet, damit erfrischende Luft hereinstr�me und das Gemach vom letzten Dunst der Speisen reinige. An der dunkeln get�felten Decke zuckten die Flammen des L�sterweibchens. Drau�en rauschte die Isar und der Regen str�mte wie eine S�ndflut hernieder, klatschte auf die wildwogenden Gebirgswasser und erf�llte die Stra�e mit grauen Pf�tzen. Von der Mariahilfkirche in der Au klangen die Abendglocken her�ber, so verweint, so tieftraurig wie ein grauzerw�hltes Chopin'sches Notturno...</p>
    <p>Schweigend hatte sich die Familie mit den G�sten aus der Provinz, dem fr�nkischen Deputierten, einem alten Freund des Hauses, dem Schwiegersohne, einem N�rnberger Fabrikanten, dem jugendlichen Reallehrer, einem hoffnungsvollen Verehrer der einzigen Tochter des Hauses, der sch�nen, blonden, achtzehnj�hrigen Elsa, ihres Zeichens Musiksch�lerin &#8211; in den Salon zur�ckgezogen, den nur eine schwere Draperie von der altdeutschen Speisestube trennte. Blo� der Gro�vater, jetzt noch eine hohe, r�stige Gestalt, einst betriebsamer Bierbrauer von au�erordentlicher Gesch�ftst�chtigkeit, war in der Speisestube zur�ckgeblieben, um in seinem ledergepolsterten Armstuhle sein gewohntes D�mmerst�ndchen zu vertr�umen. Sein Sohn, der Herr dieses behaglichen Heims und Direktor eines Bankgesch�fts, hatte dem Alten nochmals stumm die Hand gedr�ckt und sich mit den andern entfernt. Die Enkels�hne Franz und Paul, vortreffliche Gymnasialsch�ler, waren nicht zum Abendtisch erschienen; bei der Nachricht von der schaudervollen Katastrophe in Schlo� Berg waren sie sofort an den Starnberger See gefahren.</p>
    <p>Elsa kam aus dem Salon zur�ck: �Ach, Gro�vater, ich kann heute nicht spielen, es ist zu entsetzlich.� Dann beugte sie sich zum Erkerfenster hinaus, ihre hervorquellenden Tr�nen mischten sich mit den Regentropfen; Isarrauschen und Glockengel�ute erf�llten sie mit Schauder. Sie trat zur�ck: �O&#160;dieses Nachtlied des Wahnsinns! O&#160;der ungl�ckliche K�nig!�... Schluchzend warf sie sich dem Gro�vater an die Brust; der holte tief Atem und sprach: �Ja, Kind, solche Pfingsten habe ich in meinen sechsundachtzig Jahren nie erlebt; je �lter man wird, desto unglaublicher erscheinen alle Dinge und Verh�ltnisse. Wie furchtbar das Herzeleid und die Verzweiflung, einen solchen K�nig in solchen Tod zu hetzen! Des Himmels Ratschl�sse sind unerforschlich und dieser Welt Weisheit ist Torheit vor Gott... Ich versteh's nicht, ich versteh's nicht...�</p>
    <p>Lebhafte Bewegung und lautes Gespr�ch im Salon: Franz war soeben mit dem letzten Zuge aus Starnberg zur�ckgekehrt. Er hat die Ungl�cksst�tte besichtigt, die Leiche des K�nigs und v.&#160;Guddens gesehen &#8211; und berichtet jetzt in atemloser Hast. Er hat auch einen Pack neuer Extrabl�tter mitgebracht. Das ganze Haus ist aufs neue in Aufruhr.</p>
    <p>�Wo ist Paul?� fragt pl�tzlich die Mutter.</p>
    <p>�Er war nicht fortzubringen. er wollte bis zur Einsargung und Aussegnung des K�nigs bleiben und mit dem Leichenzug gehen. Jetzt sind sie unterwegs, nach Mitternacht treffen sie in M�nchen ein; wi�t Ihr, die Stra�e durch den gro�en Forst, Reiter und Fackeltr�ger, dann der Leichenwagen viersp�nnig, alles schwarz, dann die Geistlichen... alles in tiefdunkler Nacht durch den Wald... wie ein Gespensterzug... der tote K�nig... ich hab' ihn gesehen, wie er dalag auf dem einfachen Bett, mit dem wundersch�nen Gesicht, so friedlich, so majest�tisch... und so... heut nacht bringen sie ihn in seine Residenz...�</p>
    <p>Schluchzen unterbrach seine verworrene Erz�hlung. Der gute J�ngling, vom Schmerz bei der Erinnerung an die furchtbaren Geschehnisse �berw�ltigt, weinte laut auf. In tiefster Ergriffenheit sa� die Familie da. Als h�tte das Schicksal an die Pforte des eigenen Hauses gepocht, als h�tte ein teures Familienmitglied in Nacht und Grauen geendet, so schauderten die Herzen bei dieser K�nigstrag�die. Nun hatte der Tod diesen weltscheuen, so lange in geheimnisvoller, einsamer H�he thronenden K�nig mit einem Schlag zum Gast eines jeden B�rgerhauses gemacht, zum beweinten Liebling eines jeden Herzens!</p>
    <p class="centerbig">*</p>
    <p>Die n�chsten Tage brachten neue G�ste: alte Freunde aus Franken und aus Preu�en. Sogar die K�chin und der Hausdiener wurden in dieser Trauerwoche von Landsleuten aufgesucht, die weit aus dem Gebirge und aus dem Allg�u zu Fu� nach M�nchen gepilgert kamen. Die Stadt wimmelte. Und in dieser ungew�hnlichen Bewegung der ergreifend hervorstechende Zug tief inniger Trauer und Ratlosigkeit. Stra�en und Pl�tze ein schwarzwogendes Menschenmeer, besonders in der N�he der Residenz. Berittene Gendarmen und Milit�r hielten m�hsam die Ordnung aufrecht. Es kam zu unerquicklichen Szenen vor dem Kaiser- und Kapellentor der Residenz: Tausende von Menschen aus allen Teilen des Landes harrten von fr�hester Morgenstunde bis zur einbrechenden Nacht auf Einla�, um den toten K�nig auf dem Paradebett zu sehen und zu beweinen.</p>
    <p>W�hrend in der Tagespresse der reinmenschliche Gesichtspunkt allm�hlich vom politischen verdr�ngt, und der Zusammenhang der Katastrophe mit der Einsetzung der Regentschaft und allen Ma�nahmen, die ihr vorausgegangen, in einer Weise behandelt wurde, die geeignet schien, die erregten Gem�ter zu bes�nftigen und die vom Schmerz verwirrten und erhitzten K�pfe k�hlerem Erw�gen und �berdenken der verwickelten Lage wieder zug�nglicher zu machen, behielt im Volk die freie, ungez�gelte Gef�hlskritik doch noch die Oberhand. Auch als der Sektionsbefund ver�ffentlicht war und wichtige St�cke aus dem Aktenmaterial, das die Staatsminister den Kammern vorgelegt, zu allgemeiner Kenntnis gelangten, blieb noch eine starke Beimischung von Bitternis im Empfinden des Volkes gegen alle, die n�her oder ferner in das Schicksal des K�nigs einzugreifen die schwere Verpflichtung hatten. Selbst das harte Totengericht, das in den Kammern �ber den Monarchen gehalten wurde, vermochte keine nachhaltige Gef�hlswende im Volke herbeizuf�hren. Der Zauber, den das Schicksal Ludwigs auf die Menschheit �bte, konnte nat�rlich in der Landeshauptstadt am wenigsten gebrochen werden; auch jene erlagen ihm, die fr�her selbst an den Sonderbarkeiten des K�nigs, solange er in souver�ner Weltverachtung in seinen Schl�ssern gehaust, scharfen Tadel �bten. Zu der Majest�t der Tradition hatte sich die Majest�t unerh�rter Tragik gesellt &#8211; dazu die ganze Phantastik dieses k�niglichen Lebensr�tsels, das selbst im Tode noch die wunderbarste Legendenbildung herausforderte!</p>
    <p>Eine und dieselbe Empfindung bem�chtigte sich aller Herzen, war auf allen Lippen, in allen Bildern zu lesen. Es war aber nicht weniger nat�rlich, da� neben diesem Allgemeingef�hl der Trauer, das die guten Menschen verbindet und �ber die flache Bedeutungslosigkeit und regelrechte Nichtigkeit des Alltaglebens erhebt, auch die Gemeinheit und innere Verrohung der B�sen ihre kleine Privatorgie feiern wollte.</p>
    <p>Franz hatte seinen Hausknecht geohrfeigt, weil er die Bemerkung fallen lie�: wenn die Geschichte einem beliebigen M�ller oder Huber passiert w�re, h�tte man gewi� kein solches Aufheben davon gemacht.</p>
    <p class="centerbig">*</p>
    <p>W�hrend Franz und Paul einem wahren Fanatismus des Schmerzes um den verg�tterten K�nig sich hingaben in der reinen, schw�rmerischen Unerfahrenheit ihres Gem�tes, hatte sich die poetische Elsa einen Totenkultus m�dchenhafter Art ertr�umt. Sie pl�nderte ihre Sparkasse, um sich s�mtliche Photographien des K�nigs, seiner Schl�sser und Lieblingsorte zu kaufen und in einem �Ludwigs-Album� zu vereinen. In demselben fanden auch Platz die photographischen Nachbildungen mehr oder weniger gelungener Bildwerke der K�nstler, so Koppays �K�nig auf dem Paradebett�, Graf Courten's �Ein ewiges Geheimnis�, das �Gedenkblatt� von Otto Seitz usw. Dann durchst�berte sie ihre Dichter-Bibliothek nach Stellen, die der Klage �ber das unabwendbare Erdenleid seltsamer, erhabener Menschen ergreifenden Ausdruck liehen. Auch befragte sie die Geschichtswerke nach �hnlichen Schicksalen und reihte Parallelen aneinander von dem ungl�cklichen K�nig Saul im Alten Testament bis auf den Kaiser Rudolf&#160;II. und die wahnsinnige Johanna von Spanien.</p>
    <p>Von den Dichtern kamen ihr besonders Lord Byron in seinem �Manfred� und Goethe in seinem �Faust� mit herrlichen Schilderungen entgegen. Hatte sie ihre Phantasie zermartert mit Ausmalung des geheimnisvollen Todeskampfes im Starnberger See und war sie unerm�dlich, von ihren Br�dern Einzelheiten, Mutma�ungen, Hypothesen �ber die tragische Szene zu erfragen, so war sie ganz merkw�rdig ber�hrt, als sie endlich im zweiten Teil des �Faust� auf Stellen traf, die sich wunderbar der Phantastik jener m�rderischen Abendstunde am Seeufer anpa�ten.</p>
    <p>Den Finger auf die betreffenden Stellen gelegt, sprach sie sinnend vor sich hin: Ja, so war's! Er sa� mit Gudden auf der Bank, seinen Entschlu� erw�gend, wie das unertr�gliche Los mit einem Mal zu wenden f�r immer; die Nacht d�mmerte �ber dem See, da vernahm er, wie einst Faust die lockenden Stimmen der Nymphen, ein Gefl�ster aus dem Wasser:</p>
    <table class="poem" summary="">
      <tr>
        <td><i>Am besten gesch�h' dir,<br/>
        Du legtest dich nieder.<br/>
        Erholtest im K�hlen<br/>
        Erm�dete Glieder,<br/>
        Gen�ssest der immer<br/>
        Dich meidenden Ruh';<br/>
        Wir s�useln, wir rieseln,<br/>
        Wir fl�stern dir zu.</i></td>
      </tr>
    </table>
    <p>Dann lief er, den Stimmen folgend, ins Wasser, warf den Mann nieder, der ihn mit Gewalt zur�ckhalten wollte, dr�ben leuchtete die Roseninsel im Abendschein ihm entgegen:</p>
    <table class="poem" summary="">
      <tr>
        <td><i>... O la�t sie walten<br/>
        Die unvergleichlichen Gestalten,<br/>
        Wie sie dorthin mein Auge schickt.<br/>
        So wunderbar bin ich durchdrungen!<br/>
        Sind's Tr�ume? Sind's Erinnerungen?<br/>
        Schon einmal warst du so begl�ckt...</i></td>
      </tr>
    </table>
    <p>Und im seligen Wahn alle Erdenschwere und alle Wirklichkeit absch�ttelnd, schritt er leuchtenden Auges weiter:</p>
    <table class="poem" summary="">
      <tr>
        <td><i>Wundersam! Auch Schw�ne kommen<br/>
        Aus den Buchten hergeschwommen,<br/>
        Majest�tisch rein bewegt...</i></td>
      </tr>
    </table>
    <p>So sank er nieder im Schreiten und Tr�umen und hauchte die k�nigliche Seele aus wie in einem letzten, alle Sinne �berw�ltigenden Traumbild...�</p>
    <p class="centerbig">*</p>
  </div>
  <div class="chapter" id="ludwig22">
    <p>Elsa fand einen gro�en Trost in dieser phantasmagorischen Vorstellung. Nun lebte ihr K�nig herrlich, unentweiht im sch�nen Fabelreich der Dichtung! �Ja, Ludwig wird auch einst seinen Goethe, seinen Byron, seinen Shakespeare finden�, rief sie fast triumphierend ihren Br�dern zu, �wenn auch die heutigen Poeten schweigen oder so schw�chlich singen, da� es einen erbarmen k�nnte, so wenig tragischen Heldensinn in diesen stolzen Dichterk�pfen zu entdecken, die sich an jedem Geburtstag gegenseitig �ffentlich ansingen oder die �Gartenlaube� vollgreinen, wenn ihnen einmal ein Kindlein stirbt...&#160;-</p>
    <p>�Mit den gro�en Ber�hmtheiten�, bemerkte darauf der Reallehrer, �ist in solchen Tagen freilich nicht zu rechnen. Die Politik verdirbt den Charakter &#8211; auch der modernen Olympier. Sie f�rchten sich vielleicht, irgendwo anzusto�en, wer wei�! Bei einem Regentschaftswechsel sind tausend R�cksichten zu nehmen. Einen allm�chtigen Bismarck anzududeln, ist allerdings bequemer. Wer mit den gewaltigen Lebenden geht, hat nichts zu f�rchten. �brigens habe ich doch manches Sch�ne &#8211; ich bin freilich kein kritisches Lumen in poetischen Dingen &#8211; gefunden, so in einem l�ngeren Gedicht von einem mir ganz unbekannten Verfasser folgende Schilderung:</p>
    <p class="center"><i>Die Gesellschaft.</i></p>
    <table class="poem" summary="">
      <tr>
        <td><i>Grau ist der Himmel; von der Alpen Schnee<br/>
        Wogt dichter Nebel nieder �ber'n See,<br/>
        Und was sonst lieblich gl�nzt im Sonnenstrahl,<br/>
        Jetzt liegt es �de wie ein Totental.</i>
        <p class="left"><i>Da schreitet hohen Wuchses, stolz und sch�n,<br/>
          Ein andrer Baldur aus Walhalla's H�hn,<br/>
          Ludwig der Zweite in des Schlosses Park.<br/>
          ...</i></p>
        <p class="left"><i>Der K�nig sieht, verzehrt von Wahnsinns Glut,<br/>
          Die Geister winken aus der dunklen Flut,<br/>
          Und schwer das Herz von ungeheurem Leid,<br/>
          Macht sich der F�rst zum letzten Gang bereit.</i></p>
        <p class="left"><i>Noch einmal denkt er mit verkl�rtem Blick<br/>
          An seiner Tr�ume sch�ne Welt zur�ck;<br/>
          Noch einmal sieht er in des Himmels Blau<br/>
          Aufragen seiner Schl�sser Wunderbau;<br/>
          Noch einmal lauscht er mit verz�cktem Ohr<br/>
          Der Zukunftskl�nge wildbacchant'schem Chor;<br/>
          Noch einmal f�hlt er Siegfried sich und Held &#8211;</i></p>
        <p class="left"><i>Da bricht er einem Edelhirsche gleich,<br/>
          Zum Tode wund, durch's bl�hende Gestr�uch,<br/>
          Da wirft er fieberartig an den Strand,<br/>
          Als wollt' er schlafen gehen, sein Gewand,<br/>
          Und wie bereit zur G�tterd�mmerung<br/>
          Wagt K�nig Ludwig stolz den Todessprung.<br/>
          ...</i></p></td>
      </tr>
    </table>
    <p>�O&#160;geben Sie her!� rief Elsa. �Ich will auch alle Gedichte sammeln, helfen Sie mir! Ich nehme alles zur�ck, was ich Garstiges �ber die heutigen Dichter gesagt habe, wenn Sie mir recht viel Sch�nes zusammenbringen, lieber Herr Professor!�</p>
    <p class="centerbig">*</p>
    <p>Alles war vor�ber: die gro�artige Leichenfeier, die denkw�rdigen Verhandlungen in den Kammern, die erhebende Zeremonie der Eidesleistung des Prinzregenten. M�nchen hatte seine gew�hnliche schlaffe Sommerphysiognomie angenommen. Nur das einst�ndige Trauergel�ute von allen T�rmen, die Trauerabzeichen der Milit�rs- und Hofbeamtenwelt erinnerten noch m�chtig an die schweren Schicksalstage. Auch in den Zeitungen und Unterhaltungen kam das gro�e K�nigsthema noch in allen m�glichen Variationen vor.</p>
    <p>Wieder war die Bankiersfamilie in der Isarquaistra�e mit den nahen und fernen Freunden im Salon versammelt: es war der letzte Abend des Zusammenseins, die Abschiedsfeier. Morgen mu�te alles wieder ins alte Geleis des Wirkens und Schaffens am alten Platze zur�cktreten. Andere Gedanken, andere Sorgen, andere Stimmungen l�sen im ewigen Kreislaufe des Lebens die heutigen ab. Aber die heutigen wollten noch einmal ihr volles Recht haben: in der letzten Gespr�chsstunde gaben sie sich alle noch ein Stelldichein wie zur letzten Heerschau der Geister, welche die au�erordentlichen Ereignisse der Woche entfesselt hatten.</p>
    <p>Der Freund aus Preu�en gefiel sich in gro�en Spr�chen, in welchen er das Ergebnis der wochenlangen Eindr�cke und Debatten zusammendr�ngen wollte. Manches Paradoxon lief dabei mit unter. So sagte er in selbstbewu�tem Berliner Akzent:</p>
    <p>�Es ist �berhaupt tragisch f�r ein so genial begabtes F�rstengeschlecht wie die Wittelsbacher �ber Leute wie Ihr Bajuwaren herrschen zu m�ssen, nehmt mir's nicht �bel! Schon dem prunkliebenden und lebensfreudigen Karl Theodor ist bange geworden wie er die Pfalz verlassen und die altbajuwarische Erbschaft antreten mu�te. Er h�tte Euch damals auch gerne verhandelt und gegen etwas Passenderes umgetauscht, wenn's gegangen w�re!�</p>
    <p>�Soviel ist richtig�, bemerkte der N�rnberger Fabrikherr, �da� die drei letzten bayrischen K�nige in ihren gro�artigen Bestrebungen, ihr M�nchen zur bedeutendsten Kunststadt in deutschen Landen zu erheben, nichts als Hemmnisse, Anfeindungen und boshafte Bemerkungen bei den eigenen altbajuwarischen Landeskindern gefunden haben. Der erste Ludwig hat M�nchen zur Kunstmetropole umgeschaffen &#8211; wider den Willen der M�nchner, die bekanntlich nicht m�de wurden, seine Bauten, seine Bauk�nstler und Maler auf das schlimmste zu glossieren. Max der Zweite berief norddeutsche Gelehrte und Dichter, um den bayrischen Schul- und Literaturgeist aufzufrischen &#8211; und wie krumm wurden ihm diese Berufungen der vielbefehdeten �Nordlichter� genommen! Wie vom ersten Ludwig der K�nstler-, so datiert vom zweiten Max der Gelehrsamkeits- und Literaturruf M�nchens &#8211; was w�rde ohne sie das alte M�nchen in der m�chtig fortgeschrittenen Welt des Geistes von heute bedeuten? Ludwig&#160;II. wollte in seinem genialen Jugendenthusiasmus M�nchen zu einem Weltwunder der musikdramatischen Kunst gestalten &#8211; die M�nchner haben ihm das gr�ndlich verleidet. Man lese doch einmal die Schmutzliteratur nach, die damals produziert wurde zum Entsetzen des jungen idealen Monarchen, der mit gro�artigst begabten K�nstlern wie Richard Wagner und Gottfried Semper den Verkehr abbrechen und seine Pl�ne scheitern lassen mu�te, um das gute M�nchner Volk zu anst�ndiger Laune zur�ckzuf�hren! Ja, h�tte er am Gasteig neue Brauh�user oder neue Kirchen oder Kl�ster gebaut...!�</p>
    <p>�Nun�, sagte der fr�nkische Deputierte, �K�nig Ludwig hat die M�nchner hart genug gestraft f�r die Unbill, die sie ihm damals angetan.�</p>
    <p>�Lange nicht genug!� rief der Preu�e. �Denn in allem sind wir im Norden Euch �ber &#8211; und wenn Ihr Eure K�nige hindert, den S�den in einer Weise zur Geltung zu bringen, die Euch die norddeutschen Volksst�mme nicht nachmachen k�nnen, in den Werken der Phantasie, der Musik, der Kunst, so begeht Ihr eine S�nde wider den heiligen Geist deutscher Nation, die weder in dieser noch in jener Welt vergeben werden kann, laut Bibel!�</p>
    <p>Der Bankdirektor bemerkte beschwichtigend: �Vielleicht w�re es den M�nchnern beim aufgekl�rtesten und besten Willen nicht m�glich geworden, zu den erhabenen Pl�nen ihres K�nigs Ja und Amen zu sagen, auch wenn er sie s�mtlich in M�nchen selbst h�tte zur Ausf�hrung bringen wollen. Ein so in's Ungemessene, Schrankenlose schweifender Geist, wie der seinige, w�re �ber kurz oder lang auch hier an der harten Wirklichkeit der Dinge, besonders im Finanziellen gescheitert...�</p>
    <p>Hier nahm der ehrw�rdige Gro�vater das Wort, der lange still lauschend dagesessen: �Es ist wahr, Ihr Herren, ich habe zuerst die traurige Geschichte nicht fassen k�nnen, auch was die Bedienten �ber ihren k�niglichen Herrn ausgesagt, hat mir nicht sehr imponiert &#8211; da m��te man viele als Narren behandeln, wenn man sie von ihren Dienern, Knechten, K�chen richten lie�e &#8211; aber zuletzt habe ich mir das gesagt: wer �ber seine Mittel gegangen ist und dabei beharrt, zeitlebens �ber seine Mittel gehen zu wollen, unbek�mmert was seine Verwandten, sein Land und die Fremden davon halten, und wie die Unordnung in den Finanzen notwendig auch zur Unordnung im ganzen Regiment, und zuletzt zu allen erdenklichen Schlechtigkeiten verf�hrt, da mu� gewaltsam Z�gel angelegt und Halt geboten werden. Und das ist wahrhaftig nicht zu fr�h geschehen.�</p>
    <p>�Du hast Recht�, f�gte der Bankdirektor den ruhigen, schweren Worten seines greisen Vaters bei, �auch im Geld liegt eine sittliche Macht, die respektiert werden mu� von Hoch und Nieder. Im Erwerben wie im Verbrauchen sprechen sich moralische Prinzipien h�chsten Ranges aus. So sagt mir auch der Rechenschaftsbericht �ber einen Haushalt mehr von dem Grade der Zurechnungsf�higkeit seines Leiters aus, als alle Sektionsbefunde der �rzte und alle Ausplaudereien der Lakeien. Sage mir, wie du in den heutigen schweren Zeiten wirtschaftest, und ich sage dir, wie weit dein Verstand mein Zutrauen verdient. Alle Pracht, alle Idealit�t und alle Genialit�t in Ehren: aber man mu� sich ihren Genu� erschwingen k�nnen und nicht aus den Taschen anderer Leute leisten wollen, denn das w�re nicht blo� Paranoia...�</p>
    <p>�Aber siehst du, Papa�, fiel ihm die liebliche Hausfrau in die Rede: �du �bersiehst, da� der K�nig keine K�nigin hatte, die ihm h�tte wirtschaften helfen. Geniale Menschen ohne Frauenhilfe sind zu allen Zeiten leicht ein Opfer ihres leeren Portemonnaie's geworden &#8211; und ihr strengen Finanzleute tut, als ob aller Verstand und alle Ehrbarkeit nur im Portemonnaie sitze &#8211; wie garstig!�</p>
    <p>Und der Preu�e: �Sehr richtig! Hochachtung, gn�dige Frau! Der K�nig war unzweifelhaft zum Wahnsinn disponiert, er war erblich belastet &#8211; wir sind's schlie�lich alle mehr oder weniger, nur die nicht, die selbst dazu zu dumm sind, die Normalsch�pfe &#8211; allein die Erziehung vermag viel. Eine allzu strenge, zu exklusive und einseitige Prinzenerziehung ist selten ein Gl�ck. Sehen Sie, wie wir's in Preu�en machen: unsere jungen k�niglichen Prinzen m�ssen nicht nur mit den anderen J�nglingen gemeinsam die �ffentliche Gymnasiumsbank dr�cken, sie m�ssen auch bei einem ehrsamen Handwerksmeister allerdurchlauchtigst ein Handwerk lernen. Da wird manche Exzentrizit�t beizeiten aus den K�pfen hinausgehobelt und hinausgedrechselt.�</p>
    <p>Da wagte Elsa ein Wort: �Glauben Sie, da� durch Handwerksk�nste der Einflu� einer guten Frau ersetzt wird?�</p>
    <p>Und der Preu�e: �Nein, niemals, ich schw�re es Ihnen, Fr�ulein! Alle M�nner m�ssen heiraten und die K�nige in erster Linie. Wer nicht heiratet, ist eo ipso ein Narr &#8211; Herr Professor, zu Ihrer besonderen Beherzigung. Allein, auch ein K�nig kann in der Liebe Ungl�ck haben, Numero eins, und Numero zwei: die heiratsf�higen Prinzessinnen der amerikanischen Eisenbahn- und Minen- und Petroleumsquellen-K�nige mit den rasend vielen Millionen sind in Europa noch nicht ebenb�rtig &#8211; und&#160;&#8211;�</p>
    <p>Der Reallehrer war fast erz�rnt �ber die Wendung, die das Gespr�ch durch den Berliner genommen. Darum hatte er, bevor letzterer seine frivole Phrase vollenden konnte, einen guten Ausweg suchend, das Glas ergriffen und nach einer sehr geschickt improvisierten Einleitung einen Toast auf das vielgepr�fte Bayernland und auf den Prinzregenten vorgeschlagen, damit das endlose Gespr�ch �ber die vielen tr�bseligen und bedenklichen Dinge in einem freudigen Gru� an das Leben, an die Kraft und an die bessere Zukunft ausklinge!</p>
    <p>Der Vorschlag gefiel allgemein. �Es lebe Bayern! Es lebe der Prinzregent!� Und die Gl�ser erklangen, und der Gro�vater richtete sich auf und nahm Abschied von den Freunden, denn es war sp�t geworden: �Gott sei mit euch allen, da� so schlimme Zeiten nimmer wiederkehren!�</p>
  </div>
</body>
</html>

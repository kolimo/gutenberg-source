<?xml version="1.0" encoding="ISO-8859-1"?>
<?xml-stylesheet type="text/xsl" href="../../bin/xsl/xml2htm.xsl"?>
<html>
<head>
  <title>Emerenz Meier: Der Juhschroa</title>
  <meta name="type"      content="narrative"/>
  <meta name="booktitle" content="Aus dem Bayerischen Wald"/>
  <meta name="author"    content="Emerenz Meier"/>
  <meta name="year"      content="1974"/>
  <meta name="firstpub"  content="1897"/>
  <meta name="publisher" content="Verlag Morsak"/>
  <meta name="address"   content="Grafenau"/>
  <meta name="isbn"      content="387553049"/>
  <meta name="title"     content="Der Juhschroa"/>
  <meta name="pages"     content="12"/>
  <meta name="created"   content="20121216"/>
  <meta name="sender"    content="gerd.bouillon@t-online.de"/>
  <link href="../../css/prosa.css" type="text/css" rel="stylesheet"/>
</head>
<body>
  <div class="chapter" id="titlepage">
    <h3 class="author">Emerenz Meier</h3>
    <h2 class="title">Der Juhschroa</h2>
  </div>
  <div class="chapter" id="chap001">
    <p>Es ist nun schon an die zehn Jahre her, da� das lustigste Weib, das je auf dieser traurig-lustigen Welt gelebt, seinen letzten Jauchzer ausgesto�en hat. Ich war damals elf Jahre alt und hatte meine Freude an jenem schrillen, markersch�tternden Schrei, dessen Ausklang die Knochenfaust des Todes in der r�chelnden Kehle erstickte.</p>
    <p>�Juhuhu!� t�nte es von dem elenden Lager, auf das die noch elendere Gestalt der achtzigj�hrigen Frau gebettet war, und wir Kinder, die wir ahnungslos fr�hlich wie an sonstigen Tagen im Kreis herum standen, stimmten laut lachend mit ein.</p>
    <p>�Juhuhu� &#8211; das war der letzte Seufzer des Hanserl Enzls.</p>
    <p>Als es lange, ungew�hnlich lange still blieb, als auf unsere neckenden Bemerkungen, auf die Aufforderung zu neuem Jauchzen keine Antwort kam, trat ich ganz an das Kopfende des Bettes und beugte mich �ber unsere Freundin, vorsichtig, scheu, denn ich f�rchtete den raschen Griff ihrer Finger, mit denen sie mich, so oft sie mich erhaschen konnte, derb an den Haaren zauste.</p>
    <p>�Enzl, was ist's?� fl�sterte ich, fuhr aber im n�chsten Moment schreiend zur�ck, denn ich hatte in ein Paar grauer, stierer, verglaster Augen geblickt, in ein gelbblasses Gesicht mit weit offenem Mund, aus dem kein Hauch mehr drang. Mein Geschrei scheuchte die Spielgenossen aus dem halbdunklen Dachraum hinab �ber die Stiege, ich lief hinterdrein.</p>
    <p>�Das Hanserl Enzl ist tot!� schallte es durch das Dorf.</p>
    <p>�Das Enzl? Na, gottlob! Es ist gut f�r sie; Gott mag sie tr�sten.�</p>
    <p>Am Abend gingen wir, zwei meiner Freundinnen und ich, mit den Erwachsenen zur Totenwache, die Furcht vor der Verstorbenen den Vergn�gungen zuliebe �berwindend, die uns bei dieser Gelegenheit winkten. War es doch nur eine Quartiererin, eine in der Gemeinde Umziehende, der gewacht wurde, und da hatten wir das Stillsitzen, das Trauergesichtschneiden und das Lachenverbei�en nicht zu f�rchten. Da schmeckte uns das w�rzige Schwarzbrot besser als zu Hause, da verschm�hten wir selbst ein Schl�ckchen Schnaps nicht, den die B�uerin, der das Enzl als tote Last geblieben war, <a class="pageref" name="page024">24</a> herumreichte. Die Hauptsache aber war, da� sich in der vollgedr�ngten, gl�hhei�en Stube ein M�nnlein befand, ganz hinter dem Ofen versteckt, dessen unscheinbare �u�ere H�lle einen k�stlichen Schatz barg, um den wir Kinder es bei jeder Gelegenheit plagten, n�mlich M�rchen &#8211; wunderbare, abenteuerliche Geschichten und M�rchen. Sie entstanden w�hrend des Erz�hlens hinter seiner niedrigen, von grauem, struppigen Haar umrahmten Stirn und alles horchte atemlos, sobald er nur den Mund �ffnete. Flori hie� der kleine Graue, war �F�tterer� auf dem Hof, sonst auch Holzschuhmacher, <span class="tooltip" title="bitzeln = schnitzeln, Holzger�te verfertigen und ausbessern">Bitzler</span>, �berhaupt ein ganz niedriger und trotzdem unentbehrlicher <span class="tooltip" title="Knecht">Ehehalte</span>.</p>
    <p>Als die Totenschau beendet war, bei der jedermann mit dem Strohwedel Weihwasser auszuspritzen und ein Vaterunser zu beten hatte, als die Leute mit Brot und Branntwein versorgt waren, kam Flori aus der <span class="tooltip" title="Raum hinter dem Ofen">H�hle</span> hervor, lie� sich gravit�tisch am Tisch nieder und erz�hlte, &#8211; erz�hlte von dem Hanserl Enzl. Ich wunderte mich, wie die Leute bei dieser f�r mich bedeutungslosen Schw�tzerei so still sitzen konnten, wunderte mich, da� man nicht schrie:</p>
    <p>�Aber das Hanserl Enzl! Wir kennen es ja unser Lebtag! Erz�hl uns lieber eine sch�ne Geschichte und la� die Quartiererin ruhen oben unterm Dach auf ihrem Strohbett.�</p>
    <p>Wohl horchte ich auch zu, aber ich �rgerte mich dabei und z�rnte der Verstorbenen, z�rnte ihr noch mehr, als es endlich zw�lf Uhr schlug und wir uns zum Rosenkranzgebet knien mu�ten. Da� ich niemals wu�te, ob jetzt das zweite oder vierte Geheimnis kam, ist gewi�, und da� mein Gebet des Enzls Seelenheil wenig gef�rdert hat, ebenso. Aber meine Gedanken besch�ftigten sich mit ihm, mit dem hohen, hageren, immer lustigen Weib, wie es daheim in unserer Gaststube oft herumgesprungen war, zum Erg�tzen der G�ste. Wie es, als Mann verkleidet, bei der Kammerwagenfahrt des Marinibauern das Mautseil �ber die Stra�e gespannt, die Spr�che hergesagt, die Gaben des Br�utigams in Empfang genommen, wie es Schnadah�pfl gesungen und getanzt, wie es sich endlich einen solch kolossalen Nebel angeduselt hatte, da� wir es zu Bett f�hren <a class="pageref" name="page025">25</a> mu�ten. Dieses Enzl hatte mir gefallen; auch dann noch, als es schon lange hinter unserem Dach lag, als es nur selten mehr herauskriechen konnte an die warme, helle Sonne, deren Kind sie einst gewesen, wenn man leichtlebige, nur zu Lust und Freud und Jubel geschaffen scheinende Menschen anders Sonnenkinder hei�en darf.</p>
    <p>Treppauf, treppab ging es den ganzen Tag, denn wir Geschwister hatten unsere Freude an ihrer tollen Art, die sich auch im Elend noch durch Singen, Jauchzen und Schreien �u�erte. Die Mutter war stets besorgt um sie und kochte ihr �extra�, eine Bevorzugung, die Quartierern selten zuteil wird. Mich schien sie zu ihrem Liebling erkoren zu haben, wohl weniger deshalb, weil ich ihre Namenskollegin, als weil ich ihr immer zu Willen war. Denn wenn sie mit ihrer rauhen, m�nnlichen Stimme bat:</p>
    <p>�Enzei, um Gotts-Himmelswilln, gib mir a Seidl Bier� oder: �Enzei, nur grad an oanzige Wurst!� so lief ich heimlich in Keller und Speisekammer, um ihr das Verlangte zu bringen.</p>
    <p>�Bist brav, Dirnei, gar so brav. Wird dir no oamal guat gehn�, sagte sie dann und zauste meine Haare.</p>
    <p>Doch sie mu�te von Haus zu Haus, die Quartiererin, und selbst vor den elendesten H�tten blieb sie nicht verschont. So lag sie Monate hindurch in einem Flachshaus oberhalb des Dorfes, wohin man ihr die Speisen brachte. Auch dort verga� ich sie nicht, und oft kletterte ich den steilen, mit Fichten und F�hren bewachsenen Felsen empor, auf dem das Flachshaus thronte. Als es im Herbst f�r den Flachs ger�umt werden mu�te, wurde Enzl wieder in das Dorf transportiert und dem letzten Bauern verblieb sie. Nun hatte sie noch ein einziges Haus vor sich, das zwar eng und dunkel war, ihr aber doch endlich die bleibende Ruhestatt und dauerndes Quartier gew�hrte.</p>
    <p>Keine Tr�ne flo�, als der lange, m�chtige Sarg in die Grube fuhr, aber erleichtert atmete man auf. Auch ich stand trockenen Auges da, denn wie erfa�t es je ein Kinderherz, was das Knarren der Seile, das Dr�hnen der nachkollernden Erdschollen bedeutet, was mit so einem Sarg alles hinabsinkt in das Dunkel des Vergessens, der Ewigkeit. Welch ein St�ck Schicksal, das mit rastlos tollem Spiel sein Opfer endlich hinabgegaukelt zur Ruhe!</p>
    <p><a class="pageref" name="page026">26</a> Den Todesschrei des Hanserl Enzls verga� ich aber nie. Er hatte seine Bedeutung f�r mich erst erlangt, als ich in das erstarrte Gesicht geblickt, und immer hallte er mir dann in den Ohren. Viele Jahre sp�ter sprach ich mit der Mutter noch von ihr.</p>
    <p class="centerbig">*</p>
    <p>Inmitten des Dorfes Richardsreut stand neben dem ansehnlichen Hof des Rabenbauern, meines Gro�vaters, der des Hanserlbauern. Dieser war ein stolzer und eigensinniger Mann, der mit dem Nachbarn stets in Unfrieden und Feindschaft lebte, einzig aus dem Grunde, weil er es dem Rabenbauer nicht vergeben konnte, da� er seit Jahren schon die W�rde des <span class="tooltip" title="B�rgermeister">Obmanns</span> bekleidete, w�hrend er selbst nur Mitglied des Gemeindeausschusses war. Sein Weib, das lammfromme und geduldige <span class="tooltip" title="Marie">Mirl</span>, hatte unter seinem rauhen, durch Neid und Hoffart verbitterten Wesen viel zu leiden und die Leute begl�ckw�nschten es, als es endlich mit Hinterlassung zweier erwachsener T�chter zur ewigen Ruhe einging.</p>
    <p>Der Hanserlbauer vergo� deswegen keine Tr�ne.</p>
    <p>Als sie, die ihm eben noch zum Abschied die Hand gereicht hatte, sterbend zur�ckgesunken war, zog er die Tabakflasche aus dem <span class="tooltip" title="Brusttasche">Gamsen</span>, schnupfte gem�chlich und schickte dann den Knecht fort mit der Weisung, die ganze Dorfschaft, ausschlie�lich der Rabenbauerschen Familie, auf die Nacht zur Totenwache einzuladen. Dann befahl er der �ltesten Tochter Enzl, die mit finsterem Gesicht neben der Leiche der Mutter stand, die Stube zu fegen, alles auf das Pr�chtigste herzurichten und beim Wirt im Nachbardorf ein Fa� Bier zu bestellen, damit sich die Hanserls vor den Leuten nicht zu sch�men brauchten. Enzl, des Vaters Liebling, tat denn auch nach seinen Worten und schalt auf Leni, die schluchzend und jammernd am Hals der toten Mutter hing und sich um nichts k�mmerte als um ihren Schmerz.</p>
    <p>�Bist du so dumm�, sagte sie, grollend den Besen schwingend, �woanst und pl�rrst und d' Muatta wird doch nimmer lebendig. Trag liaba die sch�nen Bleamibuschn vom Bodn <a class="pageref" name="page027">27</a><span class="tooltip" title="herunter"> aba</span> und die fein <span class="tooltip" title="Leinent�cher">Lalocha</span>, da� 's sauber is, wenn d' Leut kommen. &#8211; Schau, es tuat mir ja auch weh einwendig, so weh, da� i moan, i mu� mir d' Finger abbei�n, aber pl�rrn tua i net.�</p>
    <p>�O Enzl, du woa�t net, wia mir is! I m�cht sterbn, Enzl, m�cht mit der Muatta ins Grab.� Und Leni umfa�te aufs neue das kalte, friedliche Antlitz der Toten und netzte es mit ihren Tr�nen.</p>
    <p>�Dummheit, Lenei! Wer wird sich denn den Tod w�nschen! Wir sind noch so jung &#8211; i bin zwanzg Jahr alt und du achtzehne &#8211; und schon sterbn! O na, na!�</p>
    <p>Mit diesem energischen Protest warf sie den Besen weg, schlang das schwarze, mit bunten Seidenblumen durchwirkte Tuch zierlicher um den Kopf und lief fort ins Dorf, um dort das Bier f�r die Totenwache zu bestellen.</p>
    <p>Als sie durch den Hohlweg schritt und an den gr�nen Felsw�nden emporsah, die bl�hende Schlehen- und Elexenstr�uche zierten, in deren jungem Bl�ttergeb�sch die V�gel zwitscherten, da jauchzte sie pl�tzlich, alles vergessend auf und sang:</p>
    <p class="vers">�Zwanzig Jahr bin i alt,<br/>
      Geh alle Tag in Wald;<br/>
      Geh wohl hinauf die H�h,<br/>
      Such mir an Vierblattklee;<br/>
      Geh wohl hinauf die H�h,<br/>
      Such mir an Klee.</p>
    <p class="vers">Mei Muatta hat allmal gsagt:<br/>
      Wer a treus Herzerl hat,<br/>
      Der hat 's Gl�ck allezeit &#8211;�</p>
    <p>Hier brach ihre rauhe, unsch�ne Stimme j�h ab, denn das Wort �Muatta� hatte ihr den Verlust, den sie heute erlitten, ins Ged�chtnis zur�ckgef�hrt.</p>
    <p class="centerbig">*</p>
    <p>Mirl, deren frommes, sanftes Wesen fr�her auf den unweiblichen, heftigen Charakter der �ltesten Tochter <a class="pageref" name="page028">28</a> fruchtbringend gewirkt hatte, lag seit f�nf Jahren im Grabe und war von allen, Leni ausgenommen, vergessen. Der Hanserlbauer war noch unausstehlicher, Enzl noch herber und lustiger geworden. Sie war ein h�bsches M�dchen, das konnte niemand bestreiten, aber ihr m�nnlich keckes Benehmen wirkte fast absto�end.</p>
    <p>Wo irgend ein toller Streich ver�bt ward, nannte man ihren Namen zuerst und wenn es zwischen den Burschen zu Schl�gereien kam, war sicher Enzl schuld.</p>
    <p>Sie hatte das seidene Kopftuch hinten auf dem Haarnest sitzen, das Merkmal einer flotten Dirn, sie sang in den Wirtsh�usern mit den M�nnern trotz ihrer unsch�nen Stimme um die Wette, sie f�hrte �berall, wo sie hinkam, das gro�e Wort, doch galt sie dennoch als ein ordentliches Weibsbild. Mehrere Freier kamen, haupts�chlich von ihrem Verm�gen angezogen, doch sie wurden heimgeschickt. Da� es aber daraufhin gebrochene Herzen gegeben h�tte, wagte niemand zu behaupten.</p>
    <p>Da starb pl�tzlich der Hanserlbauer, ohne Obmann geworden zu sein. Enzl war drei�ig Jahre alt und man erz�hlt, da� sie, als sie von dem Begr�bnis heimkehrte, vierbl�ttrigen Klee gesucht und gefunden habe:</p>
    <p class="vers">�Mei Muatta hat allmal gsagt:<br/>
      Wer a treus Herzerl hat,<br/>
      Der hat 's Gl�ck allezeit &#8211;�</p>
    <p>Nun war sie Herrin �ber Haus und Hof und Leni ging ungehei�en in den Austrag.</p>
    <p>Das stille, sanfte, tr�umerische M�dchen konnte neben der wilden Schwester nicht leben. Es half wohl allezeit arbeiten in Haus und Feld, tanzte mit beim Ernte-, R�ben- und Brechtanz, doch mehr als alles liebte es die Zur�ckgezogenheit.</p>
    <p>Da die Feindschaft der Nachbarn mit dem Tod des Hanserl ein Ende genommen hatte, entspann sich bald ein reger Verkehr zwischen h�ben und dr�ben. Leni sa� zur Winterszeit stundenlang in des Rabenbauern Stube, spielte mit den j�ngeren M�dchen, lehrte sie spinnen, stricken und h�keln. Und Enzl st�rmte zwanzigmal im Tage herein, Neuigkeiten bringend, Spiele veranstaltend und so weiter. Mirl, des Raben �lteste, war ihre beste Freundin, Enzi, die J�ngste, ihr <a class="pageref" name="page029">29</a> �krausk�pfigs Schatzerl�, Peter ihr guter Kamerad und Hans, der bildsch�ne, braunlockige, blau�ugige, h�nengro�e Mann, ihr was, das sprach weder sie, noch ein anderer Mensch aus. Fragte indessen jemand danach, so machte sie ein grimmiges Gesicht, Hans aber sch�ttelte unwillig die braunen Locken und lachte laut auf.</p>
    <p>Diese sch�nen Locken! Ich habe sie noch, zierlich in Kr�nze geflochten, zwischen Glas und Rahmen und weh wird mir um das Herz, wenn ich sie betrachte.</p>
    <p>�Solch eine Sch�nheit von einem Mann mu�te so fr�h ins Grab�, sagen die alten Leute jetzt noch und das Auge wird ihnen na�.</p>
    <p>Die gem�tliche R�bin, meine Gro�mutter, mochte Enzl wohl leiden um ihrer Lustigkeit willen, mehr noch aber Leni, die mit Mirl, Nanni und Enzi so sinnig plauderte, die mit Peter so kindlich froh scherzte und die den Hans so sehr lieb hatte. Leni half ihr ja auch die Krapfen, die Strauben und Kr�nze backen zum <span class="tooltip" title="Ausdrusch des letzten Getreides">Dengelbo�</span>, Leni teilte Leid und Freud mit ihr, Leni lie� sich sogar herbei, f�r den Raben den Tabak zu reiben und f�r Hans dazu. Wie dann ihre sch�chternen, grauen Augen gl�nzten, wenn dieser mit Kennermiene eine Prise nahm und lebhaft Beifall nickte. Er sprach nicht viel, deshalb hatten seine Worte doppelten Wert.</p>
    <p>�Wenn mir 's Lenei den Tabak reibt, gfreut mi 's Lebn noch mal so stark.�</p>
    <p>Und damit er sich seines sch�nen Lebens recht freuen konnte, rieb sie ihm den Schmalzler allw�chentlich frisch.</p>
    <p>Der Rabenbauer begann alt zu werden und oft sprach er davon, wie froh er w�re, wenn der Sohn den Hof n�hme und heiratete. Hans war ein guter, schaffensfroher Mann, der gerne dabei war, wo es fr�hlich herging, der aber vom Heiraten nichts wissen wollte, zumal wenn er Enzl betrachtete, deren gro�e, graue Augen ihn so eigen anblickten. Und sie war doch so ruhig und dem�tig in seiner N�he, denn sie liebte ihn ja, wie nur ein Weib ihrer Art lieben kann &#8211; toll, wahnsinnig.</p>
    <p>Leni h�rte geduldig zu, wenn die Schwester ihr gegen�ber diesen Gef�hlen Luft machte und wenn sie mit dem Schwur: <a class="pageref" name="page030">30</a> �Mein mu� er wern und g�lts mei Seligkeit!� davonraste, dann schlug das arme M�dchen die H�nde vor das Gesicht und weinte laut auf. Kam Enzi, der kleine Krauskopf, dazu und fragte mitleidig, warum die Hanserlleni weine, so ward ihr die Antwort:</p>
    <p>�O Enzei, vielleicht erfahrst du's selber no oamal an dir!�</p>
    <p class="centerbig">*</p>
    <p>Wieder war ein Jahr dahingegangen und im Rabenbauerhaus bereitete man sich auf eine Hochzeit vor, auf des Hansen Hochzeit. Schreiner, Schneider und Maurer waren da und jeder kehrte in seinem Fach das Unterste zu oberst. Die T�chter n�hten schon an den sch�nen Kleidern, die sie an dem gro�en Tage tragen sollten, die Mutter wirtschaftete aufgeregt umher, der Alte fl�chtete in den verborgensten Winkel, um niemandem im Wege zu stehen und der Br�utigam eilte in das Wirtshaus des Nachbardorfes, um ruhig hinter dem Kruge sitzen bleiben zu k�nnen.</p>
    <p>Die Braut war eine Frauenw�ldlerin, eine h�bsche Dirn, die freilich mehr schwarze L�ckchen um die Stirn und perlwei�e Z�hne im Munde hatte, als Taler im S�ckel. Sie hatte es dem bisherigen Hagestolz eines sch�nen Sonntags angetan, so da� es ihn oft und oft hinaufzog in den d�steren Frauenwald zu dem einsamen H�uschen, in dem die schwarze Franzi wohnte. Da hatte kein Warnen und �berreden, kein Bitten und Drohen von Seiten der Eltern und Geschwister mehr geholfen; er wollte einmal keine andere als die Franzi, die gar nicht so leichtsinnig war, als ihr nachgesagt wurde, und er setzte seinen Willen durch.</p>
    <p>Im ganzen Dorf sprach man von dem bevorstehenden Ereignis und freute sich darauf, nur im Hanserlhaus wurde jedes Wort dar�ber sorglich vermieden. Man wu�te ja, wie Enzl dar�ber dachte und was sie f�hlte &#8211; doch nein, man wu�te es dennoch nicht. Sie lachte und schrie ja den ganzen Tag wie toll, sie sang und jubelte, wie noch nie. Sie fuhr st�ndlich zweimal wie ein lustiges Gewitter in des Raben Stube hinein, scherzte mit den M�dchen, warf ihnen N�h- und Kleiderzeug durcheinander, sie balgte sich mit dem �berm�tigen Schreiner, dem sie die frische Politur des Nu�baumkastens verdorben <a class="pageref" name="page031">31</a> hatte, sie lief dann wieder in das Austragshaus zu Leni, die seit acht Tagen krank im Bett lag und qu�lte sie so lange, bis sie weinte: erst dann gab sich die Wilde f�r kurze Zeit zufrieden.</p>
    <p>Leni war krank, kr�nker als man glauben wollte, trotz der dunklen R�te ihrer Wangen, des hellen Glanzes ihrer Augen.</p>
    <p>Als eines Abends sechs b�hmische Musikanten durch das Dorf wanderten, auf das Rabenbauerhaus zu, wo heute der <span class="tooltip" title="Polterabend">Vortanz</span> abgehalten werden sollte, als dann bald die lustigen Weisen des Dudelsackes in ihr dunkles St�bchen her�berklangen, da richtete sie sich im Bett auf und sagte wiederholt:</p>
    <p>�Enzl, la� mir den Pfarrer kommen, mir is net guat.�</p>
    <p>Enzl versprach es, schlang ihr Tuch um den Kopf, band die Sonntagssch�rze vor und ging, den Knecht zu holen, der dr�ben tanzte. Vor den Fenstern des Nachbarhauses, die hell erleuchtet waren und einen Blick in das frohbelebte Innere gew�hrten, blieb sie wie gebannt stehen. Sie sah Mirl, Nanni und andere M�dchen mit den Dorfburschen tanzen, sah die Alten in fr�hlicher Unterhaltung am Eichentisch sitzen, sah Hans mit lachendem Gesicht an der T�re stehen.</p>
    <p>Jetzt �ffnete er diese und entschwand so ihrem Blick.</p>
    <p>Doch sie mu�te ja hinein zum Knecht.</p>
    <p>Wie ein n�chtlicher Unhold huschte sie um die Ecke der Holzwand und durch das Hoftor, prallte aber pl�tzlich zur�ck.</p>
    <p>�He, bist du's, Enzl?�</p>
    <p>�Ja, i!�</p>
    <p>�Warum kimmst denn net zum Vortanz?�</p>
    <p>�Zum Vortanz? I, &#8211; zu deim Vortanz? Ha, ha, ha, Hans, du bist net gscheit. &#8211; Aber halt, i kimm, &#8211; ja, wennst du mit mir tanzt, Hans, wennst mit mir tanzt. &#8211; So geh, der Pfarrer kann morgn auch kommen, es is no fr�ah gnuag, denn 's Lenei stirbt ja net so gleich!�</p>
    <p>�Der Pfarrer? &#8211; 's Lenei? &#8211; Was?�</p>
    <p>�Ah geh, die dumm Gschicht! Geh, Hans!�</p>
    <p>Sie ri� ihn gewaltsam mit sich hinein in die Stube.</p>
    <p>�An Landler, B�hm, an Landler aufgspielt f�r mich und den Hansen!� schrie sie dort.</p>
    <p><a class="pageref" name="page032">32</a> Sie tanzten ein &#8211; zweimal herum, dann blieb Hans stehen und zwang sie in eine Ecke.</p>
    <p>�Was ist's mit dem Pfarrer und dem Lenei?� fragte er mit halb scheuem, halb ver�chtlichem Blick.</p>
    <p>�Krank is sie, aber zum Sterbn net. Sie wird schon wieder.�</p>
    <p>�So? Wird's? Nachher tanz nur zu, dort hast ein T�nzer.�</p>
    <p>Mit diesen Worten kehrte er ihr den R�cken und ging hinaus.</p>
    <p class="centerbig">*</p>
    <p>In Lenens Kammer herrschte Stille und tiefes Dunkel. Die Kranke sa� im Bett, den Kopf m�de an die h�lzerne Wand gelehnt, und sah hinaus in die freundliche Nacht, deren zahllose, flimmernde Augen ihre Blicke durch das Fenster erwiderten. Sie harrte auf die Schwester, auf den Pfarrer, doch niemand kam.</p>
    <p>Sie wollte weinen, weil man sie so ganz allein lie�, weil sich kein Mensch um sie k�mmerte, aber sie konnte es nicht und ihre magere, fieberhei�e Rechte legte sich zitternd an jene Stelle, wo es so schmerzlich weh tat, wo es so �ngstlich pochte.</p>
    <p>Endlich, horch &#8211; ein Gepolter im Flur, ein Tappen her an der Mauer.</p>
    <p>�Enzl, geh, mir is so bang, i f�rcht mi alloa!� rief sie, den Kopf nach der sich �ffnenden T�r wendend, und:</p>
    <p>�Lenei, arms Lenei, bist leicht krank?� t�nte es leise zur�ck.</p>
    <p>�Mei Herrgott &#8211; Hans, bist es du?�</p>
    <p>�Ja, i bin 's. Warum aber lassen sie dich denn alloa, wennst krank bist?� frug er grollend.</p>
    <p>�O mei Hans, &#8211; i hab halt neamd &#8211; 's Enzl&#160;&#8211;�</p>
    <p>Sie konnte nicht mehr weiter sprechen und sank schluchzend in die Kissen zur�ck.</p>
    <p>Hans suchte ihre beiden H�nde und dr�ckte sie warm.</p>
    <p>�Sei <span class="tooltip" title="still">stad</span>, Lenei, woan net, es wird ja wieder anders. Wennst du oamal wieder gsund bist und wenn d'Hochzeit vorbei is, nachher mua�t alle Tag zu uns kommen, mua�t mir 'n Tabak wieder reibn. Wir alle habn di dann so gern und tuan dir, was wir nur vom Augn absehn. 's Enzl kann treibn, <a class="pageref" name="page033">33</a> was sie will, sie is net wert, da� 's dei Schwester hoa�t.�</p>
    <p>�Ja, habt's mi denn bisher gern ghabt, Hans?�</p>
    <p>�Und wia! Schau, mir geht allweil was ab, wenn i di net sieg und wennst net bald wieder kimmst zum Tabakreibn, lauf i no auf und davon.�</p>
    <p>�Wirkli? &#8211; Ja, i kimm scho wieder, des hoa�t, wenn i no oamal gsund werd. Und heut soll mir der Pfarrer kommen, i hab's dem Enzl gsagt&#160;&#8211;�</p>
    <p>�Morgn, Lenei, morgn! Oder feht's dir wirkli so weit? &#8211; Nachher geh i liaba selbn.�</p>
    <p>Leni dachte nach, ob sie bis morgen noch warten k�nne. Vor einer halben Stunde h�tte sie �nein� gesagt, jetzt aber, da der unerkl�rliche Schmerz im Herzen pl�tzlich wie weggeflogen war, da sich ihr Kopf so frei f�hlte, wie seit langer Zeit nicht mehr, zog sie instinktiv dankbar seine Hand an die Lippen und sagte: �Ja.�</p>
    <p>Als er sich nach einer Viertelstunde wieder entfernte, fa�te sie es nicht, wie sie vorher so verzagt hatte sein k�nnen.</p>
    <p>Am folgenden Morgen kam der Pfarrer. Gerade unterhalb des Dorfes begegnete er dem Hochzeitszug, dem sechs Musikanten vorausmarschierten, und ehrerbietig kniete alles nieder vor dem Sakrament, das er zu der kranken Leni bringen sollte.</p>
    <p>Drei Tage darauf schlummerte diese still und sanft in die Ewigkeit hin�ber und ihr Leib wurde neben die l�ngst vorausgegangenen Eltern gebettet.</p>
    <p>�Ein Engel ist gestorben�, sagten die Leute, �ein Engel, der zu gut war, an der Seite eines Enzls weiter zu leben und zu dulden.�</p>
    <p>Und Enzl blieb, was sie immer gewesen, ein weiblicher Unhold, dessen einzige sanftere Seite bei dem Jubel jener Hochzeit entzweigesprungen war. Nicht ganz vielleicht, denn als sie ein paar Jahre sp�ter am Grabe des Rabenhans stand, den eine heftige Krankheit j�h dahingerafft hatte, da brach sie in Tr�nen aus und ihr wildes Schluchzen �bert�nte den Jammer der Witwe, der Eltern und der Geschwister. Ein d�rres, vierbl�ttriges Kleeblatt nahm sie aus ihrem Gebetbuch, warf es in die Grube und wankte aus dem Kirchhof.</p>
    <p>Nimm ein Holzst�ck und wirf es mit aller Kraft in das Wasser, es taucht unter, schwimmt aber sogleich wieder auf der Oberfl�che dahin: so war es mit Enzl.</p>
    <p><a class="pageref" name="page034">34</a> Sie tauchte schnell wieder empor aus der Flut des Schmerzes, doch ohne gewaschen, gel�utert worden zu sein; sie schwamm dahin mit immerw�hrendem Jubel und Jauchzen, dem Strudel zu, der sie hinunterwirbelte.</p>
    <p>Nach zwanzig Jahren war ihr Hof verkauft, ihr Verm�gen dahin, zum Teil in den H�nden falscher Freunde verschwunden: das einst so reiche, stolze Hanserlenzl war nun eine Quartiererin.</p>
    <p>Zehn Jahre noch zog sie jauchzend von Dorf zu Dorf, von Haus zu Haus, das verk�rperte, lustige Elend.</p>
    <p>Und wie ich sie im Geiste jetzt liegen sehe auf hartem, schmutzigen Bett, die lange Gestalt mit dem gro�knochigen, gelben Gesicht, unheimlich umrahmt von einem schwarzen Tuch, da h�re ich wieder den schrillen, langgezogenen Ton, der ihr Todesschrei war: �Juhuhu!�</p>
    <p>&#160;</p>
    <p class="centerbig">*</p>
    <p>&#160;</p>
  </div>
</body>
</html>

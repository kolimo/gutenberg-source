<?xml version="1.0" encoding="ISO-8859-1"?>
<?xml-stylesheet type="text/xsl" href="../../bin/xsl/xml2htm.xsl"?>
<html>
<head>
  <title>Sonnenschein-Geschichten</title>
  <meta name="type"      content="fairy"/>
  <meta name="author"    content="Andersen, Hans Christian"/>
  <meta name="booktitle" content="Andersens M�rchen"/>
  <meta name="publisher" content="Gondrom Verlag"/>
  <meta name="year"      content="1976"/>
  <meta name="title"     content="Sonnenschein-Geschichten"/>
  <meta name="sender"    content="Lorelies.Kuhn@t-online.de"/>
  <meta name="created"   content="20010222"/>
  <link href="../../css/prosa.css" rel="stylesheet" type="text/css"/>
</head>
<body>
  <h2 class="title">Sonnenschein-Geschichten</h2>
  <h3 class="author">Hans Christian Andersen</h3>
  <p>"Jetzt werde ich erz�hlen!" sagte der Wind.</p>
  <p>"Nein, erlauben Sie, "sagte das Regenwetter, "jetzt ist die Reihe an mir! Sie haben lange genug an der Stra�enecke gestanden und haben geheult, was Sie heulen konnten!"</p>
  <p>"Ist das der Dank", sagte der Wind, "weil ich Ihnen zur Ehre manchen Regenschirm umgekippt, ja zerknickt habe, wenn die Leute nichts von Ihnen wissen wollten!"</p>
  <p>"Ich erz�hle!" sagte der Sonnenschein. "Still!", und das wurde mit Glanz und Majest�t gesagt, so da� der Wind sich legte, so lang er war, aber das Regenwetter r�ttelte den Wind und sagte: "Da� wir das ertragen m�ssen! Sie bricht immer durch, diese Madame Sonnenschein. Wir wollen sie nicht anh�ren! Es ist der M�he nicht wert!"</p>
  <p>Und der Sonnenschein erz�hlte:</p>
  <p>"Es flog ein Schwan �ber das rollende Meer dahin, jede Feder desselben leuchtete wie Gold; eine Feder fiel herab auf das gro�e Kaufmannsschiff, welches mit vollen Segeln vor�berglitt; die Feder fiel auf den Lockenkopf eines jungen Mannes, des Beaufsichtigers der Waren, Superkargo nannten sie ihn. Die Feder des Gl�cksvogels ber�hre seine Stirn, wurde zur Schreibfeder in seiner Hand, und er wurde bald der reiche Kaufmann, der sich schon Sporen von Gold kaufen und eine goldne Sch�ssel in einen Adelsschild verwandeln konnte; ich habe den Schild beschienen!" sagte der Sonnenschein.</p>
  <p>"Der Schwan flog �ber die gr�ne Wiese dahin, wo der kleine Schafh�ter, ein Knaben von sieben Jahren, sich in den Schatten des alten einzigen Baumes hingestreckt hatte. Und der Schwan in seinem Fluge k��te ein Blatt des Baumes und das Blatt fiel herab, in die Hand des Knaben, und dieses eine Blatt wurde zu dreien, wurde zu zehn Bl�ttern, wurde zu einem ganzen Buch, und der Knabe las in demselben von den Wunderwerken der Natur, von der Muttersprache, von Glauben und Wissen. Wenn er schlafen ging, legte er das Buch unter seinen Kopf, damit er nicht vergessen m�chte, was er gelesen, und das Buch trug ihn auf die Schulbank und zu dem Tische des Gelehrten. Ich habe seinen Namen unter denen der Gelehrten gelesen!" sagte der Sonnenschein.</p>
  <p>"Der Schwan flog in die Waldeinsamkeit, ruhte sich dort aus auf den stillen dunklen Seen, wo die Wasserlilien bl�hen, wo die wilden Wald�pfel wachsen und des Kuckucks und der Waldtaube Heimat ist. Eine arme Frau las dort Reisig auf; herabgefallene Baumzweige, die sie in einem B�ndel auf ihrem R�cken trug, ihr Kind trug sie an der Brust, und so wanderte sie ihren Weg nach Hause. Sie sah den goldenen Schwan, den Gl�cksschwan, sich von dem schilfbewachsenen Ufer emporschwingen. Was gl�nzte dort? Ein goldiges Ei; es war noch warm. Sie legte es an ihre Brust, und es blieb warm, das Ei hatte gewi� Leben. Ja, es tickte hinter der Schale; sie f�hlte das und glaubte, es sei ihr eigenes Herz, welches klopfte.</p>
  <p>Zu Hause in ihrem �rmlichen St�bchen nahm sie das Goldei hervor. Ticktick! sagte es, als sei es eine k�stliche goldene Uhr, aber es war ein Ei mit lebendigem Leben. Das Ei platzte, ein kleines Schwanenjunges, wie aus dem reisten Gold, strecke das K�pfchen hervor; das Junge hatte vier Ringe und den Hals, und da die arme Frau gerade vier Knaben, drei zu Hause und den vierten, den sie bei sich in der Waldeinsamkeit getragen, hatte, so begriff sie sogleich, da� hier ein Ring f�r jedes der Kinder sei, und indem sie das begriff, flog der kleine Goldvogel davon.</p>
  <p>Sie k��te jeden Ring, lie� jedes der Kinder einen von den Ringen k�ssen, legte diesen an das Herz des Kindes und steckte ihn an seinen Finder.</p>
  <p>"Ich sah dies alles" sagte der Sonnenschein, "ich sah auch, was darauf geschah.</p>
  <p>Der eine Knabe setzt sich an den Lehmgraben, nahm einen Klumpen Lehm zur Hand, drehte und formte ihn mit den Fingern, und es wurde eine Jasongestalt daraus, die, welche das Goldene Vlies geholt hatte.</p>
  <p>Der zweite der Knaben lief sogleich auf die Wiese hinaus, wo Blumen in allen denkbaren Farben standen; er pfl�ckte eine Handvoll, dr�ckte sie, da� der Saft ihm in die Augen spritzte und den Ring benetzt; es kribbelte und krabbelte in Gedanken und in der Hand, und nach Jahren und Tagen sprach die gro�e Stadt von dem gro�en Meister.</p>
  <p>Der dritte der Knaben hielt den Ring so fest in seinem Mund, da� er Klang und Widerhall vom Herzboden gab, Gef�hle und Gedanken stiegen empor in T�nen, stiegen empor wie singende Schw�ne, tauchten wie Schw�ne hinab in den tiefen See, den tiefen See der Gedanken; er wurde ein Meister der T�ne, jedes Land kann nun sagen: "Mir geh�rt er an!"</p>
  <p>Der vierte Kleine, ja, der war nun der Zur�ckgesetzte; er habe den Pips, sagten die Leute, er m�sse Pfeffer und Butter haben, wie die kranken H�hner, geschmiert werden &#8211; und das meinten sie nun in ihrem Sinne, und Schmiere bekam er, aber von mir bekam er einen Sonnenschein-Ku�!" sagte der Sonnenschein. "Er bekam zehn K�sse f�r einen. Er war eine Dichter-Natur, er wurde gebliebkost und gek��t; aber den Gl�cksring hatte er vom goldenen Schwan des Gl�cks. Seine Gedanken flogen aus wie singende Schmetterlinge, der Unsterblichkeit Symbol."</p>
  <p>"Das war eine lange Geschichte!" sagte der Wind.</p>
  <p>"Und langweilig!" sagte das Regenwetter.</p>
  <p>"Blase mich an, da� ich mich wieder erhole!"</p>
  <p>Und der Wind blies, und der Sonnenschein erz�hlte:</p>
  <p>Der Gl�cksschwan flog �ber den tiefen Meerbusen dahin, wo die Fischer ihr Garn ausgeworfen hatte. Der �rmste derselben dachte daran, sich zu verheiraten und er heiratete.</p>
  <p>Ihm brachte der Schwan ein St�ck Bernstein, und der Bernstein zieht an, er zog die Herzen ans Haus heran. Bernstein ist die sch�nste R�ucherung. Es kam ein Duft ins Haus wie aus der Kirche, ein Duft wie aus der Natur Gottes. Er und die Seinen empfangen so recht das Gl�ck des h�uslichen Lebens, Zufriedenheit n kleinen Verh�ltnisse, und ihr Leben gestaltete sich deshalb auch zu einer ganzen Sonnenschein-Geschichte."</p>
  <p>"Brechen wir aber jetzt ab!" sagte der Wind. "Nun hat der Sonnenschein lange genug erz�hlt. Ich habe mich gelangweilt!"</p>
  <p>"Ich auch", sagte das Regenwetter.</p>
  <p>Was sagen nun wir anderen, die wir die Geschichten geh�rt haben?</p>
  <p>Wir sagen: "Nun sind sie aus!".</p>
</body>
</html>

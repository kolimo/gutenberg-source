<?xml version="1.0" encoding="ISO-8859-1"?>
<?xml-stylesheet type="text/xsl" href="../../bin/xsl/xml2htm.xsl"?>
<html>
<head>
  <title>Theophrastus Paracelsus in Salzburg</title>
  <meta name="type"      content="legend"/>
  <meta name="booktitle" content="Die sch�nsten Sagen aus �sterreich"/>
  <meta name="title"     content="Theophrastus Paracelsus in Salzburg"/>
  

  
  <meta name="copyright" content="Sonderausgabe"/>
  <meta name="sender"    content="harald.aichmayr@netway.at"/>
  <meta name="isbn"      content="3-85001-573-4"/>
  <meta name="author"    content="�berlieferung"/>
  <link href="../../css/prosa.css" rel="stylesheet" type="text/css"/>
</head>
<body>
  <h2 class="title">Theophrastus Paracelsus in Salzburg</h2>
  <p>Allerlei sonderbare Geschichten sind �ber das Leben und Wirken dieses Mannes im Umlauf, der sich als Arzt einen ber�hmten Namen gemacht hat und einen gro�en Teil seines Lebens in Salzburg verbrachte, wo er auch begraben liegt. Sein Ruf drang weit �ber die Grenzen Salzburgs hinaus, und von allen Seiten kamen Kranke und Leidende nach Salzburg, um bei ihm Rat und Hilfe zu finden. Da ihm die schwierigsten Kuren gl�ckten, hie� er im Volk bald, Paracelsus wirke Wunder, verf�ge �ber geheime Zauberkr�fte, besitze ein wundert�tiges Lebenselixier, von dem ein Tropfen gen�ge, alle Krankheitskeime zu vernichten, die halberloschenen Lebensgeister aufs neue zu entflammen und das Leben auf hundert Jahre zu verl�ngern; er k�nne Gold machen und verstehe die Sprache der Tiere und Pflanzen. Seine Feinde und Neider dagegen sagten ihm alles �ble nach und behaupteten sogar, er stehe mit dem Teufel im Bunde.</p>
  <p>Als Theophrastus noch in Innsbruck studierte, ging er h�ufig des Morgens in den Wald, um sich in Gottes Einsamkeit sinnend zu ergehen. Wie er einmal so dahinwanderte, h�rte er, ohne jemanden zu sehen, seinen Namen rufen.</p>
  <p>�Wer ruft mich da?� fragte er.</p>
  <p>�Ich�, erscholl es zur Antwort. �Erl�se mich aus dieser Tanne, in der ich eingeschlossen bin!�</p>
  <p>�Wer ist dieses Ich?� erkundigte sich Theophrast</p>
  <p>�Man nennt mich den B�sen�, gab die Stimme zur Antwort, �aber du wirst sehen, da� ich zu Unrecht so hei�e, wenn du mich befreist.�</p>
  <p>�Wie kann ich das?�</p>
  <p>�Schau dort rechts an der alten Tanne empor, da wirst du ein Z�pfchen mit drei Kreuzen bemerken, das ich von innen nicht heraussto�en kann. Ein Geisterbeschw�rer hat mich da hineingezw�ngt.�</p>
  <p>�Und was soll mein Lohn sein, wenn ich dich befreie?� �Was verlangst du denn?�</p>
  <p>�Erstens eine Arznei, die jede Krankheit zu heilen vermag, und zweitens ein Mittel, das alles zu Gold verwandelt, was ich damit ber�hre.�</p>
  <p>�Beides sollst du haben!�</p>
  <p>�Wer b�rgt mir aber daf�r, da� du mich nicht betr�gst?� �Ich, so wahr ich der Teufel bin!�</p>
  <p>�Nun gut, dann will ich dich befreien, wenn ich das Z�pflein herauszuziehen vermag.�</p>
  <p>Theophrast trat an den Baum heran, lockerte das Z�pfchen mit seinem Messer und zog es mit vieler M�he aus dem Stamm heraus. Dann harrte er gespannt, was sich weiter ereignen werde. Da kroch langsam eine gro�e schwarze Spinne aus dem Loch heraus, lie� sich an einem Faden auf das Moos herab und war pl�tzlich verschwunden. Daf�r stand ein langer, hagerer Mann mit grinsender Teufelsfratze vor ihm, dessen langer roter Mantel nur schlecht den Pferdefu� seines Tr�gers verbarg.</p>
  <p>�Komm mit!� rief der Teufel, ri� sich eine Haselrute ab und schritt mit Theophrast zum n�chsten Felsen, der zwischen den Tannen emporragte. Auf einen Schlag mit der Gerte spaltete sich das Gestein. �Warte hier eine Weile, ich bin gleich wieder da�, meinte der Satan zu seinem Begleiter und trat in die entstandene Kluft hinein. Schon nach wenigen Minuten erschien er wieder und hielt dem Doktor zwei Fl�schchen entgegen, wobei er erkl�rte: �Hier sind die versprochenen Mittel! In dem gelben Fl�schchen ist die Goldtinktur, im wei�en die Arznei!� Dann f�gte er noch hinzu: �Gehst du mit nach Innsbruck? Ich m�chte mir nur rasch den Mann holen, der mich in den Baumstamm hineinbeschworen hat. Der denkt sicher nicht, da� ich so rasch aus dem Loch wieder herausgekommen bin.�</p>
  <p>Theophrast tat dieser Mann leid, und er wollte ihn retten. Aber er konnte ihn nicht warnen; denn er wu�te ja nicht, wie er hei�e und wo er wohne; auch war der Teufel viel flinker als er! Da kam ihm ein guter Gedanke: er wollte den Teufel bei seiner Eitelkeit packen. Als sie wieder an der Tanne vor�berkamen, worin der Teufel gesteckt hatte, sagte er: �Dieser Geisterbeschw�rer mu� aber viel Macht haben, da� er imstande war, Euch in ein so kleines L�chlein hineinzuzw�ngen. Von selbst br�chtet Ihr es wohl kaum zuwege, Euch in eine so kleine Spinne zusammenzuziehen.�</p>
  <p>�Mein Lieber, da irrst du gewaltig�, erwiderte der B�se, �berlegen grinsend. �Der Teufel kann manches, wovon ihr armseligen Erdenkinder keine Ahnung habt. Was gilt's, da� ich mich vor deinen Augen sogleich in eine kleine Spinne verwandle und wieder in das L�chlein krieche?�</p>
  <p>�Ja, wenn Ihr das imstande seid�, versetzte der Doktor, �sollt Ihr die beiden Fl�schchen wiederhaben, die Ihr mir soeben gebracht habt; denn ein solches unbegreifliches Kunstst�ck zu sehen, w�re mir kein Opfer zu gro�.�</p>
  <p>�So schau her und staune!� rief der Teufel und verwandelte sich flugs in eine schwarze Spinne, die den Stamm des Baumes hinaufkrabbelte und in dem kleinen Loch verschwand.</p>
  <p>Kaum war er drinnen, ergriff Theophrast das Z�pflein, trieb und h�mmerte es mit aller Kraft in den Baumstamm und schnitzte mit seinem Messer drei neue Kreuze drauf. Dann verlie� er schleunig den Wald, den Teufel seinem Schicksal �berlassend. Drau�en auf der blumigen Wiese aber blieb er stehen und sagte bei sich: �Jetzt m�chte ich doch einmal sehen, ob mich der Teufel nicht etwa betrogen hat.� Er �ffnete das gelbe Fl�schchen und lie� ein Tr�pflein daraus auf seine Hand lallen. Und wirklich, das Tr�pflein nahm zu an Schwere in seiner Hand und wurde zu gediegenem Gold. Da freute sich Theophrast und dachte, das andere Fl�schchen wolle er beim n�chsten Kranken probieren, der ihm unterkommen werde. Unweit dieser Stelle lag in einer einfachen H�tte ein armer J�ger krank danieder. Einige Tropfen der Medizin, die Theophrast ihm reichte, gen�gten, den Mann von seiner Krankheit zu heilen.</p>
  <p>Froh �ber die Wundermittel, die er dem Teufel entlockt hatte, schritt der Doktor nun heimw�rts und wurde in kurzer Zeit der ber�hmteste Arzt des Landes.</p>
  <p>Jahrelang �bte Theophrastus seine Kunst und machte viele Menschen, die von andern �rzten schon aufgegeben waren, wieder kerngesund. Sein Ansehen und sein Ruhm wuchsen und drangen in immer weitere Kreise; freilich wuchsen auch der Neid und der Arger der �brigen �rzte in der Stadt, die allm�hlich ihre ganze Kundschaft verloren. Wiederholt trachteten sie, ihn aus dem Weg zu r�umen, aber nie gl�ckte der abscheuliche Plan. Schlie�lich aber fiel er doch der T�cke seiner Rivalen zum Opfer. Es gelang ihnen n�mlich, dem Doktor zerriebene Diamantenk�rner beizubringen, gegen deren t�dliche Wirkung kein Mittel bekannt war.</p>
  <p>Als Theophrast nun sterbenskrank in seinem Wohnhaus am �Platzl� in Salzburg daniederlag und f�hlte, da� sein Ende nahe sei, rief er seinen Diener, gab ihm ein Fl�schchen mit gelber Fl�ssigkeit und befahl ihm, es sogleich in die Salzach zu sch�tten. Der aber meinte, das Fl�schchen enthalte den Zaubertrank, mit dem sein Herr so viele Wunderkuren ausgef�hrt hatte, und dachte: Das Fl�schchen behalte ich mir. Ich w�re doch ein Narr, wenn ich mir die Gelegenheit entgehen lie�e, reich und angesehen zu werden. So kehrte er nach kurzer Zeit, ohne den Befehl auszuf�hren, an das Krankenbett seines Herrn zur�ck und sagte: �Herr, ich habe getan, was Ihr mir befohlen habt.�</p>
  <p>�Und was sahst du�, fragte Theophrast, �als die Fl�ssigkeit ins Wasser rann?�</p>
  <p>�Nichts�, entgegnete verwundert der Diener.</p>
  <p>Da fuhr der Kranke von seinem Lager auf und rief: �Nichtsw�rdiger Mensch, so vollziehst du meine Befehle. Du wolltest mich betr�gen und bedachtest nicht, da� ich alles erfahre. Geh sofort und f�hre meinen Befehl aus oder, bei Gott, du sollst meinen Zorn noch einmal zu f�hlen bekommen!�</p>
  <p>Erschrocken eilte der Diener, der den Zorn seines Herrn f�rchtete, so rasch er konnte zur Salzach und tat, wie ihn Theophrastus gehei�en. Als sich der Wundertrank mit den Fluten der Salzach vermischte, gl�nzte lauteres Gold zu dem erstaunt in den Flu� hinabschauenden Diener empor. Seitdem f�hrt die Salzach Goldk�rner auf ihrem Grund.</p>
  <p>Theophrastus aber wollte sich noch nicht mit dem Tod abfinden und ein letztes Mittel gegen die vernichtende Wirkung der Diamantenk�rner versuchen. Er gebot dem Diener, aus der Krankenstube zu gehen und sich vor Abend nicht mehr blicken zu lassen, fing zwei Spinnen und zwang sie, in seinen Schlund hinabzukriechen, um die Diamantenk�rner heraufzuholen. Schon hatten die Tiere sie beinahe bis in den Mund heraufgebracht, und im n�chsten Augenblick w�re der Kranke gerettet gewesen, da �ffnete der Diener zur Unzeit die T�r. Das Knarren der Angeln erschreckte die Spinnen, sie lie�en die K�rner wieder in den Magen hinabfallen, und nun war das Schicksal des Wunderdoktors besiegelt. Theophrastus war unrettbar dem Tod verfallen und starb nach wenigen Tagen in seinem Wohnhaus am �Platzl� in Salzburg.</p>
</body>
</html>

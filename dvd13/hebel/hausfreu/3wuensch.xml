<?xml version="1.0" encoding="ISO-8859-1"?>
<?xml-stylesheet type="text/xsl" href="../../bin/xsl/xml2htm.xsl"?>
<html>
<head>
  <title>Drei W�nsche</title>
  <link rel="stylesheet" href="../../css/prosa.css" type="text/css"/>
  <meta name="title"    content="Drei W�nsche"/>
  <meta name="author"   content="Johann Peter Hebel"/>
  <meta name="type"     content="narrative"/>
  <meta name="author"   content="Johann Peter Hebel"/>
  <meta name="created"  content="20020415"/>
  <meta name="sender"   content="h.guhl@bluewin.ch"/>
  <meta name="firstpub" content="1811"/>
</head>
<body>
  <h3 class="author">Johann Peter Hebel</h3>
  <h2 class="title">Drei W�nsche</h2>
  <p>Ein junges Ehepaar lebte recht vergn�gt und gl�cklich beisammen und hatte den einzigen Fehler, der in jeder menschlichen Brust daheim ist: wenn man's gut hat, h�tt man's gerne besser. Aus diesem Fehler entstehen so viele t�richte W�nsche, woran es unserm Hans und seiner Liese auch nicht fehlte. Bald w�nschten sie des Schulzen Acker, bald des L�wenwirts Geld, bald des Meyers Haus und Hof und Vieh, bald einmal hunderttausend Millionen bayerische Taler kurzweg. Eines Abends aber, als sie friedlich am Ofen sassen und N�sse aufklopften und schon ein tiefes Loch in den Stein hineingeklopft hatten, kam durch die Kammert�r ein weisses Weiblein herein, nicht mehr als eine Elle lang, aber wundersch�n von Gestalt und Angesicht, und die ganze Stube war voll Rosenduft. Das Licht l�schte aus, aber ein Schimmer wie Morgenrot, wenn die Sonne nicht mehr fern ist, strahlte von dem Weiblein aus und �berzog alle W�nde. �ber so etwas kann man nun doch ein wenig erschrecken, so sch�n es aussehen mag. Aber unser gutes Ehepaar erholte sich doch bald wieder, als das Fr�ulein mit wunders�sser, silberreiner Stimme sprach: �Ich bin eure Freundin, die Bergfei Anna Fritze, die im kristallenen Schloss mitten in den Bergen wohnt, mit unsichtbarer Hand Gold in den Rheinsand streut und �ber siebenhundert dienstbare Geister gebietet. Drei W�nsche d�rft ihr tun; drei W�nsche sollen erf�llt werden.� Hans dr�ckte den Ellenbogen an den Arm seiner Frau, als ob er sagen wollte: Das lautet nicht �bel. Die Frau aber war schon im Begriff, den Mund zu �ffnen und etwas von ein paar Dutzend goldgestickten Kappen, seidenen Halst�chern und dergleichen zur Sprache zu bringen, als die Bergfei sie mit aufgehobenem Zeigefinger warnte: �Acht Tage lang�, sagte sie, �habt ihr Zeit. Bedenkt euch wohl und �bereilt euch nicht.� Das ist kein Fehler, dachte der Mann und legte seiner Frau die Hand auf den Mund. Das Bergfr�ulein aber verschwand. Die Lampe brannte wie vorher, und statt des Rosendufts zog wieder wie eine Wolke am Himmel der �ldampf durch die Stube.</p>
  <p>So gl�cklich nun unsere guten Leute in der Hoffnung schon zum voraus waren und keinen Stern mehr am Himmel sahen, sondern lauter Bassgeigen, so waren sie jetzt doch recht �bel dran, weil sie vor lauter Wunsch nicht wussten, was sie w�nschen wollten, und nicht einmal das Herz hatten, recht daran zu denken oder davon zu sprechen, aus Furcht, es m�chte f�r gew�nscht passieren, ehe sie es genug �berlegt h�tten. �Nun�, sagte die Frau, �wir haben ja noch Zeit bis am Freitag.�</p>
  <p>Des andern Abends, w�hrend die Grundbirn zum Nachtessen in der Pfanne prasselten, standen beide, Mann und Frau, vergn�gt an dem Feuer beisammen, sahen zu, wie die kleinen Feuerf�nklein an der russigen Pfanne hin und her z�ngelten, bald angingen, bald ausl�schten, und waren, ohne ein Wort zu reden, vertieft in ihrem k�nftigen Gl�ck. Als sie aber die ger�steten Grundbirn aus der Pfanne auf das Pl�ttlein anrichteten und ihr der Geruch lieblich in die Nase stieg: &#8211; �Wenn wir jetzt nur ein gebratenes W�rstlein dazu h�tten�, sagte sie in aller Unschuld und ohne an etwas anders zu denken, und &#8211; o weh, da war der erste Wunsch getan. &#8211; Schnell wie ein Blitz kommt und vergeht, kam es wieder wie Morgenrot und Rosenduft untereinander durch das Kamin herab, und auf den Grundbirn lag die sch�nste Bratwurst. &#8211; Wie gew�nscht, so geschehen. &#8211; Wer sollte sich �ber einen solchen Wunsch und seine Erf�llung nicht �rgern? Welcher Mann �ber solche Unvorsichtigkeit seiner Frau nicht unwillig werden? �Wenn dir doch nur die Wurst an der Nase angewachsen w�re�, sprach er in der ersten �berraschung, auch in aller Unschuld, und ohne an etwas anders zu denken &#8211; und wie gew�nscht so geschehen. Kaum war das letzte Wort gesprochen, so sass die Wurst auf der Nase des guten Weibes fest, wie angewachsen im Mutterleib und hing zu beiden Seiten hinab wie ein Husarenschnauzbart.</p>
  <p>Nun war die Not der armen Eheleute erst recht gross. Zwei W�nsche waren getan und vor�ber, und noch waren sie um keinen Heller und um kein Weizenkorn, sondern nur um eine b�se Bratwurst reicher.</p>
  <p>Noch war ein Wunsch zwar �brig. Aber was half nun aller Reichtum und alles Gl�ck zu einer solchen Nasenzierat der Hausfrau? Wollten sie wohl oder �bel, so mussten sie die Bergfei bitten, mit unsichtbarer Hand Barbiersdienste zu leisten und Frau Liese wieder von der vermaledeiten Wurst zu befreien. Wie gebeten, so geschehen, und so war der dritte Wunsch auch vor�ber, und die armen Eheleute sahen einander an, waren der n�mliche Hans und die n�mliche Liese, nachher wie vorher, und die sch�ne Bergfei kam niemals wieder.</p>
  <p>Merke: Wenn dir einmal die Bergfei also kommen sollte, so sei nicht geizig, sondern w�nsche</p>
  <p>Numero eins: Verstand, dass du wissen m�gest, was du</p>
  <p>Numero Zwei w�nschen sollest, um gl�cklich zu werden. Und weil es leicht m�glich w�re, dass du alsdann etwas w�hltest, was ein t�richter Mensch nicht hoch anschl�gt, so bitte noch</p>
  <p>Numero Drei: um best�ndige Zufriedenheit und keine Reue.</p>
  <p>Oder so</p>
  <p>Alle Gelegenheit, gl�cklich zu werden, hilft nichts, wer den Verstand nicht hat, sie zu benutzen.</p>
</body>
</html>

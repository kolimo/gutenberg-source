<?xml version="1.0" encoding="ISO-8859-1"?>
<?xml-stylesheet type="text/xsl" href="../../bin/xsl/xml2htm.xsl"?>
<html>
<head>
  <title>Fritz Mauthner: Aus dem M�rchenbuch der Wahrheit</title>
  <meta name="type"      content="sketch"/>
  <meta name="booktitle" content="Vom armen Franischko"/>
  <meta name="author"    content="Fritz Mauthner"/>
  <meta name="year"      content="1994"/>
  <meta name="publisher" content="Oberon Verlag"/>
  <meta name="address"   content="Frankfurt am Main"/>
  <meta name="isbn"      content="3-925844-08-2"/>
  <meta name="title"     content="Aus dem M�rchenbuch der Wahrheit"/>
  <meta name="pages"     content="123-138"/>
  <meta name="created"   content="19991226"/>
  <meta name="sender"    content="gerd.bouillon@t-online.de"/>
  <meta name="firstpub"  content="1899"/>
  <link href="../../css/prosa.css" type="text/css" rel="stylesheet"/>
</head>
<body>
  <div id="titlepage" class="chapter">
    <h3 class="author">Fritz Mauthner</h3>
    <h2 class="title">Aus dem M�rchenbuch der Wahrheit</h2>
    <h3>Prosaminiaturen</h3>
  </div>
  <div class="chapter" id="wahr1">
    <h3>Die Palme und die Menschensprache</h3>
    <p>Am niederen Ufer des Kongo standen zwei Palmen, eine alte, hohe, mit Fr�chten behangene, nicht weit ab eine junge, schlanke, nicht gr��er als drei Menschenzwerge, und die bl�hte zum erstenmal. Die junge Palme dachte gar nichts, denn sie bl�hte. Die alte sann seit Jahren nach und wollte etwas sagen. Doch alles, was sie durch Biegen und Rauschen zustande brachte, war doch immer nur: Es ist schw�l, es regnet, und so �hnlich. Da beneidete sie die Menschen, die so sch�n schwarz waren, auch um ihre gel�ufige Sprache.</p>
    <p>Eines Tages kamen Mwato und Nganya mit Spaten heran und begannen die junge Palme mit allen Wurzeln aus der dunklen Erde zu graben. Die alte Palme hatte die Empfindung, nun k�nnte es hier still werden. Und sie wunderte sich, da� der J�ngling und das M�dchen nicht unaufh�rlich plauderten, da sie es doch konnten. Die aber gruben nur immer tiefer.</p>
    <p>Als die hei�e Mittagstunde nahte, legte Mwato zuerst den Spaten fort und Nganya folgte ihm. Er holte N�sse herbei, und sie brachte Wasser. Sie hielten eine Mahlzeit und dann begannen sie zu sprechen.</p>
    <p>Nganya: Hat der wei�e Mann dir das Geldst�ck schon gegeben, ich meine den Lohn, weil wir die junge Palme ausgraben?</p>
    <p>Mwato: Er hat es mir versprochen. Versprechen ist dasselbe, wie geben.</p>
    <p>Nganya: Kannst du mir sagen, zu welchem Zauber die wei�en M�nner die Palme brauchen?</p>
    <p>Mwato: Das kann ich dir ganz genau sagen. Sie sind Priester des Kaisers, der kein Land hat und auf dem Wasser herrscht. Ich verstehe sehr gut ihre Sprache. Es ist Englisch, was soviel hei�t wie die G�ttersprache. Der Kaiser dieses Landes hat gar kein Land. Aber dort ist es das ganze Jahr so kalt, da� das Meer so hart wird wie Stein. Darum kann er auf dem Wasser herrschen. Auf dem harten Wasser wachsen aber nur Jamwurzeln und Reis, nicht Bananen und Datteln. Der Kaiser aber wird b�se, wenn er nicht Bananen und Datteln hat, und findet er keine in der l�ngsten und k�ltesten Nacht des Jahres, so mu� der Himmel einst�rzen, und das harte Wasser wird in Tr�mmer geschlagen, und ihr oberster Gott, der kein Wasser vertragen kann, mu� ins Meer st�rzen.</p>
    <p>Nganya: Das ist Bitterwasser.</p>
    <p>Mwato: Bitterwasser kann er auch nicht vertragen. Nun kamen die wei�en Priester sorglich zu uns, um f�r ihren Kaiser Bananen und Datteln zu holen. Jetzt aber holen sie sich schon junge B�ume und wollen sie ganz und gar auf ihr gefrorenes Wasser pflanzen.</p>
    <p>Nganya warf sich auf den R�cken und strampelte vor Vergn�gen mit ihren schwarzen Beinen. Sie lachte unb�ndig. Dann sprang sie auf, umrankte mit ihrem schlanken Leib die junge Palme und rief unaufh�rlich: �Ich will dich w�rmen, du sollst nicht frieren!� &#8211; Und dann lachte sie wieder und sagte zu Mwato: �Soll ich auch dich w�rmen?�</p>
    <p>Mwato: Sie wird nicht frieren. Der oberste der wei�en Priester hat mir alles genau erz�hlt, und ich habe alles genau verstanden. In der Hauptstadt des gefrorenen Wasserlandes steht ein gro�er Tempel, und seine W�nde sind hart wie Eisen und durchsichtig wie Luft.</p>
    <p>Nganya: Du l�gst!</p>
    <p>Mwato: Ich nicht. Es ist der Tempel der Palmen. Dorthin schaffen sie Erde vom Kongo und senken die Palmen mit Wurzeln ein. Dort auch &#8211; so sang mir der erste der Priester &#8211; sch�ren des Tempels dienende Br�der ein ewiges Feuer im Palmendienst. Und die Sonne scheint durch die luftigen W�nde und verm�hlt sich drin mit dem ewigen Feuer und ruft die Palmen hinauf in die H�h.</p>
    <p>Nganya: Wei�t du noch mehr so M�rchen? Was geschieht sonst in dem Tempel der Palmen?</p>
    <p>Mwato: Des Morgens sieht man dort junge M�tter mit ihren S�uglingen und Lehrer mit den Knaben. An den Palmen lernen die Knaben lesen.</p>
    <p>Nganya: Lesen? Was ist das?</p>
    <p>Mwato (nachdenklich): Ich wei� nicht gewi�. Ich glaube so ungef�hr gefrorenes Sprechen.</p>
    <p>Nganya: Und dann?</p>
    <p>Mwato: Dann kommt in der D�mmerstunde wohl ein Lehrer und eine junge Mutter allein unter die Palmen und empfangen die Weihen f�r das Geheimnis der Liebe.</p>
    <p>Nganya: Liebe?</p>
    <p>Mwato: Na ja, das ist wieder gefrorene Freude bei ihnen; wie zum Beispiel, wenn wir beide erfroren w�ren und uns doch umarmen wollten.</p>
    <p>Nganya warf sich lachend auf Mwato und schrie: �Ich bin nicht erfroren, ich liebe dich nicht.� Dann hielt sie pl�tzlich inne und sagte: �Nein, es mu� doch sch�n sein, sich vorher dazu weihen zu lassen. Womit werden sie geweiht?�</p>
    <p>Mwato: Mit Kleidern.</p>
    <p>Nganya: Kleider? Was ist das schon wieder?</p>
    <p>Mwato: Bunte Matten. Wer dort keine solchen Kleider auf dem Kopfe tr�gt, der hei�t ein Heide und wird verbrannt.</p>
    <p>Nganya (weinend): Ich will nicht hin! Ich will mich vom Kleiderpriester nicht weihen lassen! Mir wird kalt!</p>
    <p>Und sie warf sich schluchzend mit den Augen auf Mwatos Kniee.</p>
    <p>Die alte Palme aber, die hoch hinausragte �ber den Urwald und viel gesehen hatte, wiegte sich leise und merkte, da� die wei�en und schwarzen Menschen einander nicht verstanden.</p>
    <p>Nach einer Weile fl�sterte Nganya: �Wie gut du bist!�</p>
    <p>Mwato antwortete: �Nein, du bist gut!�</p>
    <p>Die alte Palme sah ihnen in die Augen und vernahm, da� sie beide sagen wollten: �Ich bin gl�cklich.�</p>
    <p>Mwato und Nganya waren gl�cklich alle zwei beide.</p>
    <p>Aber die alte Palme wu�te jetzt, da� auch gleichfarbige Menschen einander nicht verstehen, selbst dann nicht, wenn sie sich verstehen wollen, und sie beneidete die Menschen nicht mehr um ihre arme Sprache.</p>
  </div>
  <div class="chapter" id="wahr2">
    <h3>Rosenrote Fenster</h3>
    <p>Der gute Herzog lebte mit seiner sch�nen Frau in einem gro�en Schlosse, das hatte lauter rosenrote Fensterscheiben. Darum glaubte der Herzog, die Welt sei rosenrot. Denn er kam niemals aus dem Schlosse heraus.</p>
    <p>Eines Tages las er in seiner rosenroten Zeitung, die B�rger lebten der Meinung, die Welt sei n�chtens schwarz, bei Tage aber mitunter blau und meistens grau. Da wurde er zornig und rief seinen Schatzmeister. Der mu�te ungeheure Schulh�user im ganzen Lande bauen, darin waren lauter rosenrote Fensterscheiben.</p>
    <p>Nun lernten die Schulkinder wirklich, die Welt sei rosenrot, und waren guter Dinge. Wenn sie aber herauskamen aus den Schulh�usern, so erfuhren sie zu ihrem Schrecken, da� die Welt n�chtens schwarz war, bei Tage aber mitunter blau und meistens grau. Weil sie nun die Augen an die rosenrote Farbe gew�hnt hatten und weil sie sich �ber die Fopperei �rgerten, darum erschien ihnen auch der blaue Himmel gr�ulich.</p>
    <p>Der Herzog las in seiner rosenroten Zeitung, da� die Schule die B�rger nicht gebessert h�tte. Da lie� er noch zorniger seinen Kriegsfeldherrn kommen und befahl ihm, jeden einzelnen B�rger zu binden und ihm beide Augen mit Gewalt rosenrot anzustreichen. Das tat weh, und die B�rger wurden b�se. Sie rotteten sich vor dem Schlosse zusammen und drohten die rosenroten Schlo�fenster mit grauen Steinen einzuwerfen. Da erschraken der Kriegsfeldherr und der Schlo�kaplan �ber alle Ma�en. �Alles, nur das nicht!� Es gab n�mlich eine alte Wahrsagung, da� die Kapelle einst�rzen m��te, wenn auch nur eines der rosenroten Schlo�fenster zerbrochen w�rde. Lieber sollte alles beim alten bleiben.</p>
    <p>Der gute Herzog aber wollte noch einen Versuch machen. Er berief abermals seinen Schatzmeister und sagte zu ihm: �Mein lieber Schatzmeister, �ffne alle deine Truhen; wir wollen �ber der Erde einen neuen starken Himmel aus rosenrotem Glase bauen. Dann werden die schlechten B�rger endlich zugeben m�ssen, da� die Welt rosenrot aussieht.�</p>
    <p>�Dann w�re sie es sogar, Hoheit�, sagte der ehrliche Schatzmeister. �Aber dazu langt's nicht.�</p>
    <p>Und so blieb alles beim alten.</p>
  </div>
  <div class="chapter" id="wahr3">
    <h3>Zwei Schuster</h3>
    <p>Es hat eine Zeit gegeben, wo alle Menschen glaubten, die Erde schwimme als eine betr�chtliche Scheibe auf dem Ozean. Nur �ber die Form der Scheibe ist man damals nicht einig gewesen.</p>
    <p>Zu dieser Zeit lebten in Athen zwei Schuster in einer gemeinsamen Werkstatt. Sie waren gute Schuster und arbeiteten gleichm��ig an jedem Paar Stiefel, der eine am rechten, der andere am linken Stiefel. Aber sie waren nicht einer Meinung �ber die Form der Erdscheibe. Der eine Schuster hielt sie f�r kreisrund, der andere f�r rechteckig. Unabl�ssig schlugen sie mit dem Hammer, bohrten sie mit der Ahle und schmierten sie mit Pech; aber ebenso unabl�ssig zankten sie, und der eine verschwor die H�upter seiner Kinder f�rs Quadrat, der andere f�r den Kreis. Nur die Stiefel machten sie nicht nach dem Vorbild des Quadrats oder des Kreises, die Stiefel machten sie nach der Form ihrer eigenen gro�en F��e, der eine den rechten, der andere den linken. Und sie standen oder sa�en, beim Streit oder bei der Arbeit, fest auf der schwimmenden Erdscheibe.</p>
    <p>Die Ururenkel der beiden Schuster von Athen sitzen wieder in einer gemeinsamen Werkstatt. Jetzt zanken sie �ber die Freiheit des Willens und �ber die Ungleichheit der Menschenk�pfe. Ihre Stiefel aber machen sie immer noch gleich nach ihren gro�en F��en, der eine den rechten, der andere den linken, und arbeiten willig.</p>
  </div>
  <div class="chapter" id="wahr4">
    <h3>Der Buchweizen und die Rechenmeister</h3>
    <p>Das Volk hatte nicht genug Buchweizengr�tze. Als es immer lauter nach Gr�tze schrie, bestellte die Regierung einen gelehrten Rechenmeister, der herausbringen sollte, auf welchem Boden Buchweizen am besten gedeihe. Der Meister erhielt einen Gehalt, eine Frau, drei Assistenten, ein Laboratorium und eine Bibliothek. Nach langen M�hen und Versuchen brachte er endlich heraus, da� Buchweizen am besten in einem Boden gedeihe, der aus der und der Mischung von Lehm, Sand und seinen Nitraten bestehe. Er ver�ffentlichte diese Entdeckung, und das Volk freute sich.</p>
    <p>Bald stellte es sich aber heraus, da� das Volk nicht wu�te, welcher Boden aus der und der Mischung bestehe. Da gab die Regierung einem anderen Rechenmeister einen Gehalt, eine Frau, drei Assistenten, ein Laboratorium und eine Bibliothek, und dazu den Auftrag, herauszubekommen, woran man einen Boden von der und der Mischung erkenne. Der treffliche Gelehrte studierte zuerst mit der Retorte und dem Mikroskop, dann erst entschlo� er sich, Experimente mit dem Auss�en von Buchweizen anzustellen. Sie gl�ckten. Nach langen M�hen und Versuchen brachte er es heraus, da� man einen Boden von der und der Mischung daran erkenne, da� Buchweizen darin am besten gedeihe. Er ver�ffentlichte diese Entdeckung, und das Volk freute sich.</p>
    <p>Viele Jahre sp�ter kam ein schlechter und sparsamer Mann an die Spitze der Regierung. Da beide Rechenmeister eben gestorben waren, gab der neue Minister eine Frau, drei Assistenten, ein Laboratorium und eine Bibliothek an das Nachbarvolk ab, steckte einen Gehalt in den Staatss�ckel und betraute einen dritten Gelehrten mit beiden Wissenschaften.</p>
    <p>Schleunig bekam dieser eine Gelehrte Kopfschmerzen und dachte nach, wie er die beiden Entdeckungen vereinigen k�nnte. Eines Tages, als er es vor Kopfschmerzen nicht mehr aushalten konnte, fiel es ihm ein. Wo Buchweizen am besten gedeiht, da ist der und der Boden; wo der und der Boden ist, da gedeiht Buchweizen am besten. Alle drei Assistenten sprangen von ihren Arbeitsst�hlen auf, als sie den logischen Schlu� vernahmen: Also gedeiht Buchweizen dort am besten, wo Buchweizen am besten gedeiht.</p>
    <p>Uneigenn�tzig �berlie� der treffliche Rechenmeister seine Entdeckung den drei Assistenten und seiner Witwe. Denn er h�ngte sich auf.</p>
  </div>
  <div class="chapter" id="wahr5">
    <h3>Das Gesetz</h3>
    <p>Seit Weltengedenken liebten einander der Mond und die Erde.</p>
    <p>Eines Erdenabends, es hatte zwischen den Liebenden stundenlang gewittert und sie waren wieder gut, sagte die Erde: �Willst du was Tolles h�ren, lieber Mond? Horch zu. Der Staub auf meinem gr�nen G�rtel vermag ein Ger�usch zu machen. Der Staub nennt das Sprechen und Denken.�</p>
    <p>�Ho!� machte der Mond erstaunt. �Spricht und denkt er auch was �ber uns, der Staub auf deinem lieben gr�nen G�rtel?�</p>
    <p>�Ja. Der Staub hat herausgebracht, da� du dich um mich drehst.�</p>
    <p>�Ho, das war schwer! Was wei� er noch, der kluge Staub?�</p>
    <p>�Nichts weiter. Nicht da� der �ther uns innig verbindet, nicht, da� wir unendliche K�sse tauschen, nicht, da� du dich mit deiner Kraft einw�hlst in meine wogenden Meere. Nichts. Aber er hat einen Grund gefunden f�r dein Drehen, wie er es nennt.�</p>
    <p>�Was f�r einen Grund?�</p>
    <p>�Ein Wort.�</p>
    <p>�Hoho! Wie kann ein Wort ein Grund sein? Was f�r ein Wort?�</p>
    <p>�Gesetz nennt der Staub unsere Liebe.�</p>
    <p>�Hohohoho! Was ist das, Gesetz?�</p>
    <p>�Der Staub auf meinem gr�nen G�rtel hat viele Gesetze. Das sind kleine Tafeln und auf jeder steht ein Wort: du sollst. Und wenn nun ein Staubkorn nicht kann, so kommt ein zweites, eins mit Eisenatomen, und packt es beim Kragen. Das ist das Gesetz des Staubes und so erkl�rt er sich unsere Liebe. Wir sollen! Wir!�</p>
    <p>�Hoho!� lachte der Mond und pre�te die Erde an sich und sp�lte mit etwas Flut den Staub von ihrem lieben gr�nen G�rtel.</p>
    <p>�Das war die Sintflut�, sagte der Staub.</p>
  </div>
  <div class="chapter" id="wahr6">
    <h3>Die Jury</h3>
    <p>Ein t�chtiger K�nig hatte auch Adler in seinem Reich. Lange achtete er ihrer nicht, bis eines Tages ein Abenteurer an des K�nigs Tisch den Einfall vorbrachte, man k�nnte gut gez�chtete Adler zu einer Schwadron von Brief- und Pakettr�gern drillen f�r den n�chsten Krieg. Der K�nig schenkte dem Abenteurer eine abgelegte Komtesse zur Frau und lenkte seine hohe Aufmerksamkeit sofort auf die Adlerzucht. F�r das bestgebaute Adlerweib setzte er einen Preis aus. Und Ochsen bestellte er zu Preisrichtern.</p>
    <p>Vettern einer kalbenden Kuh, die dem K�nig befreundet lebte, waren alle Preisrichter. Und allemal h�rten sie zuerst die Meinung der Kuh, bevor sie auch nur �Muh� sagten. Es waren beb�nderte Ochsen, und zierliche Gl�ckchen hingen ihnen von der Wampe hinunter.</p>
    <p>Ein junges Adlerweib, weil es t�richt war, ihren Adlermann verlassen hatte und sich in ihrem einsamen Stadtnest nicht wohl f�hlte, meldete sich zum Wettbewerb. Ein stolzes Adlergesch�pf.</p>
    <p>Um das Adlerweib standen die Preisgeschworenen herum, sieben Ochsen, und dem Neste zun�chst die einflu�reiche Kuh mit ihrem Kalbe.</p>
    <p>�Hat sie H�rner?� fragte der Ochsen�lteste und r�sselte mit seinem Maul vor den Augen des Adlerweibs.</p>
    <p>�Mein K�lbi kriegt sie bald�, sagte die Kuh. �Und nicht einmal ihr Mann hat welche.�</p>
    <p>�Muh�, machten die sieben Ochsen.</p>
    <p>�Hat sie ein goldenes Joch?� fragte der zweite Ochse und legte sich zum Wiederk�uen nieder.</p>
    <p>�Nicht einen roten Pfennig�, sagte die Kuh. �Mein K�lbi hat auch kein Geld, aber es geh�rt zur Familie.�</p>
    <p>�Muh�, machten die sieben Ochsen.</p>
    <p>�Kann sie alten Fra� wiederk�uen?� fragte der dritte Ochse.</p>
    <p>�Lebendiges schlingt sie hinunter�, rief die Kuh.</p>
    <p>�Seht nur, wie sch�n mein K�lbi schon wiederk�ut.�</p>
    <p>�Muh.�</p>
    <p>�Hat sie 'nen Stall, 'nen warmen Stall?� fragte der vierte Ochse.</p>
    <p>�Bei Mutter Gr�n, in Luft und Wind!� rief die Kuh.</p>
    <p>�Eine Hergeflogene! Aus dem Ausland! Nicht einmal eine Streu hat sie wie mein K�lbi, das hier geboren ist.�</p>
    <p>�Muh.�</p>
    <p>�War sie bei Hofe?� fragte der f�nfte Ochse und bewegte die Wampe, da� die Schellen klingelten.</p>
    <p>�Ohne Band und Schelle seht ihr sie�, rief die Kuh. �Da guckt mein K�lbi an. Seinem Vater zuliebe hat es, wie es nur vierundzwanzig Stunden alt war, schon ein Gl�ckchen gekriegt.�</p>
    <p>�Muh.�</p>
    <p>�Gibt sie Milch?� fragte der sechste Ochse.</p>
    <p>Die Kuh antwortete gar nicht. K�lbi suchte muffelnd unter dem Adlerweib und kehrte dann kl�glich zum strotzenden Euter der Mutter zur�ck.</p>
    <p>�Muh.�</p>
    <p>�Gibt sie Mist?� fragte der siebente Ochse.</p>
    <p>Der Ochsen�lteste bat das Adlerweib, sich ein wenig vom Neste zu l�ften.</p>
    <p>�Etwas Mist wenigstens mu� da sein�, meinte er wohlwollend.</p>
    <p>Da lagen zwei Eier.</p>
    <p>�Wie ekelhaft!� rief die Kuh. �Nicht einmal Mist gibt sie. Nichts als diese sch�bigen Eier. Adlereier. Riecht mal, wie sch�n mein K�lbi schon mistet.�</p>
    <p>Da machten alle Ochsen �Muh� und sprachen den ersten Preis f�r Adlerzucht dem K�lbi zu. Das wurde sp�ter ein gro�er Ochse.</p>
    <p>Damals aber war es noch jung und gut. Darum sagte es freundlich zum Adlerweib: �Mach dir nichts draus. Was h�ttest du vom Preis des K�nigs gehabt, au�er der Ehre? Der Preis ist ja ein Fuder Heu.�</p>
    <p>Da stie� das Adlerweib ab und schwang sich auf durch den wogenden �ther, zur�ck zu ihrem Adlermann.</p>
  </div>
  <div class="chapter" id="wahr7">
    <h3>Das Opfer</h3>
    <p>Es war einmal ein Maler, der hatte viel gelernt auf der Akademie: Zeichnen und Farbenreiben, Anstreichen und Sichkleiden und gro�e Pl�ne machen. Dazu hatte er noch eine wundersch�ne Geliebte.</p>
    <p>Er hie� Stieglitz und war ein schlechter Maler. Die Geliebte aber hatte starke Arme und starr emporgerichtete Augen; sie wollte ihn hoch sehen �ber seinen Genossen oder ihn hochheben �ber sie, wenn er nicht steigen konnte aus eigener Kraft.</p>
    <p>Er ging m��ig umher und sprach von einem gro�en Bilde, das sollte �das Opfer� hei�en. Morgen wollte er anfangen. Nur ein Modell suchte er f�r das �Opfer�.</p>
    <p>Jahrelang suchte er sein Modell. Endlich fragte die Geliebte: �Was suchst du denn f�r ein Modell? Was wird dieses Weib denn k�nnen, das ich nicht kann?�</p>
    <p>�Sch�n mu� sie sein wie du, und dieses Messer mu� sie sich in die Brust sto�en k�nnen, aufrecht auf dem Modelltisch, vor mir.�</p>
    <p>�Spann die Leinwand auf�, sagte die Geliebte. �Nimm die Palette und mach die Augen auf!�</p>
    <p>Ein Band nur l�ste sie und alle Gew�nder fielen von ihr ab. Nackt stand sie da, schlank und voll in ihrer Bl�te; sie stieg drei Stufen hinauf, setzte das Messer an unter der schwellenden linken Brust, blickte stolz und entschlossen empor und fragte: �Ist es so recht?�</p>
    <p>�Nicht ganz�, rief der Maler ungeduldig. �Das &#8250;Opfer&#8249; mu� ja l�cheln! L�chelnd mu� sie das Messer hineinsto�en.�</p>
    <p>Die Geliebte l�chelte und stie� sich l�chelnd das Messer tief ins Herz.</p>
    <p>Der Maler ist alt geworden und hat sein gro�es Bild nie gemalt. Er hatte doch ein gl�ckliches L�cheln verlangt! Mit einem gl�ckhaften L�cheln auf den Lippen mu�te das Opfer sterben. Die Geliebte aber hatte im Tode ganz falsch gel�chelt, bitter, sp�ttisch.</p>
  </div>
  <div class="chapter" id="wahr8">
    <h3>Zwei Bettler</h3>
    <p>Zwei Bettler sa�en an der Kirchent�r. In der Kirche donnerte der Pfarrer von der Allmacht und von der G�te Gottes, ermahnte die S�nder und mahnte zum Glauben. Die M�nner und Frauen blickten verstohlen aufeinander oder lasen in ihren B�chern oder schliefen. An dem gro�en Fenster rechts vom Hauptaltar zwitscherten die Schwalben.</p>
    <p>Bis vor die Kirchent�r hinaus h�rte man von Zeit zu Zeit die eindringlichsten Stellen der Predigt. Denn es war Sommer, die T�r stand offen, und die Bettler hatten ihre Gebreste entbl��t. Und Schwalben schossen hin und her und fingen im Fluge die kleinen M�cken. Der j�ngere Bettler hatte nur einen Stelzfu�. Dem �lteren Bettler fehlte ein Arm und seine Brust war grauenhaft zerfressen.</p>
    <p>Der j�ngere Bettler sagte: �Der Pfaff ist doch ein Feigling. Ich kann hier in der warmen Sonne sitzen und brauche immer nur: &#8250;Vergelt's Gott&#8249; zu sagen und stehe mich jahraus, jahrein ebenso hoch wie er. Daf�r mu� er aber Tag und Nacht ein Gesicht machen wie ein Leichenbitter und mu� alle segnen, die ihm was geben, mit vielen hundert Worten segnen, und auf lateinisch, die Kinder, die Brautleute und die Toten. So eine Memme, und kommt nicht h�her als ich! Pfaff, anstatt sich ein Bein abschie�en zu lassen!�</p>
    <p>Der alte Einarm schlug ein Kreuz und sagte: �L�stere nicht! Du wirst noch die ewige Seligkeit verscherzen.�</p>
    <p>�Ewige Seligkeit?...! Du nat�rlich, du h�ltst zu den Pfaffen. F�r dich hat Gott auch etwas getan. Er hat dich als Einarm auf die Welt kommen lassen und dir dazu den Aussatz gegeben. Du hast allen Grund, ihm dankbar zu sein. Aber ich? Nichts hat er mir gegeben, und ich pfeife auf den Pfaffen. Alles habe ich mir selber zu verdanken.� Und er schob den versteckten Fu� ein wenig zur�ck und schnallte das Stelzbein fester.</p>
  </div>
</body>
</html>

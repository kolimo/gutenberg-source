<?xml version="1.0" encoding="ISO-8859-1"?>
<?xml-stylesheet type="text/xsl" href="../../bin/xsl/xml2htm.xsl"?>
<html>
<head>
  <title>Alexander Sch�ppner: Bayrische Sagen / 408</title>
  <meta name="type"      content="legend"/>
  <meta name="booktitle" content="Bayrische Sagen / Erster Band"/>
  <meta name="author"    content="Alexander Sch�ppner"/>
  <meta name="year"      content="1979"/>
  <meta name="publisher" content="Verlag Lothar Borowsky"/>
  <meta name="address"   content="M�nchen"/>
  <meta name="isbn"      content="3-7917-0896-1"/>
  <meta name="title"     content="Bayrische Sagen / 408"/>
  <meta name="pages"     content="404-407"/>
  <meta name="created"   content="20010805"/>
  <meta name="sender"    content="gerd.bouillon@t-online.de"/>
  <link rel="stylesheet" href="../../css/prosa.css" type="text/css"/>
  <meta name="firstpub"  content="1852"/>
</head>
<body>
  <h3>408 &#160; Die gerettete Unschuld</h3>
  <p class="centersml">Die vor. Schrift, S. 147</p>
  <p>Der Ruf vandalischer Grausamkeit ging den Schweden schon voraus, und das Geschrei: �Die Schweden kommen�, war hinreichend, ganze Ortschaften zu entv�lkern. Alles, was gesunde F��e hatte, suchte sein Heil in der Flucht. Nur schwache Greise, kleine Kinder und kranke Personen waren die Zur�ckgebliebenen, gegen die die Schweden um so grausamer verfuhren, da sie die einzigen Opfer ihrer Rache waren.</p>
  <p>Es lebte damals in Wertingen eine Jungfrau, die, sch�n wie der Fr�hling, ihre Eltern �ber alles liebte. Mit Schrecken und b�ser Ahnung hatte sie oft von den Grausamkeiten der Schweden in anderen L�ndern geh�rt, wie Jungfrauen von ihren Eltern gerissen und schmachvoller Entehrung preisgegeben wurden, und diese Gedanken hatten in stiller Nacht oft schon ihre Augen mit Tr�nen der Furcht gef�llt.</p>
  <p>Ihre Bef�rchtungen waren leider nicht ohne Grund, denn auch in Wertingen ert�nte eines Tages der Ruf: �Die Schweden kommen.� Wie ein elektrischer Schlag wirkte dieser Ruf auf alle. Man raffte in aller Eile das Notwendigste zusammen, und der Wald wurde als vorl�ufiges Asyl gesucht. Das Schreien der Kinder, das H�nderingen der M�tter mag wohl manchem Vater das Herz durchschnitten haben.</p>
  <p>Auch unsere Jungfrau war mit dem Zusammenraffen des Notwendigsten besch�ftigt, nahm still unter Tr�nen von ihrem elterlichen Haus Abschied und schlo� sich mit ihren Habseligkeiten einem Zug an, indem sie glaubte, ihre Eltern bef�nden sich bei den vorauseilenden Haufen. Im Wald begegnete man sich gegenseitig &#8211; und wer malt den Schrecken des M�dchens, als sie nirgends ihre Eltern finden konnte. Endlich erfuhr sie, da� sie jenseits des Tals dem Wald zugeeilt seien. Der erste Schmerz war der schrecklichste; sie konnte nicht weinen.</p>
  <p>Man hatte sich bei der Ankunft der Schweden tiefer in den Wald zur�ckgezogen, und mehrere Wochen vergingen ohne Gefahr. Oft ging die Jungfrau in stiller Nacht aus ihrem Versteck hervor, dem H�gel zu und schaute so wehm�tig �ber das Tal hin�ber nach dem Wald, der ihr Teuerstes &#8211; ihre Eltern &#8211; barg. Weder der kalte Nachtzug noch die Unsicherheit der Gegend hielten sie von diesem n�chtlichen Besuch ab. Wenn sie sich dann ausgeweint hatte und dem Mond, der soeben �ber das Tal dem Wald zu zog, viele Gr��e an ihre Eltern mitgegeben hatte, eilte sie mit nassem Blick wieder dem Wald zu. Doch auch dieser Trost wurde ihr entzogen, da die Gegend immer unsicherer wurde.</p>
  <p>Das war f�r die kindliche Liebe der Jungfrau zuviel. Sie entschlo� sich, lieber zu sterben, als l�nger �ber das Schicksal ihrer Eltern ungewi� zu sein und von ihnen getrennt zu leben. Es war mondhelle Nacht, als das k�hne M�dchen aus dem Wald hervoreilte und mit scheuem Blick in der Gegend umhersp�hte, ob sie nichts entdecken k�nnte; und als ihr Auge nichts sah, ging sie, sich Gott empfehlend, fl�chtigen Schrittes wieder weiter. Es war ihr so bang ums Herz, und gern h�tte sie weinen m�gen, h�tte es die Angst ihr zugelassen. Doch f�hlte sie sich wieder gest�rkt, als sie sich vor einem Feldkreuz, das am Weg stand, niedergeworfen und recht innig gebetet hatte.</p>
  <p>Sie konnte jetzt das ganze Tal �bersehen.</p>
  <p>Viele Wachtfeuer waren um Wertingen herum angez�ndet, und um diese waren Truppen gelagert, deren wilder Gesang weit in der Gegend herum geh�rt wurde. Eine nie gef�hlte Angst bem�chtigte sich ihrer, als sie die Anh�he herunterstieg, denn jedes Gestr�uch sah sie f�r Feinde an, und an dem Rauschen ihres eigenen Kleides glaubte sie den Tritt eines Schweden zu h�ren.</p>
  <p>Gl�cklich war sie in das Tal gelangt, und feuriger schlug ihre Brust bei dem Gedanken, da� sie jetzt ihre Eltern bald wiedersehen werde. Aber pl�tzlich gebot hinter ihr eine rauhe Stimme: �Halt.� Unwillk�rlich sah sie um und erblickte einen hochst�mmigen Schweden hinter ihr, dessen blanke R�stung beim Schein des Mondes hell gl�nzte. Entkr�ftet sank sie auf den Boden, als sie sich verraten sah, und schon glaubte sie das Schwert in ihrem Herzen zu f�hlen, schon das warme Blut auf den Boden flie�en zu h�ren.</p>
  <p>Aber wie erstaunte das unschuldige M�dchen, als er sie freundlich umfa�te und so sch�n mit ihr tat, als w�re er ihr eigener Bruder. Bald h�tte sie ihn als ihren Retter begr��t, bald ihn ersucht, sie bis an jenen Wald hin zu f�hren und zu sch�tzen &#8211; da erwachte in ihr pl�tzlich der Gedanke an ihre gef�hrdete Tugend, und dieser Gedanke gab der schwachen Jungfrau wieder Kraft und Mut. Sie entwand sich schnell seinen umschlingenden Armen und flog eilends davon.</p>
  <p>Der get�uschte Schwede sch�umte vor Wut und st�rzte ihr mit gezogenem Schwert nach. �Willst du das Opfer meiner Lust nicht werden, so bist du als Opfer meiner Rache mir gewi߫, dachte der wilde Krieger in seinem Herzen.</p>
  <p>Die Angst lieh indes dem M�dchen Fl�gel, und schon war sie ihm um vieles voraus, als sie pl�tzlich am Ufer der Zusam stand und nirgends eine rettende Br�cke sah; denn in der Eile hatte sie den Weg verfehlt. Da warf sich das fromme M�dchen auf die Knie nieder, und vertrauensvoll ihre Augen zum Himmel erhebend und ihre H�nde faltend flehte sie zur Himmelsk�nigin, wenn nicht um Rettung ihres Lebens, doch um Rettung ihrer Tugend &#8211; und die himmlische Jungfrau erh�rte sie. &#8211; Ein �berirdischer Glanz verbreitete sich um sie, und von sanfter Hand f�hlte sie sich hin�bergetragen ans jenseitige Ufer der Zusam.</p>
  <p>Geblendet vom himmlischen Glanz vermochte sie erst nach einiger Zeit die Augen wieder aufzuschlagen, und erst jetzt sah sie, was mit ihr vorgegangen war. Im Gef�hl der Andacht und des w�rmsten Dankes fiel sie wieder auf die Erde und dankte inbr�nstig f�r ihre Rettung. Bald war sie nun im Wald, wo sie ihre hartgepr�ften Eltern, die sie schon als tot beweinten, wiederfand. Der schwedische Soldat wurde von seinen Kameraden am n�chsten Tag am Ufer der Zusam tot liegend gefunden.</p>
  <p>An der St.-Michaels-Kirche auf dem Friedhof zu Wertingen ist die Begebenheit in einem Bild wiedergegeben, wie ein Engel die Jungfrau �ber die Zusam hin�berf�hrt und wie der Schwede vernichtet am jenseitigen Ufer steht.</p>
  <p class="left">&#160;</p>
  <hr size="1"/>
  <p class="center"><a href="bys0407.xml"> <img alt="Vorige Seite" src="../../pic/bwd.gif" border="0" hspace="10"/> </a> <a href="bysagen.xml"> <img alt="Titelseite" src="../../pic/up.gif" border="0" hspace="10"/> </a> <a href="bys0409.xml"> <img alt="N�chste Seite" src="../../pic/fwd.gif" border="0" hspace="10"/> </a></p>
</body>
</html>

<?xml version="1.0" encoding="ISO-8859-1"?>
<?xml-stylesheet type="text/xsl" href="../../bin/xsl/xml2htm.xsl"?>
<html>
<head>
  <title>Sommernacht in Wien</title>
  <meta name="type"      content="sketch"/>
  <meta name="booktitle" content="Wiener Geschichten"/>
  <meta name="author"    content="Peter Altenberg"/>
  <meta name="year"      content="2001"/>
  <meta name="publisher" content="Sch�ffling &amp; Co. Verlagsbuchhandlung"/>
  <meta name="address"   content="Frankfurt am Main"/>
  <meta name="isbn"      content="3-89561-542-0"/>
  <meta name="title"     content="Sommernacht in Wien"/>
  <meta name="pages"     content="97-100"/>
  <meta name="created"   content="20020225"/>
  <meta name="sender"    content="gerd.bouillon@t-online.de"/>
  <meta name="firstpub"  content="1906"/>
  <link rel="stylesheet" href="../../css/prosa.css" type="text/css"/>
</head>
<body>
  <h3>Sommernacht in Wien</h3>
  <h4>(in "Pr�dromos", Berlin 1906)</h4>
  <p>Nach den M�hseligkeiten, Dem�tigungen des Gelderwerbes vermittels Blumen und Champagner im �Englischen Garten� kommen die M�dchen ins Kaffeehaus, als freie Herrinnen, zu <i>ihrem eigenen Vergn�gen</i>, gleichsam momentan in Prinzessinnen umgewandelt aus dienenden Sklavinnen. Niemand darf mehr denken �ber sie: �Zudringliche Gesch�pfe� oder es sogar aussprechen: �Bitte, bel�stigen Sie uns nicht!�</p>
  <p>Sie sind Damen geworden, die Gnaden austeilen, nach Laune und eigenem Willen. Von 3&#160;Uhr morgens an spielt Herr Karl dort auf seiner s��en sanften Geige. Wally beginnt zu tanzen und Steffi und Tertschi. Jede in ihrer Weise eine Vollkommenheit. Wally tanzt, wie eine kranke, leidenschaftliche Seele sich austanzen m�chte, um sich zu erl�sen. Oft mit Tr�nen in den Augen und Hilfe suchend. Steffi tanzt in wilder, wunderbarer, unerm�dlicher Kinder-Naturkraft. Tertschi tanzt wie die s��en Wiener M�del tanzen auf dem Relief vom Strauss-Lanner-Denkmal im Rathausparke. Wie ein Modell zu Seiferts wunderbar zarten Reliefen ist sie. Besonders der Gesichtsausdruck. So weltentr�ckt vor Tanzesfreude.</p>
  <p>Erf�llt von <i>romantischen Tr�umereien</i> und <i>Hirngespinsten</i> und <i>unerf�llbaren Sehnsuchten</i> und <i>Gutm�tigkeiten</i> sind diese M�dchen. K�nstlerische Anmut wird in ihnen frei bei den Kl�ngen des Kake-Walk und der polnischen Mazur. Man versteht es, da� sie in <i>heldenhafter Leichtsinnliebe</i> eventuell in Abgr�nde st�rzen und zerschellen, <i>klaglos und dennoch verwundert �ber ihr Schicksal</i>.</p>
  <p>Wer will sie denn je erretten, besch�tzen, betreuen?!?</p>
  <p>Wer hat Achtung und Ehrfurcht vor ihren k�nstlerischen Qualit�ten?!?</p>
  <p>Der Mann ist dumpf und stumpf und tr�ge in seiner erm�deten und ebenfalls entt�uschten Seele.</p>
  <p>Daran gehen diese M�dchen zugrunde. An dem, was die <i>schlechteren</i> M�dchen am Manne ges�ndigt haben. Er r�cht sich &#8211; an den <i>besseren</i> unter ihnen.</p>
  <p>Es sch�mt sich au�erdem heute ein jeder, begeistert zu sein, aus sich selbst f�r Augenblicke herauszutreten, einfach <i>au�er sich zu sein!</i> Jeder hat im Kampf ums Dasein irgendwo eine sch�bige W�rde zu bewahren, eine Stellung zu ber�cksichtigen! <i>Einer L�ge seine Wahrhaftigkeit zum Opfer zu bringen!</i></p>
  <p>Nur die W�rde seiner menschenfreundlichen Begeisterung achtet er nicht! Er hat nicht den Mut, in diesen M�dchen ein tiefes K�nstlertum zu <i>erlauschen</i>, zu <i>entdecken</i>. Es sind eben noch keine <i>�protokollierten Firmen�</i> �&#160;la Cleo, Otero, Cavalieri, Paquerette. F�r Blumenm�del und Champagnerm�del <i>setzt man sich noch nicht ein</i>. Die <i>verf�hrt</i> man und <i>n�tzt sie aus</i>, wirft sie dann weg wie Krebsschalen und Zitronenschalen. <i>Feiges, duckm�userisches Gesindel der M�nner!</i> Nur vor immens bezahlten <i>�Sternen�</i> habt ihr den Mut, begeistert zu sein? Weshalb? Weil ein Vari�t�-Direktor ihnen 6000&#160;Kronen monatlich bezahlt? Das, das treibt euch, <i>Hohlk�pfe</i>, zu Schulden und Verbrechen?</p>
  <p>In unbeschreiblich r�hrender Weise bieten <i>Wally, Steffi, Tertschi</i> ihre K�nste gratis dem Zuschauer dar im Caf�, 3&#160;Uhr morgens.</p>
  <p>Keine Brettl-Diva k�nnte je so wirken. Man erlebt Menschenschicksale. <i>Schweigende Not des Herzens</i> und wiederum daneben die <i>kreischende Verzweiflung</i>. Und alles ausgel�st durch Alkohol und Musik. <i>Frei geworden in der geknechteten Seele!</i></p>
  <p>In den Ruhepausen singt die s��e, sanfte Geige: �Madrigal� und �Ouvres tes yeux bleus� und �Wenn es am sch�nsten ist, dann mu� man scheiden�.</p>
  <p><i>Tertschi</i>, du hast die idealsten zartesten Beine und F��e von der Welt und die s��este wienerische Anmut!</p>
  <p><i>Wally</i>, du tanzest die Leiden der Seele und ihre Qualen!</p>
  <p><i>Steffi</i>, du bist die Tanzk�nigin an und f�r sich, in edler Bewegung jauchzend, erst <i>dabei du selbst werdend!</i></p>
  <p>Morgens zwischen 3 bis 5 Uhr spendet ihr eure edle K�nstlerschaft! Im �Englischen Garten� waret ihr Angestellte, Verk�uferinnen, Sklavinnen. Da aber seid ihr freie Herrinnen, so ohne Wunsch und Zweck. Edle, s��e, geniale T�nzerinnen! Seid bedankt und gesegnet!</p>
</body>
</html>

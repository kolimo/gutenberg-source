<?xml version="1.0" encoding="ISO-8859-1"?>
<?xml-stylesheet type="text/xsl" href="../../bin/xsl/xml2htm.xsl"?>
<html>
<head>
  <title>Landpartie</title>
  <meta name="type"      content="sketch"/>
  <meta name="booktitle" content="Sonnenuntergang im Prater"/>
  <meta name="author"    content="Peter Altenberg"/>
  <meta name="year"      content="1993"/>
  <meta name="publisher" content="Philipp Reclam jun."/>
  <meta name="address"   content="Stuttgart"/>
  <meta name="isbn"      content="3-15-008560-8"/>
  <meta name="title"     content="Landpartie"/>
  <meta name="pages"     content="10-13"/>
  <meta name="sender"    content="gerd.bouillon@t-online.de"/>
  <meta name="firstpub"  content="1896"/>
  <link rel="stylesheet" href="../../css/prosa.css" type="text/css"/>
</head>
<body>
  <h3>Landpartie</h3>
  <h4>(in "Wie ich es sehe", Berlin 1896)</h4>
  <p>Er �berreichte ihr diese goldgelben Blumen, die aussehen wie kleine bronzierte Lilien&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>�Bei mir verwelken alle Blumen&#160;&#8211;&#160;&#8211;�, sagte sie und steckte das Bouquet in das braunseidene G�rtelband.</p>
  <p>Dann stiegen sie in den Wagen und fuhren in den frischen Morgen hinein&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>Frisch war der Morgen, frisch&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>Der junge Mann sang: �Den Finken des Waldes die Nachtigall ruft&#160;&#8211;&#160;&#8211;, von Geigenstrich hallt es goldrein durch die Luft&#160;&#8211;&#160;&#8211;.�</p>
  <p>�Singen Sie nicht &#8211; &#8211; &#8211;�, sagte sie.</p>
  <p>Er schwieg &#8211; &#8211; &#8211;.</p>
  <p>�Singen Sie, wenn es Ihnen Vergn�gen macht&#160;&#8211;&#160;&#8211;�, sagte sie, �Sie haben eine h�bsche Stimme&#160;&#8211;&#160;&#8211;. Singen Sie die letzte Strophe: &#8250;auf blumiger H�h'&#160;&#8211;&#160;&#8211;&#160;&#8211;&#8249;.�</p>
  <p>Er schwieg und blickte in dieses s��e geliebte Antlitz&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>Sie l�chelte &#8211; &#8211;. Dann sah sie gleichg�ltig in die Natur. Mit der konnte man nicht spielen. Die war kalt, gelassen und l�chelte selbst&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>L�rchen mit hellgr�nem Flor standen da auf hellbraunem Boden. An sonnigen Stellen auf kurzgrasigen Wiesen standen Blumen im Herbstkleid wie grauseidene Watte und dunkelgelbe Kompositen auf graugr�nen Stengeln.</p>
  <p>Im marmorwei�en Bachger�lle standen dunkle Weidengruppen und l�ngs des Weges hellrote Berberitzen&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>Es kam ein steiles St�ck.</p>
  <p>Der Kutscher stieg ab und ging neben dem Wagen.</p>
  <p>Der junge Mann, das junge M�dchen stiegen aus&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>Sie pfl�ckte heliotropfarbigen Enzian und band ihn zu den Blumen.</p>
  <p>Er empfand das wie eine Auszeichnung. So wenig braucht man&#160;&#8211;&#160;&#8211;. Er sagte: �Wie Sie das gestern abend gesagt haben&#160;&#8211;&#160;&#8211;&#160;&#8211;: &#8250;Sie werden morgen nicht mitfahren, Sie bleiben zu Hause, Monsieur, wenn Sie so sind&#160;&#8211;&#160;&#8211;&#160;&#8211;.&#8249; Dann wandten Sie den Kopf um, weil ich zur�ckblieb, von Ihrer Seite wich. Sie l�chelten&#160;&#8211;&#160;&#8211;&#160;&#8211;. Sie l�chelten, wie wenn man sagt: &#8250;Nein, du darfst mitfahren, ich bin wieder gut, aber sei nur nicht so dumm, bist du denn ein Mann oder ein ganz kleines Baby?! Vielleicht m�chtest du sogar weinen&#160;&#8211;&#160;&#8211;&#160;&#8211;?!&#8249;�</p>
  <p>Diese Art sich auszudr�cken, die Seele plastisch hinzustellen, verstand sie gar nicht&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>Sie wurde nerv�s und sagte: �Sie, lassen Sie mich in Ruhe mit Ihren �berspannten Sachen&#160;&#8211;&#160;&#8211;&#160;&#8211;.�</p>
  <p>Dann sagte sie, ein bi�chen sch�chtern, unsicher: �Sie, Monsieur, wie hei�en diese roten Beeren&#160;&#8211;&#160;&#8211;?! Sie wissen doch alles&#160;&#8211;&#160;&#8211;&#160;&#8211;.�</p>
  <p>�Berberitzen, Weinscharl &#8211; &#8211;�, sagte er und hatte ein Gef�hl wie Blei-Schwere.</p>
  <p>Und sie: �Die sind h�bsch&#160;&#8211;&#160;&#8211;.�</p>
  <p>Das hie�: �Siehst du, ich bin gar nicht so, ich f�hre mit dir liebensw�rdige Konversation&#160;&#8211;&#160;&#8211;&#160;&#8211;!�</p>
  <p>Dann sagte sie: �Ich kann nicht mehr gehen, steigen wir ein zu den anderen&#160;&#8211;&#160;&#8211;&#160;&#8211;.�</p>
  <p>Sie gab ihm den ecruseidenen Schirm zu halten und blickte ihn an, wie wenn man sagt. �Bist du b�se&#160;&#8211;?!�</p>
  <p>Der m�de Zug verschwand aus seinem Antlitz. Er sah aus wie ein Zwanzigj�hriger, der blonde Locken sch�ttelt und jauchzt&#160;&#8211;&#160;&#8211;&#160;&#8211;. Aber er war viel �lter, und es ging vor�ber&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>Tannen in Trauer, L�rchen mit gr�nem Flor, L�rchen mit gr�nem Flor, Tannen in Trauer, L�rchen, Tannen, Tannen, L�rchen&#160;&#8211;&#160;&#8211;.</p>
  <p>Der junge Mann summte das Cello-Motiv aus Manon. Dann sang er es sanft wie der Cellist in der Hofoper&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>Auf sumpfigen, patschigen, leuchtenden Wiesen standen wei�e Sternblumen und gelbe Dotterblumen&#160;&#8211;&#160;&#8211;.</p>
  <p>Wiesen, Wiesen &#8211; &#8211; &#8211;. Irgendwo begann ein Zaun und grenzte Sumpf ab&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>Pl�tzlich lag der See da, milchblau, mare austriacum&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>Man stieg aus. Man badete im See und dinierte auf der Terrasse&#160;&#8211;&#160;&#8211;&#160;&#8211;. Sp�t abends war die R�ckfahrt. Alle nahmen Plaids.</p>
  <p>Der junge Mann sa� ihr gegen�ber&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>Sie hatte nicht mehr den triumphierenden Lach-Blick. Sie war m�de&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>Die Wagenlaternen beleuchteten hellbraune kerzengerade St�mme und gelblichgr�ne verwaschene Teppiche&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>Wie wenn man die Natur aus dem Schlafe weckte mit einem grellen Lichte&#160;&#8211;&#160;&#8211;&#160;&#8211;. Sie hat nicht mehr den triumphierenden Lach-Blick&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>Der Wagen fuhr langsam, vorsichtig, durch den dunklen kalten Wald&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>Da dachte der Herr an die Stunden, in welchen die Dame sich mit ihm spielte wie mit einem P�ppchen, H�ndchen und quasi in die H�ndchen klatschte und jauchzte �ber ihre riesigen Ungezogenheiten&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>Es war wie eine Sehnsucht in ihm nach diesem goldenen Zeitalter&#160;&#8211;&#160;&#8211;. Es war die Jugend, das leichte launige Gl�ck&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>Aber der Wagen fuhr langsam durch den kalten Wald, und sie hatte den triumphierenden Blick verloren und war m�de&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>�Singen Sie das Cello-Motiv aus Manon&#160;&#8211;&#160;&#8211;&#160;&#8211;�, sagte sie sanft. Er schwieg.</p>
  <p>Aber sie f�hlte, da� er es innerlich sang, mit lauter, s��er Stimme, wie wenn die erste Begegnung im Gasthofe darin l�ge zwischen Des&#160;Grieux und Manon und alles andere und der Tod auf fremder Erde, wo er sie begrub&#160;&#8211;&#160;&#8211;&#160;&#8211;.</p>
  <p>Der Wagen fuhr langsam durch den kalten dunklen Wald&#160;&#8211;.</p>
</body>
</html>

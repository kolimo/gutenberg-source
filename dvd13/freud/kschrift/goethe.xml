<?xml version="1.0" encoding="ISO-8859-1"?>
<?xml-stylesheet type="text/xsl" href="../../bin/xsl/xml2htm.xsl"?>
<html>
<head>
  <title>Goethe-Preis (1930)</title>
  <meta name="type"      content="tractate"/>
  <meta name="booktitle" content="Studienausgabe Band X"/>
  <meta name="author"    content="Sigmund Freud"/>
  <meta name="year"      content="1969"/>
  <meta name="firstpub"  content="1930"/>
  <meta name="publisher" content="S. Fischer Verlag"/>
  <meta name="address"   content="Frankfurt am Main"/>
  <meta name="isbn"      content="3-10-822730-0"/>
  <meta name="title"     content="Goethe-Preis"/>
  <meta name="pages"     content="6"/>
  <meta name="created"   content="20081130"/>
  <meta name="sender"    content="gerd.bouillon@t-online.de"/>
  <link href="../../css/prosa.css" type="text/css" rel="stylesheet"/>
</head>
<body>
  <div class="titlepage">
    <h3 class="author">Sigmund Freud</h3>
    <h2 class="title">Goethe-Preis</h2>
    <h4>(1930)</h4>
    <hr class="short"/>
    <p class="center">Studienausgabe Band X</p>
    <p class="center">S. Fischer Verlag<br/>
      1969</p>
  </div>
  <hr class="short"/>
  <div class="chapter" id="chap001">
    <h3><a class="pageref" name="page291">291</a><span class="upper"> Brief an Dr. Alfons Paquet</span></h3>
    <p>Ich bin durch �ffentliche Ehrungen nicht verw�hnt worden und habe mich darum so eingerichtet, da� ich solche entbehren konnte. Ich mag aber nicht bestreiten, da� mich die Verleihung des Goethe-Preises der Stadt Frankfurt sehr erfreut hat. Es ist etwas an ihm, was die Phantasie besonders erw�rmt und eine seiner Bestimmungen r�umt die Dem�tigung weg, die sonst durch solche Auszeichnungen mitbedingt wird.</p>
    <p>F�r Ihren Brief habe ich Ihnen besonderen Dank zu sagen, er hat mich ergriffen und verwundert. Von der liebensw�rdigen Vertiefung in den Charakter meiner Arbeit abzusehen, habe ich doch nie zuvor die geheimen pers�nlichen Absichten derselben mit solcher Klarheit erkannt gefunden wie von Ihnen und h�tte Sie gern gefragt, woher Sie es wissen.</p>
    <p>Leider erfahre ich aus Ihrem Brief an meine Tochter, da� ich Sie in n�chster Zeit nicht sehen soll, und Aufschub ist in meinen Lebenszeiten immerhin bedenklich. Nat�rlich bin ich gern bereit, den von Ihnen angek�ndigten Herrn (Dr. Michel) zu empfangen.</p>
    <p>Zur Feier nach Frankfurt kann ich leider nicht kommen, ich bin zu gebrechlich f�r diese Unternehmung. Die Festgesellschaft wird nichts dadurch verlieren, meine Tochter Anna ist gewi� angenehmer anzusehen und anzuh�ren als ich. Sie soll einige S�tze vorlesen, die Goethes Beziehungen zur Psychoanalyse behandeln und die Analytiker selbst gegen den Vorwurf in Schutz nehmen, da� sie durch analytische Versuche an ihm die dem Gro�en schuldige Ehrfurcht verletzt haben. Ich hoffe, da� es angeht, das mir gestellte Thema: �Die inneren Beziehungen des Menschen und Forschers zu Goethe� in solcher Weise umzubeugen, oder Sie w�rden noch so liebensw�rdig sein, mir davon abzuraten.</p>
  </div>
  <div class="chapter" id="chap002">
    <h3><a class="pageref" name="page292">292</a><span class="upper"> Ansprache im Frankfurter Goethe-Haus</span></h3>
    <p>Meine Lebensarbeit war auf ein einziges Ziel eingestellt. Ich beobachtete die feineren St�rungen der seelischen Leistung bei Gesunden und Kranken und wollte aus solchen Anzeichen erschlie�en &#8211; oder, wenn Sie es lieber h�ren: erraten&#160;&#8211;, wie der Apparat gebaut ist, der diesen Leistungen dient, und welche Kr�fte in ihm zusammen- und gegeneinanderwirken. Was wir, ich, meine Freunde und Mitarbeiter, auf diesem Wege lernen konnten, erschien uns bedeutsam f�r den Aufbau einer Seelenkunde, die normale wie pathologische Vorg�nge als Teile des n�mlichen nat�rlichen Geschehens verstehen l��t.</p>
    <p>Von solcher Einengung ruft mich Ihre mich �berraschende Auszeichnung zur�ck. Indem sie die Gestalt des gro�en Universellen heraufbeschw�rt, der in diesem Hause geboren wurde, in diesen R�umen seine Kindheit erlebte, mahnt sie, sich gleichsam vor ihm zu rechtfertigen, wirft sie die Frage auf, wie <i>er</i> sich verhalten h�tte, wenn sein f�r jede Neuerung der Wissenschaft aufmerksamer Blick auch auf die Psychoanalyse gefallen w�re.</p>
    <p>An Vielseitigkeit kommt Goethe ja Leonardo da Vinci, dem Meister der Renaissance, nahe, der K�nstler und Forscher war wie er. Aber Menschenbilder k�nnen sich nie wiederholen, es fehlt auch nicht an tiefgehenden Unterschieden zwischen den beiden Gro�en. In Leonardos Natur vertrug sich der Forscher nicht mit dem K�nstler, er st�rte ihn und erdr�ckte ihn vielleicht am Ende. In Goethes Leben fanden beide Pers�nlichkeiten Raum nebeneinander, sie l�sten einander zeitweise in der Vorherrschaft ab. Es liegt nahe, die St�rung bei Leonardo mit jener Entwicklungshemmung zusammenzubringen, die alles Erotische und damit die Psychologie seinem Interesse entr�ckte. In diesem Punkt durfte Goethes Wesen sich freier entfalten.</p>
    <p>Ich denke, Goethe h�tte nicht, wie so viele unserer Zeitgenossen, die Psychoanalyse unfreundlichen Sinnes abgelehnt. Er war ihr selbst in manchen St�cken nahegekommen, hatte in eigener Einsicht vieles erkannt, was wir seither best�tigen konnten, und manche Auffassungen, die uns Kritik und Spott eingetragen haben, werden von ihm wie selbstverst�ndlich vertreten. So war ihm z.&#160;B. die unvergleichliche St�rke der <a class="pageref" name="page293">293</a> ersten affektiven Bindungen des Menschenkindes vertraut. Er feierte sie in der Zueignung der <i>Faust-Dichtung</i> in Worten, die wir f�r jede unserer Analysen wiederholen k�nnten:</p>
    <p class="vers">�Ihr naht euch wieder, schwankende Gestalten,<br/>
      Die fr�h sich einst dem tr�ben Blick gezeigt.<br/>
      Versuch' ich wohl, euch diesmal festzuhalten?�<br/>
      &#8212; &#160; &#8212; &#160; &#8212; &#160; &#8212; &#160; &#8212; &#160; &#8212; &#160; &#8212;<br/>
      �Gleich einer alten, halbverklungnen Sage<br/>
      Kommt erste Lieb' und Freundschaft mit herauf.�</p>
    <p>Von der st�rksten Liebesanziehung, die er als reifer Mann erfuhr, gab er sich Rechenschaft, indem er der Geliebten zurief: �Ach, du warst in abgelebten Zeiten meine Schwester oder meine Frau.�</p>
    <p>Er stellte somit nicht in Abrede, da� diese unverg�nglichen ersten Neigungen Personen des eigenen Familienkreises zum Objekt nehmen.</p>
    <p>Den Inhalt des Traumlebens umschreibt Goethe mit den so stimmungsvollen Worten:</p>
    <p class="vers">�Was von Menschen nicht gewu�t<br/>
      Oder nicht bedacht,<br/>
      Durch das Labyrinth der Brust<br/>
      Wandelt in der Nacht.�</p>
    <p>Hinter diesem Zauber erkennen wir die altehrw�rdige, unbestreitbar richtige Aussage des Aristoteles, das Tr�umen sei die Fortsetzung unserer Seelent�tigkeit in den Schlafzustand, vereint mit der Anerkennung des Unbewu�ten, die erst die Psychoanalyse hinzugef�gt hat. Nur das R�tsel der Traumentstellung findet dabei keine Aufl�sung.</p>
    <p>In seiner vielleicht erhabensten Dichtung, der <i>Iphigenie</i>, zeigt uns Goethe ein ergreifendes Beispiel einer Ents�hnung, einer Befreiung der leidenden Seele von dem Druck der Schuld, und er l��t diese Katharsis sich vollziehen durch einen leidenschaftlichen Gef�hlsausbruch unter dem wohlt�tigen Einflu� einer liebevollen Teilnahme. Ja, er hat sich selbst wiederholt in psychischer Hilfeleistung versucht, so an jenem Ungl�cklichen, der in den Briefen Kraft genannt wird, an dem Professor Plessing, von dem er in der <i>Campagne in Frankreich</i> erz�hlt, und das Verfahren, das er anwendete, geht �ber das Vorgehen der <a class="pageref" name="page294">294</a> katholischen Beichte hinaus und ber�hrt sich in merkw�rdigen Einzelheiten mit der Technik unserer Psychoanalyse. Ein von Goethe als scherzhaft bezeichnetes Beispiel einer psychotherapeutischen Beeinflussung m�chte ich hier ausf�hrlich mitteilen, weil es vielleicht weniger bekannt und doch sehr charakteristisch ist. Aus einem Brief an Frau v.&#160;Stein (Nr.&#160;1444 vom 5.&#160;September 1785):</p>
    <p>�Gestern Abend habe ich ein psychologisches Kunstst�ck gemacht. Die Herder war immer noch auf das Hypochondrischste gespannt �ber alles, was ihr im Carlsbad unangenehmes begegnet war. Besonders von ihrer Hausgenossin. Ich lie� mir alles erz�hlen und beichten, fremde Untaten und eigene Fehler mit den kleinsten Umst�nden und Folgen, und zuletzt absolvirte ich sie und machte ihr scherzhaft unter dieser Formel begreiflich, da� diese Dinge nun abgethan und in die Tiefe des Meeres geworfen seyen. Sie ward selbst lustig dar�ber und ist w�rklich kurirt.�</p>
    <p>Den Eros hat Goethe immer hochgehalten, seine Macht nie zu verkleinern versucht, ist seinen primitiven oder selbst mutwilligen �u�erungen nicht minder achtungsvoll gefolgt wie seinen hochsublimierten und hat, wie mir scheint, seine Wesenseinheit durch alle seine Erscheinungsformen nicht weniger entschieden vertreten als vor Zeiten Plato. Ja, vielleicht ist es mehr als zuf�lliges Zusammentreffen, wenn er in den <i>Wahlverwandtschaften</i> eine Idee aus dem Vorstellungskreis der Chemie auf das Liebesleben anwendete, eine Beziehung, von der der Name selbst der Psychoanalyse zeugt.</p>
    <p>Ich bin auf den Vorwurf vorbereitet, wir Analytiker h�tten das Recht verwirkt, uns unter die Patronanz Goethes zu stellen, weil wir die ihm schuldige Ehrfurcht verletzt haben, indem wir die Analyse auf ihn selbst anzuwenden versuchten, den gro�en Mann zum Objekt der analytischen Forschung erniedrigten. Ich aber bestreite zun�chst, da� dies eine Erniedrigung beabsichtigt oder bedeutet.</p>
    <p>Wir alle, die wir Goethe verehren, lassen uns doch ohne viel Str�uben die Bem�hungen der Biographen gefallen, die sein Leben aus den vorhandenen Berichten und Aufzeichnungen wiederherstellen wollen. Was aber sollen uns diese Biographien leisten? Auch die beste und vollst�ndigste k�nnte die beiden Fragen nicht beantworten, die allein wissenswert scheinen.</p>
    <p>Sie w�rde das R�tsel der wunderbaren Begabung nicht aufkl�ren, die <a class="pageref" name="page295">295</a> den K�nstler macht, und sie k�nnte uns nicht helfen, den Wert und die Wirkung seiner Werke besser zu erfassen. Und doch ist es unzweifelhaft, da� eine solche Biographie ein starkes Bed�rfnis bei uns befriedigt. Wir versp�ren dies so deutlich, wenn die Ungunst der historischen �berlieferung diesem Bed�rfnis die Befriedigung versagt hat, z.&#160;B. im Falle Shakespeares. Es ist uns allen unleugbar peinlich, da� wir noch immer nicht wissen, wer die Kom�dien, Trauerspiele und Sonette Shakespeares verfa�t hat, ob wirklich der ungelehrte Sohn des Stratforder Kleinb�rgers, der in London eine bescheidene Stellung als Schauspieler erreicht, oder doch eher der hochgeborene und feingebildete, leidenschaftlich unordentliche, einigerma�en deklassierte Aristokrat Edward de Vere, siebzehnter Earl of Oxford, erblicher Lord Great Chamberlain von England. Wie rechtfertigt sich aber ein solches Bed�rfnis, von den Lebensumst�nden eines Mannes Kunde zu erhalten, wenn dessen Werke f�r uns so bedeutungsvoll geworden sind? Man sagt allgemein, es sei das Verlangen, uns einen solchen Mann auch menschlich n�herzubringen. Lassen wir das gelten; es ist also das Bed�rfnis, affektive Beziehungen zu solchen Menschen zu gewinnen, sie den V�tern, Lehrern, Vorbildern anzureihen, die wir gekannt oder deren Einflu� wir bereits erfahren haben, unter der Erwartung, da� ihre Pers�nlichkeiten ebenso gro�artig und bewundernswert sein werden wie die Werke, die wir von ihnen besitzen.</p>
    <p>Immerhin wollen wir zugestehen, da� noch ein anderes Motiv im Spiele ist. Die Rechtfertigung des Biographen enth�lt auch ein Bekenntnis. Nicht herabsetzen zwar will der Biograph den Heros, sondern ihn uns n�herbringen. Aber das hei�t doch die Distanz, die uns von ihm trennt, verringern, wirkt doch in der Richtung einer Erniedrigung. Und es ist unvermeidlich, wenn wir vom Leben eines Gro�en mehr erfahren, werden wir auch von Gelegenheiten h�ren, in denen er es wirklich nicht <a class="pageref" name="page296">296</a> besser gemacht hat als wir, uns menschlich wirklich nahegekommen ist. Dennoch meine ich, wir erkl�ren die Bem�hungen der Biographik f�r legitim. Unsere Einstellung zu V�tern und Lehrern ist nun einmal eine ambivalente, denn unsere Verehrung f�r sie deckt regelm��ig eine Komponente von feindseliger Auflehnung. Das ist ein psychologisches Verh�ngnis, l��t sich ohne gewaltsame Unterdr�ckung der Wahrheit nicht �ndern und mu� sich auf unser Verh�ltnis zu den gro�en M�nnern, deren Lebensgeschichte wir erforschen wollen, fortsetzen.</p>
    <p>Wenn die Psychoanalyse sich in den Dienst der Biographik begibt, hat sie nat�rlich das Recht, nicht h�rter behandelt zu werden als diese selbst. Die Psychoanalyse kann manche Aufschl�sse bringen, die auf anderen Wegen nicht zu erhalten sind, und so neue Zusammenh�nge aufzeigen in dem Webermeisterst�ck, das sich zwischen den Triebanlagen, den Erlebnissen und den Werken eines K�nstlers ausbreitet. Da es eine der haupts�chlichsten Funktionen unseres Denkens ist, den Stoff der Au�enwelt psychisch zu bew�ltigen, meine ich, man m��te der Psychoanalyse danken, wenn sie auf den gro�en Mann angewendet zum Verst�ndnis seiner gro�en Leistung beitr�gt. Aber ich gestehe, im Falle von Goethe haben wir es noch nicht weit gebracht. Das r�hrt daher, da� Goethe nicht nur als Dichter ein gro�er Bekenner war, sondern auch trotz der F�lle autobiographischer Aufzeichnungen ein sorgsamer Verh�ller. Wir k�nnen nicht umhin, hier der Worte Mephistos zu gedenken:</p>
    <p class="vers">�Das Beste, was du wissen kannst,<br/>
      Darfst du den Buben doch nicht sagen.�</p>
  </div>
  <hr class="short"/>
  <div class="chapter" id="chap003">
    <h3><a class="pageref" name="page301">301</a> Bibliographie der Schriften Freuds</h3>
    <blockquote>
      <p><i>G. W.</i> = S. Freud, <i>Gesammelte Werke</i> (18 B�nde und ein unnumerierter Nachtragsband), B�nde 1&#8211;17 London, 1940&#8211;52, Band 18 Frankfurt am Main, 1968, Nachtragsband Frankfurt am Main, 1987. Die ganze Edition seit 1960 bei S.&#160;Fischer Verlag, Frankfurt am Main.</p>
      <p><i>Studienausgabe</i> = S. Freud, <i>Studienausgabe</i> (10 B�nde und ein unnumerierter Erg�nzungsband), S.&#160;Fischer Verlag, Frankfurt am Main, 1969&#8211;75.</p>
    </blockquote>
    <p>Die in runde Klammern gesetzten Zahlen am Ende bibliographischer Eintragungen geben die Seite(n) des vorliegenden Textes an, wo auf das betreffende Werk hingewiesen wird.</p>
    <dl>
      <dt>(1900 <i>a</i>)</dt>
      <dd><i>Die Traumdeutung</i>, Wien. <i>G.&#160;W.</i>, Bd.&#160;2&#8211;3; <i>Studienausgabe</i>, Bd.&#160;2. (295, 296)</dd>
      <dt>(1910 <i>c</i>)</dt>
      <dd><i>Eine Kindheitserinnerung des Leonardo da Vinci</i>, Wien. <i>G.&#160;W.</i>, Bd.&#160;8, S.&#160;128; <i>Studienausgabe</i>, Bd.&#160;10, S.&#160;87. (296)</dd>
      <dt>(1925 <i>d</i>)</dt>
      <dd><i>Selbstdarstellung</i>, Wien. <i>G.&#160;W.</i>, Bd.&#160;14, S.&#160;33. (295)</dd>
      <dt>(1940 <i>a</i>)</dt>
      <dd><i>Abri� der Psychoanalyse</i>, <i>G.&#160;W.</i>, Bd.&#160;17, S.&#160;63; das Vorwort in: <i>G.&#160;W.</i>, Nachtr., S.&#160;749; Kap.&#160;VI in: <i>Studienausgabe</i>, Erg�nzungsband, S.&#160;407. (295)</dd>
      <dt>(1960 <i>a</i>)</dt>
      <dd><i>Briefe 1873&#8211;1939</i> (hrsg. von E. und L.Freud), Frankfurt am Main. (2.&#160;erweiterte Aufl., 1968.) (295)</dd>
    </dl>
  </div>
</body>
</html>

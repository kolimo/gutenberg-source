<?xml version="1.0" encoding="iso-8859-1"?>
<?xml-stylesheet type="text/xsl" href="../../bin/xsl/xml2htm.xsl"?>
<html>
  <head>
    <title>Die falschen Weihnachtsb�ume</title>
    <link rel="stylesheet" type="text/css" href="../../css/prosa.css" />
    <meta name="type" content="novelette" />
    <meta name="author" content="Charlotte Niese" />
    <meta name="booktitle" content="Meisterwerke neuerer Novellistik - F�nfter Band" />
    <meta name="title" content="Die falschen Weihnachtsb�ume" />
    <meta name="publisher" content="Max Hesses Verlag" />
    <meta name="series" content="Meisterwerke neuerer Novellistik" />
    <meta name="volume" content="F�nfter Band" />
    <meta name="corrector" content="reuters@abc.de" />
    <meta name="sender" content="www.gaga.net" />
    <meta name="created" content="20081022" />
    <meta name="projectid" content="13aa15d5" />
  </head>
  <body>
    <h3 class="author">Charlotte Niese</h3>
    <h2 class="title">Die falschen Weihnachtsb�ume</h2>
    <p>Auf unsrer Insel gab es wenig B�ume. So wenig, da� das Brennholz weither �ber das Wasser geholt werden mu�te, und da� viele der Inselbewohner niemals einen Wald gesehen hatten. Auch die Tannenb�ume waren ein seltner Artikel, was uns als Kinder immer sehr aufregte. Denn wenn es gegen die Weihnachtszeit ging, tauchten immer wieder die Zweifel auf, ob wir wohl einen wirklichen oder einen falschen Tannenbaum am heiligen Abend bek�men. Einen wirklichen Tannenbaum, der im Walde gewachsen war, und in dessen Zweigen die V�gel gesungen hatten, oder einen falschen, der in der Werkstatt des Meister Ahrens das Licht der Welt erblickt hatte.</p>
    <p>Meister Ahrens war unser Tischler. Er sah alt aus und hatte einen sehr kahlen Kopf, aber wir hatten ihn gern, besonders wenn er nicht immer von seinem guten Herzen sprach. Das langweilte uns, weil wir es eigentlich f�r selbstverst�ndlich hielten, da� man ein gutes Herz haben m�sse.</p>
    <p>Ahrens kam oft zu uns. In unsrer Kinderstube ging aller Augenblicke etwas auseinander, was eigentlich zusammengeh�rte, und Meister Ahrens erschien dann mit seinem Leimtopf, sagte, er h�tte ein gutes Herz, und klebte alles wieder zusammen. Wir halfen ihm nat�rlich und dr�ngten uns um die Ehre, in seinem klebrigen Topf dreimal herumr�hren zu d�rfen; aber seine Tannenb�ume 
    <a name="page450" title="gary/mivo" id="page450"></a> konnten wir nicht leiden. Das kam wahrscheinlich daher, weil wir sie schon so lange vorher sahen. Schon im Fr�hjahr arbeitete Ahrens an langen Wei�en St�cken, in die er L�cher bohrte; im August und September malte er diese St�cke mit grasgr�ner �lfarbe an und trocknete sie vor seiner Haust�r. Sp�ter sahen wir sie zusammengebunden in seiner Werkstatt liegen, bis der Dezember ins Land zog. Dann verschaffte er sich Tannenzweige, steckte diese in die L�cher der gr�nen St�cke und betrieb einen schwunghaften Handel mit Tannenb�umen. Auch uns bot er immer von seinem Fabrikat an, aber obgleich wir nicht leugnen konnten, da� seine B�ume schlie�lich sehr nett aussahen, so verhielten wir uns meist ablehnend. �Sie sind so billig,� sagte Ahrens eines Tages zu uns, als wir ihn einer Bestellung wegen in seiner Werkstatt besuchten, und er gerade einen gr�nen Stock etwas nachmalte.</p>
    <p>�Wir wollen sie doch nicht!� erwiderte mein Bruder J�rgen, der in seinen Ausspr�chen oft sehr bestimmt war. �Ich mag keinen falschen Tannenbaum!�</p>
    <p>�Falsch! Du lieber Gott, wasn Wort!� Ahrens sah beleidigt aus. �Da is nich die geringste Falschheit bei! Meine Tannenb�umens sind feiner als die nat�rlichen, kann ich dich sagen, mein Junge! An die nat�rlichen is oft Smutz und Erde, und bei mich is blo� die reine �lfarbe!�</p>
    <p>�Wo bekommst du eigentlich die Tannenzweige her?� fragten wir.</p>
    <p>Der alte Tischler machte ein wichtiges Gesicht. �Aus 'n Wald, aus 'n richtigen Tannwald, wo die V�gelns singen, und wo soviel B�umens stehn, da� man mannichmal keine Luft kriegen kann!�</p>
    <p>�Wo liegt der Wald, und wer holt dir die Tannenzweige?�</p>
    <p>
    <a name="page451" title="gary/mivo" id="page451"></a> Wir waren dem Tischler doch n�her ger�ckt und sahen ihn gespannt an. Aber er zuckte die Achseln. �Ja, das m�cht ihr wohl wissen! Das sag ich abersten nich &#8211; nee, das sag ich nich!�</p>
    <p>Auf diese Art umgab Meister Ahrens seine B�ume mit dem Nimbus des Geheimnisvollen, und dadurch gewannen sie nat�rlich in unsern Augen.</p>
    <p>Es war schon ziemlich nahe vor Weihnachten, und wir sprachen eigentlich von nichts anderm als von dem bevorstehenden Feste. Endlos lange Wunschzettel waren geschrieben: hin und wieder wurde eine Tr�ne �ber eine v�llig mi�gl�ckte Weihnachtsarbeit vergossen, oder wir schmiedeten Pl�ne, was wir noch verschenken wollten. Manchmal ging die Zeit entsetzlich langsam und manchmal unheimlich schnell dahin, und unsre Lehrer beklagten sich �ber unsre Zerstreutheit.</p>
    <p>Es war an einem Morgen im Dezember, da� ich zu Meister Ahrens geschickt wurde, um ihn samt seinem Leimtopfe zu uns einzuladen. Unsre Kinderstubeneinrichtung hatte durch eine l�ngere lebhafte Unterhaltung der �ltern Br�der stark gelitten, und Ahrens sollte gleich kommen. Vergn�gt polterte ich die enge Treppe zu seiner Werkstatt hinauf, konnte aber nicht bis auf die letzte Stufe kommen, weil dort ein Kind stand, auf das der alte Tischler eifrig einsprach.</p>
    <p>�Ich mu� die Zweigens haben, und Vater mu� her�ber und sie holen!�</p>
    <p>�Vater is bang!� lautete die sch�chterne Erwiderung.</p>
    <p>�I, was sollt Vater woll bang sein; er mu� los &#8211; sonsten klag ich ihm ein, wo er mich doch Geld schuldig is! Ohne die Zweigens kann ich ja nix machen, und das Gesch�ft mit die B�umens mu� anfangen! Nu geh du man, und la� Vater man auch gehn!�</p>
    <p>
    <a name="page452" title="gary/mivo" id="page452"></a> Das Kind, es war ein ziemlich gro�es M�dchen, glitt an mir vor�ber, und ich konnte jetzt in die Werkstatt treten und meine Bestellung ausrichten. Aber Meister Ahrens h�rte kaum auf mich. Er war sehr schlechter Laune und betrachtete seufzend seinen Haufen gr�ner St�cke, der friedlich in einer Ecke lag.</p>
    <p>�Kannst du keine Zweige aus dem gro�en Walde kriegen?� fragte ich neugierig. Er aber sah mich streng an.</p>
    <p>�Frag nich so dumm! Ich kann allens, was ich will, und meine Tannenb�umens sind besser als die nat�rlichen!�</p>
    <p>Als ich wieder hinauskam, da sa� dasselbe M�dchen, das vorhin mit Ahrens gesprochen hatte, auf der T�rschwelle. Sie weinte nicht, aber sie sah aus, als ob sie wohl Lust dazu h�tte, und ich setzte mich neben sie und betrachtete sie schweigend. Sie war sehr �rmlich, aber ziemlich sauber gekleidet, nur ihr dickes, blondes Haar hing unordentlich um ihren Kopf. An diesem Haar erkannte ich sie, und ich nickte ihr freundlich zu.</p>
    <p>�Du hast mir neulich mein Lesebuch nachgebracht, als ich aus der Stunde kam, wei�t du noch? Ich hatte es auf dem Wege verloren!�</p>
    <p>Sie sah jetzt auf, und ihre Augen blickten weniger tr�be.</p>
    <p>�Das war so'n feines Buch,� sagte sie, �mit Bildern ein &#8211; so'n feines Buch!�</p>
    <p>�Hast du kein Lesebuch?� erkundigte ich mich, w�hrend ich mit einiger Besch�mung daran dachte, da� ich dieses Buch schon zweimal hinter den Schrank geworfen hatte, nur um es nie wieder zu sehen. Leider war es immer wiedergefunden worden.</p>
    <p>Sie sch�ttelte den Kopf. �Nee &#8211; ich hab nix, gar nix!�</p>
    <p>�Was w�nschst du dir denn zu Weihnachten?�</p>
    <p>�Ich?� Das M�dchen sah �berrascht aus. Dann lachte sie. �Was sollt ich mich woll w�nschen; ich krieg doch nix!�</p>
    <p>
    <a name="page453" title="rofe/mivo" id="page453"></a> �Du bekommst gar nichts?�</p>
    <p>Unwillk�rlich r�ckte ich der Sprecherin n�her. �Bist du dann zu Weihnachten nicht furchtbar traurig?�</p>
    <p>�Nee� &#8211; sie lachte wieder. �Was sollt ich woll traurig sein, wo ich den ganzen Abend rumlauf und in all die Fensters guck und all die Weihnachtsb�umens zu sehen krieg! Mannichmal krieg ich auch noch ein St�ck Brot mit Rosinens geschenkt!�</p>
    <p>�Weihnachtsabend darf man eigentlich nicht ausgehn!� sagte ich. �Da mu� man zu Hause bei seinen Eltern bleiben!�</p>
    <p>�Ja, wenn Vater man nich sitzt, denn bleib ich auch bei ihm; abers er is nu ja �mmerlos im Loch &#8211; da sitz ich ja ganz allein, wo Mutter doch tot is &#8211;�</p>
    <p>�Er sitzt im Gef�ngnis?�</p>
    <p>Wenn es angegangen w�re, h�tte ich mich noch n�her an meine neue Bekanntschaft gedr�ckt. Wir sa�en aber schon ganz nahe aneinander geschmiegt. Aber um ihr doch zu zeigen, wie interessant sie mir sei, griff ich in die Tasche, in der ich einige getrocknete Pflaumen hatte, und bot sie ihr an. D�rthe Krieger, so hie� das M�dchen, nahm sie auch und verzehrte sie mit einiger Gier, w�hrend ich ihr zusah. Ich hatte mir n�mlich gerade aus dem vorhin erw�hnten Lesebuch eine wunderh�bsche Geschichte von einem unschuldig Gefangnen vorlesen lassen und nahm jetzt an, da� die Gef�ngnisse nur dazu da w�ren, Unschuldige zu qu�len.</p>
    <p>�Dein Vater hat doch nat�rlich nichts B�ses getan?� fragte ich.</p>
    <p>D�rthe sch�ttelte den Kopf. �Nee &#8211; nat�rlich nich! Blo� ein b�schen Stehlen. Weiter gar nix. Der B�rmeister is auch zu eigen. Abers nach die Tannenzweigen in Holstein will er doch nich hin!�</p>
    <p>
    <a name="page454" title="rofe/mivo" id="page454"></a> �Stiehlt er die auch?�</p>
    <p>�Ja, wo sollt er sonstens zu sie kommen? Sie sitzen an ein Baum, und der Baum geh�rt ein Grafen zu, der furchtbar slecht is und nich leiden kann, wenn man in sein Wald spazieren geht. Vater sagt, der Wald is so gro�, und da laufen Rehe und Hasen herum &#8211; da merkt kein ein, wenn ein Baum fehlt und wenn da ein Reh weniger is. Hast mal Rehbraten gegessen? Der smeckt abers fein! Vater soll dich ein St�ck abgeben, wenn er wieder mal was mitbringt! Na, abers er will diesmal nich gern hin. Die F�rsters haben ihn so gr�slich aufn Strich, und wenn sie ihn kriegen, denn sperren sie ihn gleich ein, und &#8211; denk dich mal! &#8211; er mu� jedesmal l�nger sitzen!�</p>
    <p>�Dann darf er doch nicht in den gro�en Wald gehn!� rief ich aufstehend. Mir war, ich wei� nicht weshalb, doch etwas unheimlich zumute geworden.</p>
    <p>�Meister Ahrens will es aber, und wir wohnen in seinem Haus!� D�rthe war ebenfalls aufgestanden und wischte sich an den Augen herum. �Er sagt, Vater mu� allens ein b�schen vorsichtig machen, und er braucht nicht gleich ein Reh zu nehmen. Abers wenn es nu da heruml�uft?�</p>
    <p>Auf diese Frage wu�te ich auch keine Antwort; aber ich konnte es D�rthe nachf�hlen, da� sie ihren Vater nicht gerade zu Weihnachten im Gef�ngnis haben wollte. Ich mu�te ihr pl�tzlich noch versprechen, keinem etwas von unsrer Unterhaltung zu erz�hlen, und dann trennten wir uns.</p>
    <p>J�rgen wu�te schon nach einer Viertelstunde die ganze Geschichte, und es war nur gut, da� ich sie ihm erz�hlte. Denn ich hatte etwas sehr Tadelnswertes begangen, was ich keinem erwachsnen Menschen mitteilen durfte. Von niemand w�rde ich etwas zu Weihnachten 
    <a name="page455" title="gary/mivo" id="page455"></a> bekommen, wenn man erf�hre, da� ich mit D�rthe Krieger gesprochen hatte.</p>
    <p>�Ihr Vater ist ein Dieb, und zwar ein ganz gemeiner!� berichtete J�rgen. �Rasmussen (unsers Gro�vaters Schreiber) hat mir gerade neulich davon erz�hlt! Denke dir, er stiehlt nicht einmal Geld, was doch das feinste beim Stehlen ist &#8211; er nimmt meist nur W�rste und Schinken. Und er sitzt eigentlich immer im Gef�ngnis!�</p>
    <p>D�rthe hatte mir diese betr�bende Eigenschaft ihres Vaters ja auch berichtet.</p>
    <p>�Sie will nur so ungern, da� er Weihnachten sitzt,� meinte ich; �sie ist dann ganz allein und hat niemand, dem sie ihren Weihnachtsvers aufsagen kann! Sie bekommt �berhaupt gar nichts zu Weihnachten.�</p>
    <p>�Gar nichts?� J�rgens tugendstrenges Gesicht wurde etwas milder. Aber er wu�te doch keinen bessern Rat, als da� ich nicht mehr an D�rthe Krieger denken und noch weniger mit ihr sprechen sollte. Besonders nicht vor Weihnachten. Denn wenn die erwachsnen Familienglieder merkten, welchen schlechten Umgang ich h�tte, dann w�rde es schlimm um meine Geschenkaussichten aussehen.</p>
    <p>J�rgen konnte manchmal sehr eindringlich sprechen, und da ihm wirklich in der letzten Zeit verschiedentlich Standreden dar�ber gehalten worden waren, da� er in seinem Verkehr w�hlerischer sein sollte, so wu�te er genau, was er sagen sollte, und ich h�rte ihm and�chtig zu. D�rthe Krieger war mir selbst doch auch etwas bedenklich vorgekommen; sie hatte meine Pflaumen wohl aufgegessen, sich aber nicht daf�r bedankt. Das zeugte von einem schlechten Herzen. Als ich ihr nach etlichen Tagen wieder begegnete,, und sie mir mit einer gewissen Vertraulichkeit zunickte, sah ich sie deshalb gar nicht an. Als sie aber vor�ber war, mu�te 
    <a name="page456" title="rofe/mivo" id="page456"></a> ich doch stehn bleiben und mich umsehen, und da sie dasselbe tat, sahen wir uns gerade in die Augen.</p>
    <p>Sie lachte; ich aber wurde sehr entl�ftet.</p>
    <p>�Du darfst dich nicht nach mir umsehen &#8211; dein Vater ist ein ganz gemeiner Dieb, und ich will nicht mit dir sprechen.�</p>
    <p>D�rthe sch�ttelte ihren struppigen Kopf und lachte wieder.</p>
    <p>�Nee, sprechen mu�t du auch nich mit mich! Die Kinder in die Schule wollen auch nich bei mich sitzen. Ehegestern hab ich den ganzen Tag allein aufn Bank gesessen &#8211; das war fein!�</p>
    <p>�Magst du gern allein sitzen?�</p>
    <p>Ich war dem Kinde des Diebes nun doch n�her getreten und sah neugierig in ihr unbek�mmertes Gesicht.</p>
    <p>�Nu nat�rlich mag ich es! Da sitzt kein ein bei mich und kneift mir oder schubbst mir &#8211; das is fein?�</p>
    <p>�Ist dein Vater schon im Walde gewesen?� fragte ich.</p>
    <p>Sie sch�ttelte den Kopf. �Nee &#8211; er hat ein slimmes Knie gehabt und konnt nich fort. Ahrens war doll, kann ich dich sagen, und er will uns aus 'n Haus smei�en, wenn Vater nich bald Ernst macht. For meinswegen kann Vater auch hingehn; wenn er man blo� nich wieder Weihnachten sitzen mu�!�</p>
    <p>Sie seufzte ein wenig und schob die Arme unter ihr d�nnes Schultertuch.</p>
    <p>�Ich wei�, wie allens kommt!� fuhr sie dann fort. �Vater geht in den Wald und will blo� die Zweigens abslagen, und denn sieht er ein Reh und denn slachtet er das. Und denn kommt die Pollerzei und all die slechten Menschens, und denn sitzt er Weihnachten ins Loch!�</p>
    <p>�Hast du einen Weihnachtsvers f�r ihn gelernt?� fragte ich: sie beachtete aber meine Worte nicht.</p>
    <p>
    <a name="page457" title="rofe/mivo" id="page457"></a> �Wenn es Ostern w�r oder Pfingsten, denn w�r' es mich einerlei; da is es nich mehr so dunkel, und die andern Kinners snacken nich mehr soviel von Weihnachtsb�umens und von Aufsagen, abers nu &#8211;�</p>
    <p>D�rthe wischte sich die Augen, und ich sah sie ratlos an.</p>
    <p>�Hast du deinem Vater nicht gesagt, er solle bei dir bleiben?�</p>
    <p>�Nu, ganz gewi�! Abers Ahrens wird b�s, wenn er die Zweigens nicht kriegt. Zwei Jahr haben wir die Miete nich bezahlt, weil da� Vater immer so in R�ckstand war!�</p>
    <p>�Dann mu�t du den lieben Gott bitten, da� dein Vater kein Reh totmacht, wenn er in den Wald geht!� riet ich, und D�rthe sah mich nachdenklich an.</p>
    <p>�Das kann angehn! Ich will ihm bitten, da� die Rehens vordem alle tot bleiben oder von den Grafen geslachtet werden. &#8211; For die Zweigens kriegt er ja blo� wenig Gef�ngnis!�</p>
    <p>Sie lief weiter, und mir fiel ein, da� ich nicht mit ihr hatte sprechen wollen. Aber es hatte mich, gottlob! niemand gesehen, und da au�erdem andere Gedanken mein Herz erf�llten, so verga� ich diese Unterredung so bald, da� ich sie nicht einmal J�rgen mitteilte. Es waren n�mlich nur noch acht Tage bis Weihnachten, und die prickelnde, sonderbare Unruhe kam �ber uns, die jedes Kind kennt. Wir mochten nicht mehr sehr lange auf einem Stuhle sitzen, und am liebsten liefen wir auf der Stra�e umher und besahen die bescheidnen Weihnachtsausstellungen unsers St�dtchens.</p>
    <p>Au�erdem hatten wir noch Sorge wegen des Ausbleibens unsers Tannenbaumes. Der sollte mit dem Schiffer kommen, der um die Weihnachtszeit mit seiner 
    <a name="page458" title="rofe/mivo" id="page458"></a> Jacht nach L�beck fuhr und die herrlichsten Sachen mitbrachte. Aber Schiffer Lafrenz war noch nicht in unsern Hafen eingelaufen. Das kam daher, da� der Wind die ganze Zeit �kontr�r� gewesen war, wie uns die Sachverst�ndigen sagten, aber diese Erkl�rung beunruhigte uns nur, statt uns zu beruhigen. Wir kannten Geschichten von Leuten, die drei Wochen auf der Ostsee bei �kontr�rem� Winde gekreuzt hatten, ohne ihr Reiseziel zu erreichen, und die dann schlie�lich wieder unverrichteter Sache nach Hause gefahren waren. Erlebt hatten wir solche Sachen nicht, aber man hatte uns so oft die Abenteuer einer Seereise in alten Zeiten berichtet, da� wir das Schiff mit unserm Tannenbaum im Geiste schon bei Finnland im Eise eingefroren sahen. Die gro�en Leute suchten uns die Bef�rchtungen auszureden; wir aber f�hlten uns doch verpflichtet, jeden Tag an unsern kleinen Hafen zu laufen und dort Erkundigungen nach �Anna Kathrin� einzuziehn. So hie� die Jacht vom Schiffer Lafrenz, und es war ein sch�nes Schiff, nur da� sie sehr schaukelte, auch wenn es gar nicht n�tig schien.</p>
    <p>Am Sonntag vor Weihnachtsabend war k�stliches Wetter. Gerade so, als bildete sich die Sonne ein, Weihnachten �berschlagen zu k�nnen. Sie schien so hell wie im Fr�hjahr, und als wir am Vormittag aus der Kirche kamen, beschlossen wir, sofort wieder nach dem Hafen zu gehn und uns nach der �Anna Kathrin� zu erkundigen.</p>
    <p>Als wir am Hause von Meister Ahrens vor�bergingen, stand dieser vor der T�r und hielt einen Tannenbaum in der Hand. Es war nat�rlich ein falscher, und seine Zweige waren nicht mehr frisch.</p>
    <p>�Wo hast du die Zweige her, Meister Ahrens?� fragten wir. �Das ist kein sch�ner Tannenbaum geworden!�</p>
    <p>Der Tischler antwortete nicht viel, sondern murmelte 
    <a name="page459" title="rofe/mivo" id="page459"></a> nur einige verdrie�liche Worte, worauf einer der �ltern Br�der berichtete, da� das Gesch�ft mit den Tannenzweigen dieses Jahr flau sein sollte. Da w�re niemand mit guten Tannenzweigen an die Insel gekommen, und auch die falschen Tannen sollten teuer sein. Wir andern seufzten ein wenig bei dieser Erz�hlung, und dann strebten wir eilig dem Hafen zu, um uns nach der �Anna Kathrin� die Augen auszuschauen. Aber alles Lugen half nichts &#8211; die dickb�uchige Jacht schaukelte weder am Bollwerk, noch war ihr geflicktes Segel irgendwo am Horizont zu erblicken.</p>
    <p>Nachdem diese Tatsache festgestellt war, verlie�en die �ltern Br�der uns, um einen Freund zu besuchen, dessen Onkel im Besitz eines Fernrohrs war, das dazu dienen sollte, die �Anna Kathrin� etwas schneller herbeizusehen. Wir Kleinern gingen schwerm�tig an den Strand und suchten uns dadurch aufzuheitern, da� wir flache Steine ins Wasser warfen. Bei dieser Gelegenheit entdeckten wir ein Boot, das an einen etwas abseitsstehenden Pfahl angekettet war. Beide Ruderpatten lagen darin, und dieser Umstand schien uns so verlockend, da� wir sofort hineinkletterten und zu rudern begannen.</p>
    <p>Das Boot war au�erordentlich schlecht; die Sitze morsch, und die Bretter des Fahrzeuges schienen kaum noch zusammenzuhalten. Wir schaukelten aber sehr vergn�gt darin, und J�rgen sagte, er k�nne rudern und nach Holstein fahren, dessen K�ste dunkel am Horizont auftauchte. Er konnte es nat�rlich nicht, und w�hrend wir uns um die Ruder zankten, glitt ihm das eine aus der Hand und fiel ins Wasser.</p>
    <p>Vergn�gt schwamm es davon, w�hrend wir ihm ziemlich dumm nachblickten, und als J�rgen mit dem andern Ruder den Fl�chtling zu erwischen gedachte, ging diese Stange ihm auch aus der Hand.</p>
    <p>
    <a name="page460" title="rofe/mivo" id="page460"></a> Ein kr�ftiger Fluch ert�nte vom Lande her, und ein Mann in gro�en Wasserstiefeln trat mitten ins Wasser und zog nicht allein unser Boot ans Land, sondern erfa�te auch noch die eine Stange. Die andre war aber schon zu weit fortgeschwommen, und er sah uns drohend an.</p>
    <p>�Ihr dummes Volk! Was habt ihr in meinem Boot zu tun! Heraus mit euch, sonst werfe ich euch alle ins Wasser! Und wo ist meine Ruderstange?�</p>
    <p>Er sprach fremder und ganz anders als die meisten Insulaner, so da� wir schon deswegen einen gro�en Schreck vor ihm bekamen. Aber als J�rgen mir zufl�sterte, dieser Mann w�re Jobst Krieger, der Dieb, der so oft im Gef�ngnis gesessen hatte, da erwachte in mir der Trotz der Selbstgerechtigkeit.</p>
    <p>�Zu sagen hast du uns n�mlich gar nichts!� bemerkte ich, aber ich sprang doch ziemlich schnell aus dem Boot.</p>
    <p>�Weshalb nicht?� Der Mann, dessen Gesicht uns �brigens keinen abschreckenden Eindruck machte, sah mich fragend an.</p>
    <p>�Du bist ja ein Dieb, ein ganz schlechter Mensch!� sagte ich, und J�rgen, der ebenfalls wieder auf festem Boden stand, nickte zu jedem meiner Worte.</p>
    <p>�Du darfst gar nicht mit uns sprechen,� warf er nun ein. �Du sitzt ja immerlos im Loch!�</p>
    <p>Auf Jobst Kriegers Gesicht lag der Ausdruck ungl�ubigen Staunens, dann aber wurde er pl�tzlich sehr rot.</p>
    <p>�Was geht's euch an, wenn ich im Gef�ngnis war? Darin haben schon fixe Kerle gesessen, kann ich euch sagen! Und �berhaupt� &#8211; er sah uns langsam nach der Reihe an &#8211; �ich kenn euch gut! Wie oft lauft ihr zu dem alten Mahlmann, der sein Leben lang im Zuchthaus war!�</p>
    <p>�Zuchthaus ist feiner als Gef�ngnis,� erkl�rte J�rgen; �viel feiner! Ich habe mal mit Mahlmann dar�ber gesprochen, 
    <a name="page461" title="rofe/mivo" id="page461"></a> und der hat es mir auch gesagt. So oft wie du im Gef�ngnis, ist Mahlmann auch nicht im Zuchthaus gewesen!�</p>
    <p>�Nein, er nahm gleich ein gutes Ende auf einmal!� sagte Jobst Krieger, und dabei lachte er.</p>
    <p>Er hatte wirklich kein �bles Gesicht, und sein Zorn �ber das verlorne Ruder schien auch verraucht zu sein.</p>
    <p>Mit schwerem Schritt stieg er nun ins Boot und begann die Kette zu l�sen.</p>
    <p>�Wohin f�hrst du?� fragte Bruder Milo, der sich bis jetzt nicht an der Unterhaltung beteiligt und den Dieb nur unverwandt angesehen hatte.</p>
    <p>Jobst gab keine Antwort; mir aber fiel D�rthe wieder ein, w�hrend mir nat�rlich nicht in den Sinn kam, da� ich ihr Schweigen gelobt hatte.</p>
    <p>�Er f�hrt in den gro�en Wald,� rief ich laut, �wo die Rehe und die Hasen frei herumlaufen. Da schl�gt er die Tannenb�ume entzwei und f�ngt die Rehe, und dann kommt der b�se Graf und nimmt ihn gefangen! Und D�rthe mu� wieder Weihnachtsabend auf der Stra�e herumlaufen, weil ihr Vater im Gef�ngnis sitzt!�</p>
    <p>�Dummes Zeug!� sagte Jobst. Er hatte mit einer Kelle Wasser aus dem Boot gesch�pft, nun hielt er inne mit seiner Arbeit.</p>
    <p>�Dummes Zeug ist es gar nicht!� rief ich emp�rt. �D�rthe sagt, wenn du nur Ostern oder Pfingsten stehlen wolltest, dann w�re es ihr einerlei; aber gerade Weihnachten! Da darf man doch eigentlich nicht stehlen!�</p>
    <p>�Nein, eigentlich nicht!� meinte J�rgen, und Milo stimmte zu.</p>
    <p>�Da kommt ja das Christkind auf die Erde, und wenn es dich nun im Gef�ngnis findet, dann bekommst du nichts geschenkt. Nur artige Menschen bekommen etwas!�</p>
    <p>�Ich kriege doch nichts geschenkt!� murmelte Jobst. 
    <a name="page462" title="rofe/mivo" id="page462"></a> Er hatte uns bis dahin zugeh�rt, nun griff er wieder zu seiner Sch�pfkelle.</p>
    <p>�Doch!� sagte J�rgen. �Wenn du Weihnachten nicht im Gef�ngnis sitzt, dann schenke ich dir etwas. Ich habe einen Kasten geklebt; er ist sehr h�bsch, und ich wollte ihn eigentlich selbst behalten. Wenn du aber gut sein willst, dann bekommst du ihn!�</p>
    <p>�Und ich mache dir einen Fingerring aus schwarzen Glasperlen!� rief Milo, der in Perlenvergeudung unglaubliches leistete. �Oder willst du lieber einen blauen Ring mit einer Goldperle in der Mitte? Goldperlen sind furchtbar teuer, aber ich will es doch tun!�</p>
    <p>�Dann gebe ich D�rthe auch mein altes Lesebuch!� setzte ich hinzu und trat dabei Jobst Krieger etwas n�her. Er hatte sich n�mlich ins Boot gesetzt und sah uns ganz sonderbar an. Wahrscheinlich fand er die ihm gemachten Anerbietungen zu �berw�ltigend, als da� er gleich darauf h�tte eingehn k�nnen.</p>
    <p>�Sieh mal,� setzte ich vertraulich hinzu. �La� D�rthe doch das Lesebuch bekommen! Da sind h�bsche Bilder drin, und wenn die andern Kinder die sehen, dann wollen sie auch wieder bei D�rthe sitzen. Nun wollen sie es nicht, weil du soviel im Gef�ngnis sitzen mu�t! &#8211; Sie sitzt immer ganz allein, und Weihnachten ist sie auch allein. Ich sagte ihr, sie sollte den lieben Gott bitten, da� du Weihnachten bei ihr w�rst; aber sie hat es wohl vergessen. Der liebe Gott tut sonst alles, um was man ihn ordentlich bittet!�</p>
    <p>Jobst Krieger legte die Bootkette wieder um den Pfahl und trat ans Land. Er sah beunruhigt und etwas m�rrisch aus, und als J�rgen ihm noch einmal seinen sch�nen Kasten pries, antwortete er nur durch ein unverst�ndliches Knurren.</p>
    <p>
    <a name="page463" title="rofe/mivo" id="page463"></a> Auch trat jetzt ein andrer Mann auf ihn zu, der eben erst aus der Stadt gekommen war. Der sah nicht so gut aus wie Jobst, und seine Augen fuhren scheu �ber uns hin, w�hrend er leise mit Jobst sprach. Wir gingen jetzt, J�rgen und ich voran, w�hrend Milo noch eine Weile in der N�he der M�nner blieb und uns erst sp�ter nachgelaufen kam.</p>
    <p>�Ich habe geh�rt, was sie sprachen,� erz�hlte er. �Ich sammelte Steine und war ganz nahe bei ihnen. Der andre Mann hei�t Lorenz und wollte mit Jobst Krieger und dem Boot nach dem gro�en Walde fahren. Aber Jobst sagte, rsaquo;er h�tte keine Lust, sie wollten bis morgen warten. Er m��te sich noch besinnen.rlsaquo; Da wurde der andre Mann b�se und sagte, rsaquo;er f�hre nicht am Montag, das sei ein Ungl�ckstag; er f�hre am Sonntag und wollte nicht auf Jobst warten!rlsaquo; Da haben sie sich gescholten, und nun ist Jobst Krieger zur�ckgegangen, und der andre ist im Boote!�</p>
    <p>Jetzt kamen die andern Br�der. Aber sie waren, weil sie selbst durch das Fernglas nichts von der �Anna Kathrin� gesehen hatten, so niedergeschlagen, da� wir ganz verga�en, ihnen unsre Unterhaltung zu berichten.</p>
    <p>Aber am Abend sprachen wir doch noch von Jobst Krieger und meinten, es sei ganz �berfl�ssig, uns auf Geschenke f�r ihn einzurichten. Milo begann dennoch einen Ring aus blauen Glasperlen zu arbeiten, der wirklich sehr sch�n wurde.</p>
    <p>In der Nacht kam pl�tzlich ein furchtbares Wetter. Die Dezembersonne war tr�gerisch gewesen. Der Wind sprang um, Regen schlug an die Scheiben, und die Dachpfannen prasselten auf die Stra�e. Am andern Morgen wurde es wieder ziemlich still, und die Br�der liefen gleich an den Hafen, um nach der �Anna Kathrin� zu 
    <a name="page464" title="Crown/mivo" id="page464"></a> sehen, die denn auch wirklich einlief. Etwas besch�digt zwar, denn es war auf See ein Heidenwetter gewesen; aber die �Anna Kathrin� konnte schon einen Puff vertragen.</p>
    <p>Obgleich der Tannenbaum nun wirklich in Sicht war, so konnten wir uns doch nicht so recht freuen. Denn Schiffer Lafrenz von der �Anna Kathrin� war nicht weit vom Hafen einem umgeschlagnen Boote begegnet, das er mit seinen scharfen Schifferaugen sofort erkannt hatte. Es geh�rte einem Manne, der Lorenz hie�, und der gerade so �bel ber�chtigt war wie Jobst Krieger.</p>
    <p>Am Hafen hatten die Leute gewu�t, da� Jobst und Lorenz in diesem Boote am Sonntag eine Fahrt hatten machen wollen &#8211; einige Leute wollten sie auch zusammen gesehen haben. Nun hatte sie das Wetter auf offner See �berrascht, und sie waren ertrunken.</p>
    <p>Es war eine traurige Geschichte, die gar nicht f�r die Weihnachtszeit pa�te; wir mu�ten lange dar�ber sprechen. Es tat uns so sehr leid, da� Jobst doch gefahren war, und besonders Milo konnte es gar nicht begreifen. Lorenz mu�te ihn doch schlie�lich �berredet haben.</p>
    <p>Gro�vaters Schreiber, Rasmus Rasmussen, war nicht so traurig wie wir. Er sagte, Jobst w�rde doch im Zuchthause geendet haben, weil er das Stehlen nicht h�tte lassen k�nnen. Tannenzweige aus dem Walde zu holen sei ja schlie�lich kein Verbrechen, aber Jobst h�tte die sch�nsten Tannen auseinandergeschlagen, ohne auch nur einen Menschen zu fragen. Meister Ahrens habe einen guten Lieferanten an ihm gehabt, und deshalb seien seine Tannenb�ume immer so sch�n gewesen. Dann h�tte Jobst auch noch Hasen und Rehe in Schlingen gefangen, und wenn er bei einer fremden, wohlgef�llten Speisekammer vor�bergekommen w�re, dann h�tte er tief hineingelangt.</p>
    <p>Es war gewi� ein Gl�ck, da� Jobst tot war, wie 
    <a name="page465" title="Crown/mivo" id="page465"></a> Rasmus meinte, aber wir waren doch so betr�bt, da� wir eine Weile unser Weihnachtsfest ganz verga�en. Dann sch�mten wir uns auch noch, da� wir um einen ganz gew�hnlichen Dieb weinten.</p>
    <p>Das taten wir n�mlich. Trotz seiner entsetzlichen Schlechtigkeit hatten wir Jobst sehr gern gehabt, wenn wir das auch keinem Menschen verraten und ihn ja auch nur wenig gekannt hatten.</p>
    <p>Pl�tzlich fiel mir D�rthe ein. Was w�rde sie wohl dazu sagen, da� ihr Vater ertrunken war? Den ganzen Tag mu�te ich an sie denken, und J�rgen und Milo sprachen auch von ihr. Nun war sie immer allein; nicht nur Weihnachten, nein auch Ostern und Pfingsten, das ganze Leben hindurch.</p>
    <p>In unserm Hause wurde gerade Kuchen gebacken; das war eine angenehme Zerstreuung; aber als es d�mmrig wurde, lief ich doch zu D�rthe Krieger, deren Wohnung ich jetzt ganz gut kannte, obgleich ich sie nie betreten hatte. J�rgen lief mit, und wir hatten Mama ein Paket Kuchen f�r die arme D�rthe abgebettelt.</p>
    <p>In dem kleinen, sehr verfallnen Hause am �u�ersten Ende der Stadt brannte schon Licht, und als wir ohne weiteres in die Haust�r und dann in die kleine, �rmlich eingerichtete Stube st�rzten, prallten wir erschrocken zur�ck. Denn auf einem Holzschemel, von einem Talglicht beleuchtet, sa� Jobst Krieger. Er hatte Besuch. Vor ihm stand Meister Ahrens, der heftig auf ihn einsprach. Wir beachteten aber den alten Tischler nicht. Wir liefen auf Jobst zu und betrachteten ihn aufgeregt.</p>
    <p>�Wie?� rief J�rgen; �du bist nicht tot?�</p>
    <p>Seine Stimme klang vorwurfsvoll, und auch ich konnte mich einer leichten Verstimmung nicht erwehren. Wenn man jemand einmal als tot beweint hat, dann darf 
    <a name="page466" title="Crown/mivo" id="page466"></a> er auch nicht gleich wieder auferstehn! Jobst Krieger sah uns verlegen an.</p>
    <p>�Lorenz ist allein gefahren,� sagte er nun. �Ich wollte ja nicht, ich &#8211;� er stockte und fuhr sich mit der Hand �ber das Gesicht.</p>
    <p>�Du hast Gl�ck gehabt, Jobst Krieger,� lie� sich jetzt Meister Ahrens vernehmen. �Wenn du mit Lorenz gefahren w�rst, dann l�gst du nu tot in die See! Er war auch ein slechten Kerl, der dir zu allens verf�hrt hat! Morgen f�hrst nu for mich nachn Festland und holst mich die Zweigens, sonsten sollst mich kennen lernen!�</p>
    <p>Aber Jobst sch�ttelte den Kopf.</p>
    <p>�Nein, Meister Ahrens &#8211; ich fahr nicht mehr nach den Tannenzweigen. Wenn ich in den Wald komme &#8211;� er atmete kurz auf &#8211; �dann la� ich's doch nicht &#8211; dann greif ich nach andern Dingen, die mir nicht geh�ren, und dann sitzt die D�rthe Weihnachten allein! Und jetzt, wo Gott mich vorm Tode bewahrt hat &#8211;� er stockte und sah uns an. Wir nickten ihm zu. Allm�hlich hatten wir die Entt�uschung, da� er noch lebte, �berwunden. Meister Ahrens aber rang die H�nde.</p>
    <p>�Du liebe Zeit! Nu krieg ich kein ordentlichen Tannenb�umens, wo das Gesch�ft gerade flott gehn soll. Und du wohnst in meinem Haus und tust nich, was ich will? Du mu�t zu Neujahr ausziehn!�</p>
    <p>Wir hatten Meister Ahrens niemals so b�se gesehen, und unser Interesse wandte sich ihm ungeteilt zu. �Fahre doch selbst in den Wald und hole die Zweige!� rief J�rgen.</p>
    <p>Der Alte sah ihn b�se an. �Da k�nnt ich doch bei zu Schaden kommen!� murrte er, und mein Bruder trat ganz nahe auf ihn zu.</p>
    <p>�Meister Ahrens, du hast mir neulich noch gesagt, 
    <a name="page467" title="Crown/mivo" id="page467"></a> die Hauptsache im Leben w�re ein gutes Herz. Du hast doch auch ein gutes Herz?�</p>
    <p>�Ganzen gewi�lich!� versicherte der Alte mit etwas unsichrer Stimme. �Abers die Tannenb�umens m�ssen doch Zweigens haben, sonsten sind es keine Tannenb�umens, und wenn Jobst Krieger mich nich Zweigens holen will &#8211;�</p>
    <p>�Er will doch kein Dieb mehr sein!� rief J�rgen. �La� ihn in Ruhe und gehe zu Schiffer Lafrenz auf der rsaquo;Anna Kathrinrlsaquo;. Der hat auch eine ganze Menge von Tannenzweigen mitgebracht, die Br�der haben's gesehen!�</p>
    <p>�Is wahr?� Ahrens �rgerliches Gesicht wurde etwas milder, dann lief er pl�tzlich davon, ohne Lebewohl zu sagen. Wir entbehrten ihn auch nicht. Wir hatten unsre Kuchen ausgepackt, und da wir Jobst Krieger verziehn hatten, so durfte er sie probieren. J�rgen und ich sagten ihm auch unsre Weihnachtslieder auf. Der �bung halber und auch deswegen, weil sie uns immer im Kopf herumspukten, und wir waren eigentlich etwas beleidigt, da� Jobst uns gar nicht lobte. Er sa� ganz still und hatte beide H�nde vor sein Gesicht gelegt. So still war er, da� es uns, als wir nacheinander das �Amen� von unsern Verslein gesprochen hatten, doch etwas unheimlich zu werden anfing. Aber da kam D�rthe ins St�bchen gest�rzt, und ihre �berraschung, uns zu sehen, war so gro�, und das Vergn�gen �ber die Kuchen noch so viel gr��er, da� wir ungemein heiter wurden.</p>
    <p>Jobst Krieger stand jetzt auf und sagte, da� er uns nach Hause bringen wolle; unsre Eltern w�rden gewi� nicht wollen, da� wir so lange bei ihm blieben. Wir sahen die Richtigkeit dieser Worte ein, und als wir neben ihm auf der dunkeln Stra�e gingen, stie� J�rgen pl�tzlich einen schweren Seufzer aus.</p>
    <p>
    <a name="page468" title="cal/mivo" id="page468"></a> �Jobst, wie furchtbar schade ist es doch, da� du ein so schlechter Mensch bist! Ich mag dich gern leiden &#8211; viel lieber als einige Leute, die niemals im Gef�ngnis waren!�</p>
    <p>�Ich auch!� versicherte ich, und Jobst stand still und legte ganz leise seine H�nde auf unsre Haare.</p>
    <p>�Mir ist's auch leid genug,� murmelte er; aber was er noch hinzusetzte, konnten wir nicht verstehn; seine Stimme war ganz heiser geworden. Dann war er in der Dunkelheit verschwunden, und wir mu�ten den Rest des Heimwegs allein zur�cklegen.</p>
    <p>Das war nun nicht so schlimm; wir waren nicht �ngstlich und hatten au�erdem eine F�lle von Unterhaltungsstoff, der auch nicht ausging, als wir den andern von Jobst Krieger und von dem Umstande, da� er noch lebe, berichteten. Wir wollten ihm alles m�gliche zu Weihnachten schenken, alte Anz�ge von Papa, die uns nicht geh�rten, E�waren, �ber die wir keine Verf�gung hatten, und vor allem einen Katechismus, damit er die zehn Gebote noch einmal durchlerne.</p>
    <p>Aber es kam anders. Als wir am Tage vor Weihnachten Jobst Krieger und seine Tochter feierlich zu uns einladen wollten, erfuhren wir, da� beide in der Nacht vorher verschwunden waren. Sie hatten ihre armselige Habe zur�ckgelassen und die Insel verlassen. Sie kamen auch nicht wieder, obgleich wir das ganze Weihnachtsfest auf sie warteten, und niemand konnte uns sagen, wohin sie gegangen seien.</p>
    <p>Dieses pl�tzliche Verschwinden betr�bte uns au�erordentlich, und wir tr�steten uns nur allm�hlich mit dem Gedanken, da� uns jetzt kein Mensch verbieten konnte, an Jobst und D�rthe zu denken und von ihnen zu sprechen. Unser Weihnachtsabend war trotz alledem sehr sch�n, und wir schenkten die f�r Jobst bestimmten Sachen andern Leuten, die es auch n�tig hatten.</p>
    <p>
    <a name="page469" title="Crown/mivo" id="page469"></a> Nur Meister Ahrens feierte kein fr�hliches Weihnachtsfest. Erstens waren seine falschen Tannenb�ume lange nicht so h�bsch wie sonst, obgleich er Zweige bekommen hatte, und dann fiel es den Leuten ein, da� er doch vielleicht den Jobst oft zu hart bedr�ngt und ihn schon mehrere Jahre hindurch veranla�t h�tte, in den Wald zu gehn und zu stehlen. Ob er nun wirklich schuld daran hatte, war schwer zu sagen; jedenfalls ging er k�mmerlich gebeugt einher und klagte �ber die schlechten Zeiten und die schlechten Menschen.</p>
    <p>Mehrere Weihnachtsfeste waren vergangen. Meister Ahrens machte immer noch falsche, h��liche Tannenb�ume, und wir selbst sprachen nur manchmal noch von Jobst. Zuerst hatten wir uns ausgedacht, da� er wahrscheinlich nach Amerika gegangen sei und als reicher Mann zur�ckkehren w�rde. Dann trug D�rthe seidne Kleider, und er w�rde uns allen etwas Wundervolles zu Weihnachten schenken. Wir stritten uns dar�ber, ob wir lieber eine goldne Mundtasse oder einen goldnen Teller haben wollten; allm�hlich aber verga�en wir ihn fast, bis wir an einem Weihnachtsabend ein sonderbares Paket mit der Post bekamen.</p>

    <p>Es trug J�rgens, Milos und meinen Namen und kam aus einem Orte, von dem die gro�en Leute sagten, da� er in Ost- oder Westpreu�en l�ge. Dieses Paket enthielt ein sauber geschnitztes kleines Boot, das mit frischen Christrosen angef�llt und in k�stliche Tannenzweige verpackt war. Dabei lag ein Zettel, auf dem mit unge�bter Hand die Worte geschrieben waren: Und hat ein Bl�mlein bracht mitten im kalten Winter. Da wu�ten wir, da� diese Sendung von Jobst Krieger kam, und freuten uns au�erordentlich �ber sie. Besonders dar�ber, da� er von den Weihnachtsliedern, die wir ihm aufgesagt 

    <a name="page470" title="Crown/mivo" id="page470"></a> hatten, etwas behalten hatte. Denn wer auch nur ein wenig von seinen Weihnachtsliedern im Ged�chtnis beh�lt, der kann doch ganz gewi� kein schlechter Mensch sein.</p>

    <p>Meister Ahrens sagte dasselbe. Er hatte mit derselben Post eine Geldsumme bekommen, die, wie er fest glaubte, von Jobst Krieger kam, weil er ihm gerade soviel Geld schuldig gewesen war.</p>

    <p>rsaquo;Eigentlich hast du das Geld nicht verdient!rlsaquo; sagte J�rgen, der dem alten Tischler die Behandlung von Jobst nicht vergessen konnte.</p>

    <p>Ahrens fuhr sich �ber den kahlen Kopf und seufzte.</p>

    <p>�Nee, eigentlich nich! Abersten wenn ich nu die H�lfte an die Armens gebe, und wenn es mich sowieso all die Jahrens leid getan hat, da� ich nich nett gegen den Jobst war? Ich habe sonsten warraftigen Gott ein furchtbar gutes Herz &#8211; blo� bei die Tannenb�umens, da bin ich eigen mit gewesen, weil es so'n gutes Gesch�ft war.�</p>

    <p>Ahrens richtete wirklich eine Weihnachtsbescherung f�r eine arme Familie aus, und seit der Zeit sprach er noch mehr als sonst von seinem guten Herzen. Sonderbarerweise waren es die Kinder dieser Familie, die nicht bei D�rthe Krieger in der Schule hatten sitzen wollen. Das war aber lange vergessen, und der von Ahrens verfertigte falsche Tannenbaum warf auch �ber sie seinen weihnachtlichen Schein, und ihre Freude war echt.</p>

    <p>Denn das Christkind in seiner Milde fragt nicht nach den Verdiensten und Schwachheiten der armen Erdenkinder. Sonst m��te es aufh�ren, alle Jahre wiederzukommen. 

    <a name="page471" title="cal/fibo235" id="page471"></a></p>

  </body>

</html>

